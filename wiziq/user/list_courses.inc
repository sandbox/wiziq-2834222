<?php
 include drupal_get_path('module', 'wiziq').'/function_api.php';
 // add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js	 
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
// end style	

global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),t('Courses')));

 drupal_session_start();
	if(isset($_SESSION['course_id']) || isset($_SESSION['course_id_form'])){
		unset($_SESSION['course_id']);
		unset($_SESSION['course_id_form']);
	} 
function list_course_form()
{
	global $user; 
    $userid = $user->uid;
// delete record
if(isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $userid != 0)
{   
	global $user;
	$nid = $_REQUEST['delete'];
    $num_deleted = db_delete('wiziq_courses')
    ->condition('id', $nid)->condition('created_by',$user->uid)
    ->execute();
    if($num_deleted>0)          
    drupal_set_message(t('Course Delete Successfully'));
    else
    drupal_set_message(t('Some Problem occuring'));
}

// end delete	
   
 	function array_insert(&$array, $insert, $position) {
	settype($array, "array");
	settype($insert, "array");
	settype($position, "int");	 
	//if pos is start, just merge them
	if($position == 0) {
	    $array = array_merge($insert, $array);
	} else {  
      //if pos is end just merge them
	    if($position >= (count($array)-1)) {
	        $array = array_merge($array, $insert);
	    } else {
	        //split into head and tail, then merge head+inserted bit+tail
	        $head = array_slice($array, 0, $position);
	        $tail = array_slice($array, $position);
	        $array = array_merge($head, $insert, $tail);
	    }
	}
}	
$header = array(
        array('data' => t('Course Name'),'field' => 'fullname'),
        array('data' => t('Description'),'field' => 'description'),               	 
        array('data' => t('Created by'),'field' => 'created_by'),
        array('data' => t('Number of Classes')),
 );	  
   if($user->uid != 0)
   {  
	 array_insert($header, array('data' => t('Manage Course')), 2);	      
   }   
 //$result = db_query('select a.id,a.fullname,a.description,a.created_by,b.name from {wiziq_courses} as a left join {users} as b on a.created_by=b.uid');
$query=db_select('wiziq_courses','a')
        ->fields('a');  
  $query->leftJoin('users', 'b', 'b.uid = a.created_by');        
  $query->leftJoin('wiziq_wclasses', 'c', 'c.courseid = a.id');        
  
  $query->fields('b', array('name'));
     
  $query->addExpression('COUNT(c.courseid)', 'course_count');  
  
  $query->groupBy('a.id');
       
 $table_sort = $query->extend('TableSort') // Add table sort extender.

                     ->orderByHeader($header); // Add order by headers.

 $pager = $table_sort->extend('PagerDefault')

                     ->limit(5); // 5 rows per page.
        
$result = $pager->execute();   

   if($user->uid != 0)
   {    
		 $form['button1'] = array(
		   '#type' => 'submit',
		   '#value' => t('Create Course'),
		   '#submit' => array('mymodule_my_form_action_one')
		);
   }    
  //for image
  $img = '<img src="'.base_path().'sites/all/modules/wiziq/images/edit.png" height=20px width=20px />';
  $imgadd = '<img src="'.base_path().'sites/all/modules/wiziq/images/add.png" height=20px width=20px />';
  $imgdel = '<img src="'.base_path().'sites/all/modules/wiziq/images/delete.png" height=20px width=20px />';   
  // end image
 $rows=array();
foreach($result as $res)
{	
	$desc = shorten_string($res->description , '6');	
	if(in_array('administrator', $user->roles))
	{ 
	  $linkbuttons =l($img, 'wiziq/courses/courses_ins',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Course')))).' '.l($imgadd,'wiziq/courses/classes/classes_ins',array('html' => 'true','attributes' => array('title' => t('Add Class')),'query' => array('course_id' => $res->id))).' '.l($imgdel,$_GET['q'],array('html' => 'true','query' => array('delete' => $res->id),'attributes' => array('title' => t('Delete Course'),'onclick'=>'return fun_confirm('.$res->id.');')));	  
	  $rows[]= array(
         l(t($res->fullname),'wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),
         '<div class="divdes"><span id="'.$res->id.'" class="clsdes">'.l(t($desc),'wiziq/courses/course_det',array('query' => array('course_id' => $res->id))).'</span><span id="spn_'.$res->id.'" class="pop_dis">'.$res->description.'</span></div>',
         $linkbuttons,          
         t($res->name),   
         l($res->course_count,'wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),  
       );    
    }
   elseif($user->uid != 0)
   {	   
	   if($user->uid == $res->created_by)	   
	   $linkbuttons = l($img, 'wiziq/courses/courses_ins',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Course')))).' '.l($imgadd,'wiziq/courses/classes/classes_ins',array('html' => 'true','attributes' => array('title' => t('Add Class')),'query' => array('course_id' => $res->id))).' '.l($imgdel,$_GET['q'],array('html' => 'true','query' => array('delete' => $res->id),'attributes' => array('title' => t('Delete Course'),'onclick'=>'return fun_confirm('.$res->id.');')));	  
	   else
	   $linkbuttons='____';
	     
	   $rows[]= array(
         l(t($res->fullname),'wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),
         '<div class="divdes"><span id="'.$res->id.'" class="clsdes">'.l(t($desc),'wiziq/courses/course_det',array('query' => array('course_id' => $res->id))).'</span><span id="spn_'.$res->id.'" class="pop_dis">'.$res->description.'</span></div>',
         $linkbuttons,          
         t($res->name),    
         l($res->course_count,'wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),  
       );       
   }
   else
   {
	   $rows[]= array(
         l(t($res->fullname),'wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),
         '<div class="divdes"><span id="'.$res->id.'" class="clsdes">'.l(t($desc),'wiziq/courses/course_det',array('query' => array('course_id' => $res->id))).'</span><span id="spn_'.$res->id.'" class="pop_dis">'.$res->description.'</span></div>',
         t($res->name),   
         l($res->course_count,'wiziq/courses/classes',array('query' => array('course_id' => $res->id))),   
       );
   }
}
// end select fields
       $form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      ),
      array(
        '#theme' => 'pager',
      ));		
       
return $form;  
} 
function mymodule_my_form_action_one($form, &$form_state) {			 
  drupal_goto('wiziq/courses/courses_ins');
  // Redirect to the appropriate URL.   
}
 
