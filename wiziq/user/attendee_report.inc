<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
	 // Redirect to Listing of courses if not administrator		
	   global $user;	   
/*
	   if(!in_array('administrator', $user->roles))
	   {  
		   drupal_set_message(t('You are not authorized for this section'),'error');
		   drupal_goto('wiziq/courses');
	   }		
     // End Redirect 
*/

if(isset($_GET['class_id']))
{
	$class_id = $_GET['class_id'];
	$enroll_check = get_classbyclassid($class_id);
/*
	print_r($enroll_check);
	echo $user->uid;
	exit;
*/
}
if(!in_array('administrator', $user->roles) && $enroll_check->created_by != $user->uid){
	drupal_set_message(t('You are not Authorized'),'error');
	drupal_goto('wiziq/courses');
}

global $base_url;
 drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),l(t('Courses'),'wiziq/courses'),l(t('Class'),'wiziq/courses/classes',array('query'=>array('course_id'=>$_SESSION['course_id']))),t('Attendee Report')));
function class_attendee_report()
{
	if(isset($_REQUEST['class_id']))
		$id_check = getClassDetail($_REQUEST['class_id']);
	if(!empty($id_check))
		$data = getAttendance_report($id_check->response_class_id);
		
	$header = array(
		array('data' => t('Attendee Name') ),               	 
		array('data' => t('Entry Time') ),
		array('data' => t('Exit Time')),         
		array('data' => t('Attended Time')),
 );	
 $rows = '';
 foreach($data as $val){
 $rows[]= array(
         $val->screen_name,
         $val->entry_time,
         $val->exit_time,
         $val->attended_minutes,
       );
   }
   $form['back_class'] = array(
		'#type' => 'submit',
		'#value' => t('Back to Classes'),
		'#submit' => array('back_to_classes')
	  );
	$form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      )
	);
      return $form;
}
function back_to_classes()
{
	$course_id = $_SESSION['course_id'];
		drupal_goto('wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
}
?>
