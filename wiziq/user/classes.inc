<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';	
	// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
/*
// Redirect to Listing of courses if not administrator		
	   global $user;	   
	   if(!in_array('administrator', $user->roles))
	   {  
		   drupal_set_message(t('You are not authorized for this section'),'error');
		   drupal_goto('wiziq/courses');
	   }		
     // End Redirect 
*/


global $base_url; 
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),l(t('Courses'),'wiziq/courses'),t('Create Class')));     

drupal_session_start();
function classes_form()
{
/***************** directly redirect to courses ******************/
	if(!isset($_REQUEST['course_id']))
		drupal_goto('wiziq/courses');
	
/***************** Create session for course id for form *****************/
	
	$course_id = check_courseid($_REQUEST['course_id']);
	if(!empty($course_id))
		$_SESSION['course_id_form'] = $course_id->id;
	else
		drupal_goto('wiziq/courses');

		global $user;
		$userid = $user->uid;
/*********** get enrolled users permissions *******/
			if(!in_array('administrator', $user->roles) )
		{
			$enroll_result = get_EnrollUser($userid,$course_id->id);
			
			if(empty($enroll_result) || $enroll_result->create_class == 0){
				drupal_set_message(t('You are not Authorized'),'error');
				drupal_goto('wiziq/courses');
			}
		}


         
	$form['class']['class_id'] = array(
    
    '#value' => 'id',
    
    '#type' => 'hidden',
    
    );
    
    $form['title'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Class Title'),
	
	'#required' => TRUE, 
	
    '#size' => 30, 
    
    '#maxlength' => 128, 
    
    '#suffix' => '<div id="error_class_name" class="wiziq_errors error-msg">'.t(' Please enter a title.').'</div>',
    
	);
	
	 $form['duration'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Class Duration (in minutes)'),
	
	'#required' => TRUE, 
	
    '#size' => 30, 
    
    '#maxlength' => 128, 
    
    '#default_value' => 60,
    
    '#description' => t('minimun 30 and maximum 300 minutes'),
    
    '#suffix' => '<div id="error-duration" class="wiziq_errors error-msg ">'.t('Please enter duration between 30 to 300 minutes.').'</div>',
    
	);
	
	// field set start for wiziq data & time
	
    $form['time-date'] = array(
    
    '#type' => 'fieldset',
    
    '#title' => t('Date & Time'),
    
    '#weight' => 5,
    
    '#collapsible' => TRUE,
    
    '#collapsed' => FALSE,
    
   );
    
	$form['time-date']['recurring-check'] = array(
	
     '#type' => 'radios',
    
     '#options' =>  array(0 => t('Want to create a Single Class'), 1 => t('Want to create a Recurring Class')),
     
     '#attributes' => array("onclick" => "check_recurring()"),
     
     '#default_value' => 0,
    
    );
  
   //recurring show field on checked recurring
   $schedule = array('0' => 'Select when class repeats' ,'1' => 'Daily(all 7 Days)', '2' => '6 Days(Mon-Sat)', '3' => '5 Days(Mon-Fri)', '4' => 'Weekly', '5' => 'Monthly');
   $form['time-date']['class-schedule'] = array(
    '#type' => 'select',
    
    '#title' => t('Class Schedule'),
    
    '#options' => $schedule,
    
    '#prefix' => '<div class="show-check-recurring hide-class-recurring">',
    
    '#suffix' => '</div><div id="error-schedule" class="wiziq_errors error-msg ">'.t('Please select Class schedule.').'</div>',
    
    '#attributes' => array("onchange" => "check_class_schedule()"),

  );
   
          
  $form['time-date']['right-now'] =  array( 
   
   '#type' => 'checkbox',
   
   '#title' => t('Schedule for right now'),
   
   '#description' => t('check for the current date and time'),
   
   '#attributes' => array('onclick' => 'right_now_check()'),
    
    ); 
    
    
    
      
   $form['time-date']['class-start-date'] = array(
    
     '#type' => 'date',

     '#title' => t('Class Time'), 

     '#required' => TRUE,
          
     '#date_format' => 'Y-m-d',
     
     '#attributes' => array('onchange' => array('timedate_uncheck()')),
   
    );
    
    $form['time-date']['hours'] = array(
    
    '#type' => 'select',
    
    '#options' => array(
      '00' => '00','01' => '01','02' => '02','03' => '03','04' => '04','05' => '05',
      '06' => '06','07' => '07','08' => '08','09' => '09',10 => 10,
      11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,
      16 => 16,17 => 17,18 => 18,19 => 19,20 => 20,
      21 => 21,22 => 22,23 => 23,
    ),
    
    '#description' => t('Hours'), 
    
    '#attributes' => array('onchange' => array('timedate_uncheck()')),
  );
   
   $form['time-date']['minutes'] = array(
   
    '#type' => 'select',
    
    '#options' => array(
      '00' => '00','01' => '01','02' => '02','03' => '03','04' => '04','05' => '05',
      '06' => '06','07' => '07','08' => '08','09' => '09',10 => 10,
      11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,16 => 16,17 => 17,18 => 18,19 => 19,20 => 20,
      21 => 21,22 => 22,23 => 23,24 => 24,25 => 25,26 => 26,27 => 27,28 => 28,29 => 29,30 => 30,
      31 => 31,32 => 32,33 => 33,34 => 34,35 => 35,36 => 36,37 => 37,38 => 38,39 => 39,40 => 40,
      41 => 41,42 => 42,43 => 43,44 => 44,45 => 45,46 => 46,47 => 47,48 => 48,49 => 49,50 => 50,
      51 => 51,52 => 52,53 => 53,54 => 54,55 => 55,56 => 56,57 => 57,58 => 58,59 => 59,60 => 60,
    ),
    
    '#description' => t('Minutes'),
    
    '#attributes' => array('onchange' => array('timedate_uncheck()')),
  );
  $outzon=getTimeZone();
  // output from XML
  $form['time-date']['time-zone'] = array(
   
    '#type' => 'select',
    
    '#title' => t('Time Zone'),
    
    '#options' => $outzon,
    
  );
  
  // Container for repeat every week 
  $form['time-date']['repeat-every-week-container'] = array(
  
  '#type' => 'container',
  
  );
  $form['time-date']['repeat-every-week-container'] ['repeat-every-week'] = array(
    '#type' => 'select',
    
    '#title' => t('Repeat every week'),
    
    '#options' => array(
      1 => 1,2 => 2,3 => 3,4 => 4,5 => 5,
    ),
    
  );
  $form['time-date']['repeat-every-week-container'] ['repeat-every-week-on'] = array(
    '#type' => 'checkboxes',
    
    '#title' => t('on'),       
    
    '#options' => array(
    'sunday' => 'S','monday' => 'M','tuesday' => 'T','wednesday' => 'W','thursday' => 'T', 'friday' => 'F','saturday' => 'S'
    ),
    
  );
  
  
  
  // container for Repeat by
  
  $form['time-date']['repeat-by-container'] = array(
  
  '#type' => 'container',
  
  );
  
  $form['time-date']['repeat-by-container']['weekly-repeat-by'] = array(
    
     '#type' => 'radios',
    
     '#title' => t('Repeat by'),
     
     '#default_value' => 'repeat_day',
    
     '#options' =>  array('repeat_day' => t('Day'), 'repeat_date' => t('Date')),
     
     '#attributes' => array("onclick" => "check_repeat_by()"),
     
  );
  
  
  
  
  $form['time-date']['repeat-by-container']['repeat-by-day-date'] = array(
    '#type' => 'select',
    
    '#options' => array(
      '1st' => '1st','2nd' => '2nd','3rd' => '3rd','4th' => '4th','last' =>'Last',
     
   
    ),
     '#attribute' => array('class' => 'hide-repeat-type'),  
    
  );
  $form['time-date']['repeat-by-container']['repeat-by-day-week'] = array(
    '#type' => 'select',
    
    '#options' => array(
      'sunday' => 'Sun','monday' => 'Mon','tuesday' => 'Tue','wednesday' => 'Wed','thursday' => 'Thu', 'friday' => 'Fri','saturday' => 'Sat'
    
    ),
    '#suffix' => '<div class="repeat_by_suffix">'.t('of every month').'</div>', 
    
    '#attribute' => array('class' => 'hide-repeat-type'),
  );
  
  
  $form['time-date']['repeat-by-container']['repeat-by-date'] = array(
    '#type' => 'select',
    
    '#options' => array(
      '1st' => '1st','2nd' => '2nd','3rd' => '3rd','4th' => '4th','5th' =>'5th','6th' => '6th','7th' => '7th','8th' => '8th','9th' =>'9th','10th' => '10th','11th' =>'11th','12th' => '12th','13th' => '13th','14th' => '14th','15th' =>'15th','16th' => '16th','17th' =>'17th','18th' => '18th','19th' => '19th','20th' => '20th','21st' =>'21st','22nd' => '22nd','23rd' =>'23rd','24th' => '24th','25th' => '25th','26th' => '26th','27th' =>'27th','28th' => '28th','29th' => '29th','30th' =>'30th','31st' =>'31st'      
    ),
  
  );
 
  
  $form['time-date']['end-container'] = array(
  
     '#type' => 'container',
     
     '#suffix' => '',
     
     '#prefix' => '',
  
  );
  
  $form['time-date']['end-container']['class-end'] = array(
	
     '#type' => 'radios',
    
     '#title' => t('Ends'),
    
     '#default_value' => 0,
    
     '#options' =>  array(0 => t('After Classes'), 1 => t('On date')),
     
     '#attributes' => array("onclick" => "check_end_class()"),
    
    );
    
  $form['time-date']['end-container']['after-class'] = array(
	
     '#type' => 'textfield',
     
     '#size' => 30, 
    
     '#maxlength' => 128, 
 
     '#prefix' => '<div id="after-class" class="hide-class-end">',
     
     '#suffix' => '</div>',
  );  
    
  $form['time-date']['end-container']['on-date'] = array(
	
     '#type' => 'date',
 
     '#prefix' => '<div id="on-date" class="hide-class-end">',
     
     '#suffix' => '</div>',
  );    
    
  // end class field for recurring
  
  
  // fieldset start for wiziq settings
  $form['wiziq-settings'] = array(
    
    '#type' => 'fieldset',
    
    '#title' => t('Add more information about yourself and your class'),
    
    '#weight' => 9,
    
    '#collapsible' => TRUE,
    
    '#collapsed' => TRUE,
    
   );
      
    $form['wiziq-settings']['attendee-limit'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Attendee Limit in a Class'),
	
    '#size' => 7,
    
    '#default_value' => 100, 
    
    '#suffix' => '<div id="error-attendee-limit" class="wiziq_errors error-msg ">'.t('Invalid attendee limit.').'</div>',
             
	);
  
	$form['wiziq-settings']['record-class'] = array(
	
     '#type' => 'radios',
    
     '#title' => t('Record this class'),
    
     '#default_value' => 'true',
    
     '#options' => array(
     
     'true' => t('Yes'),
     
     'false' => t('No'),
     ),
   );
    $outlan=getLanguages();
    // output from XML
   $form['wiziq-settings']['language'] = array(
    '#type' => 'select',
    '#default_value' => 'en-US',
    '#title' => t('Language of instruction in class'),
    
    '#options' => $outlan ,
    
  ); 
  
   $form['class_submit'] = array(
        '#type' => 'submit',
        '#value' => t('Schedule & Continue'), 
        '#weight'=>10,       
  );
    $form['class_cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'), 
        '#weight'=>10,       
        '#submit' => array('classes_cancel'),
        '#limit_validation_errors' => array(),    
  );
    
	return $form;
}

/*
 function classes_form_validate($form, &$form_state)
 {
	$clsnam=$form['title']['#value'];
	//variable_set('class_title', $clsnam);
	$dur=$form['duration']['#value'];	 
 	variable_set('class_duration', $dur);
 	$rec_check=$form['time-date']['recurring-check']['#value'];
 	variable_set('rec_check', $rec_check);
 	$class_schedule=$form['time-date']['class-schedule']['#value'];
 	variable_set('class_schedule', $class_schedule);
 	$tim_right=$form['time-date']['right-now']['#value'];
 	variable_set('tim_right', $tim_right); 
 	$str_date_year = $form['time-date']['start-date']['#value'];
 	variable_set('start_date_year', $str_date_year); 
 	$str_date_month = $form['time-date']['start-date']['#value']['month'];

 	variable_set('start_date_month', $str_date_month); 
 	$str_date_day = $form['time-date']['start-date']['#value']['day'];
 	//variable_set('start_date_day', $tim_day); 

 	$hours=$form['time-date']['hours']['#value'];
 	$minutes=$form['time-date']['minutes']['#value'];
 	$timezone=$form['time-date']['time-zone']['#value'];
 	$at_limit=$form['wiziq-settings']['attendee-limit']['#value'];
 	$record_cls=$form['wiziq-settings']['record-class']['#value'];

    $lang=$form['wiziq-settings']['language']['#value'];
}
*/
 
function classes_form_submit($form, &$form_state)
{
	  
  $retdata = createliveclass($form_state);
   
   if($retdata['ret_status'] == 'ok')
   {
	   
		 $nid = db_insert('wiziq_wclasses')
		 ->fields(array(   
		 'created_by'=>$retdata['created_by'],
		 'class_name' => $retdata['class_name'],
		 'class_time' => $retdata['class_time'],
		 'duration' => $retdata['duration'],
		 'courseid' => $_SESSION['course_id_form'],
		 'classtimezone' => $retdata['classtimezone'],
		 'language' => $retdata['language'],
		 'recordclass' => $retdata['recordclass'],			 
		 'attendee_limit' => $retdata['attendee_limit'],			 
		 'response_class_id' => $retdata['response_class_id'],			 
		 'response_recording_url' => $retdata['response_recording_url'],			 
		 'response_presenter_url' => $retdata['response_presenter_url'],			 
		 'status' => $retdata['status'],			 
		 'master_id' => (int) $retdata['master_id'],			 
		 'attendence_report' =>'',			 
		 'get_detail' =>$retdata['get_detail'],			 
		 'download_recording' => '',			 
		 'is_recurring' => $retdata['is_recurring'],		  		 
		  ))
		  ->execute(); 
		  drupal_set_message(t('Classes Saved Successfully'));
		if(isset($_SESSION['course_id'])){
			$course_id = $_SESSION['course_id'];			 
		}
		 else
			$_SESSION['course_id'] = $_SESSION['course_id_form'];
			//drupal_goto('wiziq/courses/classes/class_det', array('query' => array('class_id' => $retdata['response_class_id'])));
			drupal_goto('wiziq/courses/classes', array('query' => array('course_id' => $_SESSION['course_id'])));
    }
    else
    {
		$form_state['rebuild'] = TRUE;
		if(!empty($retdata['errorcode']))
		drupal_set_message(t('Error: '.$retdata['error'].'<br>Errorcode: '.$retdata['errorcode']),'error');
		else
		drupal_set_message(t('Error: '.$retdata['error']),'error');
		 //drupal_set_message(t($retdata['error'],array($retdata['errorcode'])),'error');
	}  
}

function classes_cancel()
{
	if(isset($_SESSION['course_id'])){
		$course_id = $_SESSION['course_id'];
		drupal_goto('wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
	}
	else
		drupal_goto('wiziq/courses');  	
}

?>
