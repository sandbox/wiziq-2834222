<?php
 include drupal_get_path('module', 'wiziq').'/function_api.php';

function course_det_form()
{
 
 global $user; 
$userid = $user->uid; 
   
$rows = array();  
	  


/***************** Create session for course id *****************/
	 drupal_session_start();
	 if(isset($_REQUEST['course_id']))
	 $res = check_courseid($_REQUEST['course_id']);
	 if(!empty($res))
		 $_SESSION['course_id'] = $res->id;

if(!empty($res)){
	
	global $base_url;
	drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),t($res->fullname)));
	$rows[]= array(
         t('Full Name:'),t($res->fullname),
	 );	  
	 $res->created_by == $userid ? $teacher = t('You') : $teacher = $res->name;
	 $rows[]= array(
         t('Created By:'),$teacher,
	 );
	 $rows[]= array(
         t('Start Date:'),t($res->startdate),
	 );
	 $rows[]= array(
         t('End Date:'),t($res->enddate),
	 );
	 $rows[]= array(
         t('Description:'),t($res->description),
	 ); 
}
		$form['back_class'] = array(
				'#type' => 'submit',
				'#value' => t('Back to Courses'),
				'#submit' => array('back_to_courses')
			  );
	if(!empty($res)){
		$form['goto_class'] = array(
				'#type' => 'submit',
				'#value' => t('Go to Classes'),
				'#submit' => array('go_to_classes')
			  );
		}
		$form['course_id'] = array(
				'#value' => isset($res->id) ? $res->id : -1,
				'#type' => 'hidden',
			  );

		$form['table'] = array(
		   '#theme' => 'table',       
		   '#rows' => $rows,
		   '#empty' => t('Table has no row!'),        
			  );
      
      return $form; 
 
}
 

function back_to_courses($form, &$form_state)
{
		drupal_goto('wiziq/courses');
}
function go_to_classes($form, &$form_state)
{
		$course_id = $form_state['complete form']['course_id']['#value'];
		if($course_id != -1)
		drupal_goto('wiziq/courses/classes',array('query' => array('course_id' => $course_id)));
		else
		drupal_goto('wiziq/courses');
}
 
 
