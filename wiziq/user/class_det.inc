<?php
 include drupal_get_path('module', 'wiziq').'/function_api.php';
 
/*
 // Redirect to Listing of courses if not administrator			   
	   if($userid == 0)
	   {  
		   drupal_set_message(t('You are not authorized for this section'),'error');
		   drupal_goto('wiziq/courses');
	   }		
     // End Redirect 
*/


// to be continue (later purpose)
global $base_url;
 drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),l(t('Courses'),'wiziq/courses'),l(t('Class'),'wiziq/courses/classes',array('query'=>array('course_id'=>$_SESSION['course_id']))),t('Detail')));
function class_det_form()
{
global $user; 
$userid = $user->uid;
   /*********** get enrolled users permissions *******/
$course_id = isset($_SESSION['course_id']) ? $_SESSION['course_id'] : -1 ;
			if(!in_array('administrator', $user->roles) )
			{
				$enroll_result = get_EnrollUser($userid,$course_id);
				 
				if(empty($enroll_result)){
					drupal_set_message(t('You are not enroll to view this classs'),'error');
					drupal_goto('wiziq/courses');
				}else{
					$download_recording = $enroll_result->download_recording;
				}
			}
			else
			$download_recording = 1;
			
$rows = array();  
// end select fields
if(isset($_REQUEST['class_id'])){
	$class_id = $_REQUEST['class_id'];
	$id_check = getClassDetail($class_id);
	if(!empty($id_check) && $id_check->status == 'upcoming' || ($id_check->status == 'completed' && $id_check->attendence_report != 'available' ))
		getData($id_check->response_class_id);
	}
else
$class_id = -1;

	$res = getClassDetail($class_id);   
	$rows = array();
   
if(!empty($res)){
	$outlan = getLanguages();
	foreach ( $outlan as $key=>$value)
	{
		if($res->language == $key)
		{
			$language = $value;
		}
	}
	if($res->status == 'upcoming'){
	// for current live status	
		$sta=wiziq_get_datetime ($res->class_time, $res->duration, $res->classtimezone );
		if($sta == true)
		$status = t("Live Class");
		else 
		$status = $res->status;
	}
	else
		$status = $res->status;
		// end current live status
	$rows[]= array(
         t('Class title:'),t($res->class_name),
	 );
	 $res->created_by == $userid ? $teacher = t('You') : $teacher = $res->name;
	 $rows[]= array(
         t('Teacher:'),$teacher,
	 );
	 $rows[]= array(
         t('Class Status:'),t($status),
	 );
	 $rows[]= array(
         t('Timing of Class:'),t($res->class_time),
	 );
	 $rows[]= array(
         t('Time Zone:'),t($res->classtimezone),
	 );
	 $rows[]= array(
         t('Duration (in Minutes):'),t($res->duration),
     );
     $rows[]= array(
         t('Language in Classroom:'),t($language),
	 );
	 $rows[]= array(
         t('Recording opted:'),$res->recordclass == 'true' ? t('yes') : t('No'),
     );
}
$form['back_class'] = array(
		'#type' => 'submit',
		'#value' => t('Back to Classes'),
		'#submit' => array('back_to_classes'),
        '#attributes' => array('onclick' => 'this.form.target="_self";return true;')
	  );
	global $user;
	$userid = $user->uid; 
	if($res->status == 'upcoming'){
	  if($res->created_by == $userid){
	 $form['launch_class'] = array(
		'#type' => 'submit',
		'#value' => t('Launch Class'),
		'#parents' => array($res->response_presenter_url), 
		'#submit' => array('launch_class'),
		'#attributes' => array('onclick' => 'this.form.target="_blank";return true;'),
	  );
  }else{
	  $form['join_class'] = array(
		'#type' => 'submit',
		'#value' => t('Join Class'),
		'#parents' => array($res->response_class_id , $res->language), 
		'#submit' => array('join_class'),
		'#attributes' => array('onclick' => 'this.form.target="_blank";return true;'),
	  );
  }
}
  if($res->download_recording != '' && $download_recording == 1) {
  $form['download_recording'] = array(
		'#type' => 'submit',
		'#value' => t('Download Recording'),
		'#submit' => array('download_class'),
		'#parents' => array($res->download_recording), 
	  );
  }
     $form['table'] = array(
       '#theme' => 'table',       
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),        
      );
      
      return $form; 
 
} 

function back_to_classes($form, &$form_state)
{
	$course_id = $_SESSION['course_id'];
		drupal_goto('wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
}
function download_class($form, &$form_state)
{
	$download_recording = $form['download_recording']['#parents'][0];
	$form_state['redirect'] = $download_recording;
}
function join_class($form, &$form_state)
{
	$class_id = $form['join_class']['#parents'][0];
	$language = $form['join_class']['#parents'][1];
	$join_url = wiziq_addattendee($class_id,$language);
	$form_state['redirect'] = $join_url;
}
 
 function launch_class($form, &$form_state)
 {
	$start_url = $form['launch_class']['#parents'][0];
	$form_state['redirect'] = $start_url;
 }
 
