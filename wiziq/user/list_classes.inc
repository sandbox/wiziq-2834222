<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
 	// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');

// Redirect to Listing of courses if not administrator		
	   global $user;	   
/*
	   if(!in_array('administrator', $user->roles))
	   {  
		   drupal_set_message(t('Please Login to view classes'),'error');
		   drupal_goto('wiziq/courses');
	   }		
     // End Redirect 
*/
global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'wiziq'),l(t('Courses'),'wiziq/courses')));


function list_classes_form()
{
	
	global $user; 
	$userid = $user->uid; 
  
	
 /***************** Create session for course id *****************/
	 drupal_session_start();
	 if(isset($_REQUEST['course_id']))
	 $course_id = check_courseid($_REQUEST['course_id']);
	 if(!empty($course_id))
		 $_SESSION['course_id'] = $course_id->id;
		
		/*********** get enrolled users permissions *******/
		$create_class = 1;
		$view_recording = 1;
		$download_recording = 1;
		
		if(!in_array('administrator', $user->roles) )
		{
			$enroll_result = get_EnrollUser($userid,$course_id->id);
			
			if(empty($enroll_result)){
				drupal_set_message(t('You are not enroll to view this classs.Please contact to your teacher.'),'error');
				drupal_goto('wiziq/courses');
			}
			else{
				$create_class = $enroll_result->create_class;
				$view_recording = $enroll_result->view_recording;
				$download_recording = $enroll_result->download_recording;
			}
		}

// to be continue (later purpose)
 
/************* Delete Classes ***************/
if(isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $userid != 0)
	{
		$cid = $_REQUEST['delete'];
		$result = getClassDetail($cid);
		if(isset($result) && !empty($result)){
		if($result->status == 'upcoming'){
			$return = delete_class($cid);
		if(isset($return) && $return['status'] == 'fail')
			drupal_set_message(t($return['errormsg'].'<br>Errorcode : '.$return['errorcode']),'error');
		else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				drupal_set_message(t('Class Delete Successfully'));
			}
		}
		else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				drupal_set_message(t('Class Delete Successfully'));
			}
		}
	}

 /*********** Fetch records having get_detail 0***************/
	$query = db_select('wiziq_wclasses','a')
    ->fields('a')
    ->condition('get_detail',0,'=');
    $result = $query->execute();
    foreach($result as $res)
    {
	  $rec_details = getRecurringDetail($res->master_id);
	  if(!empty($rec_details)){
       foreach($rec_details->class_details as $rec_detail) 
		{
			//echo '<pre>';print_r($rec_detail);echo '</pre>';
				  // exit;
		   if($res->response_class_id != $rec_detail->class_id)
		    {				   
			  db_insert('wiziq_wclasses')
              ->fields(array(
			  'created_by' => $res->created_by,
			  'class_name' => $rec_detail->class_title,
			  'class_time' => date("Y-m-d H:i:s" , strtotime($rec_detail->start_time)) ,
			  'duration'   => $rec_detail->duration,
			  'courseid'   => $res->courseid,
			  'classtimezone' => $res->classtimezone,
			  'language' => $res->language,
			  'recordclass' => $res->recordclass,
			  'attendee_limit' => $res->attendee_limit,
			  'response_class_id' => $rec_detail->class_id,
			  'response_recording_url' => $rec_detail->recording_url,
			  'response_presenter_url' => $rec_detail->presenter_list->presenter->presenter_url,
			  'status' => $res->status,
			  'master_id' => $res->master_id,
			  'attendence_report' => $rec_detail->attendance_report_status,
			  'get_detail' => 1,
			  'download_recording' => '',
			  'is_recurring' => 'TRUE',			  
			  ))
			  ->execute();
			}
			 
				 	 
	    }	
	     db_update('wiziq_wclasses')
				  ->fields(array(
					'get_detail' => 1,					 
				   ))
				  ->condition('response_class_id', $res->response_class_id, '=')
				  ->execute();		   
	}
}
 


 	function array_insert(&$array, $insert, $position) {
		settype($array, "array");
		settype($insert, "array");
		settype($position, "int");	 
		//if pos is start, just merge them
		if($position == 0) {
			$array = array_merge($insert, $array);
		} else { 
	 
		  //if pos is end just merge them
			if($position >= (count($array)-1)) {
				$array = array_merge($array, $insert);
			} else {
				//split into head and tail, then merge head+inserted bit+tail
				$head = array_slice($array, 0, $position);
				$tail = array_slice($array, $position);
				$array = array_merge($head, $insert, $tail);
			}
		}
	}
$refresh_img = '<img id="clsimg"  src="'.base_path().'sites/all/modules/wiziq/images/refresh.png" height=20px width=20px />';
$header = array(
        array('data' => t('Class Title'),'field'=>'class_name'),
        array('data' => t('Class Start Time') ),               	 
        array('data' => t('Presenter') ),
	    array('data' => t('Class Status').$refresh_img,'class' => 'clsimg'),
        array('data' => t('Attendance Report')),
        array('data' => t('View Recording')),
        array('data' => t('Downlaod Recording')),
        
 );	  
 if($userid != 0)
 {
	 array_insert($header, array('data' => t('Manage Class')),4);	      
 } 
  
$rows = array(); 
// button
if(isset($_REQUEST['course_id'])){
	
	if((isset($create_class) && $create_class == 1)){
		$form['create-class'] = array(
			'#type' => 'submit',
			'#value' => t('Create Class'),
			'#submit' => array('classes_form_action')
		  );
	  }
  $form['update-btn'] = array(
    '#type' => 'submit',
    '#value' => t('Get Class'),
    '#attributes' => array('style'=> 'display:none'),
    '#submit' => array('classes_form_Get')
  );
  
   if(isset($_REQUEST['master_id'])){
	$form['back-class'] = array(
    '#type' => 'submit',
    '#value' => 'Back to Classes',
    '#submit' => array('back_to_classes')
   );
  }
  
}
$form['back_course'] = array(
    '#type' => 'submit',
    '#value' => t('Back to Courses'),
    '#submit' => array('back_to_courses')
  );
// end button
// end select fields
if(isset($_REQUEST['course_id']))
$course_id = $_REQUEST['course_id'];
else
$course_id = -1;

$query = db_select('wiziq_wclasses','a')
        ->fields('a', array('id','class_name', 'class_time','duration','classtimezone', 'status','recordclass','attendence_report','response_recording_url','download_recording','response_class_id','master_id','is_recurring','created_by'));  
  $query->leftJoin('users', 'b', 'b.uid = a.created_by');        
  $query->fields('b', array ('name'));
  $query->condition('courseid',$course_id, '=');
  if(isset($_REQUEST['master_id']))
	  $query->condition('master_id',$_REQUEST['master_id'], '=');
        
 $table_sort = $query->extend('TableSort') // Add table sort extender.

                     ->orderByHeader($header); // Add order by headers.

 $pager = $table_sort->extend('PagerDefault')

                     ->limit(5); // 5 rows per page.
        
$result = $pager->execute();   

 $rows = array();
 $img = '<img src="'.base_path().'sites/all/modules/wiziq/images/edit.png" height=20px width=20px />';
 $imgdel = '<img src="'.base_path().'sites/all/modules/wiziq/images/delete.png" height=20px width=20px />';
 $img_reccur = '<img src="'.base_path().'sites/all/modules/wiziq/images/list.png" height=20px width=20px ></img>';
 
foreach($result as $res)
{
	if($res->created_by == $user->uid)
	{
		$create_class = 1;
		$view_recording = 1;
		$download_recording = 1;
	}
	if(isset($classIds))
		$classIds.=",".$res->response_class_id;
	else
		$classIds=$res->response_class_id;
	if($userid != 0)
	{	
		$edit_link = '';
		$linkbuttons = '';
		$report = '';
		 if(in_array('administrator', $user->roles) || $res->created_by == $user->uid){
			/*********** handle link buttons *********/
			$edit_link = $res->status == 'upcoming'? l($img, 'wiziq/courses/classes/edit',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Class')))) : '';
			$linkbuttons =$edit_link.' '.l($imgdel,$_GET['q'],array('html' => 'true','query' => array('course_id' => $_SESSION['course_id'],'delete' => $res->response_class_id),'attributes' => array('title' => t('Delete Class'),'onclick'=>'return confirm("Are you sure , you want to delete this class ?")')));	 
			
			/*********** handle link button for recurring class *********/
			$linkbuttons .= $res->is_recurring == 'TRUE' && !isset($_REQUEST['master_id']) ? ' '.l($img_reccur,$_GET['q'],array('html' => 'true','query' => array('course_id' => $_SESSION['course_id'],'master_id' => $res->master_id),'attributes' => array('title' => t('View Recurring Class Detail'),'class'=>array("recclass")))) : '' ;
			
			
			/*********** handle Report Link *********/
			$report = $res->attendence_report == 'available' ? l(t('View'),'wiziq/courses/classes/view_attendee_report',array('query' => array('class_id' => $res->response_class_id))) : '_' ;
		}
		/*********** handle view recording Link *********/
		$view_recording = ($res->status == 'completed' && $res->recordclass == 'true' && $view_recording == 1) ? l(t('View'),$res->response_recording_url,array('attributes'=>array('target'=>'blank'))) : '_';
		/*********** handle Download recording Link *********/
		$download = $res->download_recording != '' && $download_recording == 1 ? l(t('Download'),$res->download_recording,array('attributes'=>array('target'=>'blank'))): '_';

		// for current live status
		if($res->status == 'upcoming'){
			$sta=wiziq_get_datetime ($res->class_time, $res->duration, $res->classtimezone );
			if($sta == true)
			$status = t("Live Class");
			else 
			$status = $res->status;
		}
		else
			$status = $res->status;
			// end current live status
		$Class_time = t($res->class_time)."(".$res->classtimezone.")";
	$rows[]= array(
         l(t($res->class_name),'wiziq/courses/classes/class_det',array('query' => array('class_id' => $res->response_class_id))),
         $Class_time,
         t($res->name),
         t($status),
         $linkbuttons,          
         $report,    
         $view_recording,
         $download,
       );		
   }
}
if(isset($classIds) && !empty($classIds))
refresh_class($classIds);                // Refresh classes on load
$form['class_ids'] = array(
    '#type' => 'hidden',
    '#attributes'=>array('id'=>'clsid'),
    '#value' => isset($classIds)? $classIds: '' );
    
       $form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' =>  $rows ,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      ),
      array(
        '#theme' => 'pager',
      ));		
       
return $form;  
} 
function classes_form_action($form, &$form_state) {
			 
 $course_id = $_SESSION['course_id'];
	drupal_goto('wiziq/courses/classes/classes_ins', array('query' => array('course_id' => $course_id)));
	// Redirect to the appropriate URL. 
  
}

function classes_form_Get($form, &$form_state) {
 $clsids = $form_state['input']['class_ids'];
 refresh_class($clsids);
}
function refresh_class($clsids)
{
	 //exit;
	if(!empty($clsids)){ 			 
 
	$data = db_query("SELECT response_class_id,status,recordclass,attendence_report FROM {wiziq_wclasses} WHERE response_class_id IN($clsids)");
	$data1 = $data->fetchAll();
	
	/*********** For getData API Request ************/
	 foreach($data1 as $result)
	{
		if($result->status == 'upcoming' || ($result->status == 'completed' && $result->attendence_report != 'available' ))
			getData($result->response_class_id);
	}
	/*********** For getdownloadlink API Request ************/ 
		
	
	 foreach($data1 as $result1)
	{
		if( $result1->status == 'completed' && $result1->recordclass == 'true' )
			getdownloadlink($result1->response_class_id);
		
	}
	}
}

function back_to_courses()
{
	drupal_goto('wiziq/courses');
}
function back_to_classes()
{
	if(isset($_SESSION['course_id'])){
		$course_id = $_SESSION['course_id'];
		drupal_goto('wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
	}
}


 
