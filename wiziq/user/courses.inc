<?php
   
global $base_url;
drupal_set_breadcrumb(array(l('Home',$base_url),l('wiziQ','wiziq'),l('Courses','wiziq/courses'),t('Create Course')));
function course_form()
{
		
// add js
   drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
   drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
// end style
	    $nam = '';		 
		$strdat = '';
		$enddat = '';
		$des = '';		 
		$editid = -1;
		$id = '';
		// Redirect to Listing of courses if not administrator		
	   global $user;
	   
	   if($user->uid == 0)
	   {   
		   drupal_set_message(t('You are not authorized for this section'),'error');
		   drupal_goto('wiziq/courses');
	   }		
   // End Redirect    
if(isset($_GET['edit']))
{
	$editid = $_GET['edit'];
	$result = db_query('select id,fullname,startdate,enddate,description from {wiziq_courses} where id=:id and created_by=:creat' , array(':id' => $editid,':creat'=>$user->uid));
	foreach($result as $res)
	{		 
		$id = $res->id;
		$nam = $res->fullname;
		 
	// startdate		
     	$strdat1 = $res->startdate;   
      $year = date('Y', strtotime($strdat1))  ;    
      $month = date('m', strtotime($strdat1))  ;     
      $day = date('d', strtotime($strdat1))  ;         	
     $strdat = array(
      'day' => (int) $day,
      'month' => (int) $month,
      'year' => $year,
    );
     // end startdate
     // enddate
     	$enddat1 = $res->enddate;
        $yrend = date('Y', strtotime($enddat1))  ;    
        $mnthend = date('m', strtotime($enddat1))  ;    
        $dayend = date('d', strtotime($enddat1))  ;         	
        $enddat = array(
      'day' => (int) $dayend,
      'month' => (int) $mnthend,
      'year' => $yrend,
    );
    	 
    // end enddate	 	
		$des = $res->description;		 
	} 
}
$form['course']['course_id'] = array(
    
    '#value' => $id,
    
    '#type' => 'hidden',
    
    );
    if(isset($_GET['edit']))
		$msg = "Edit Course";
	else
		$msg = "Create Course";
	
	
		
    $form['course']['html_div'] = array(
    '#type' => 'markup',
    '#prefix' => '<h1 id="page-title" class="title">'.$msg,
    '#suffix' => '</h1>',
  );
	$form['course']['name'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Course Name'),
	
	'#required' => TRUE, 
	
    '#size' => 30, 
    
    '#maxlength' => 70, 
    
    '#default_value' =>  $nam ,
    
    '#suffix' => '<div id="error-name" class="wiziq_errors error-msg ">'.t('Course name should not be empty').'</div>',
    
	);
	
	 
	
	
	$form['course']['start_date'] = array(

     '#type' => 'date', 

     '#title' => t('Course Start Date'), 

     '#required' => FALSE,
     
     '#default_value' =>  $strdat ,
     
     '#date_format' => 'Y-m-d',
     
     '#suffix' => '<div id="error-start" class="wiziq_errors error-msg ">'.t('Start date sholuld be greater than or equal to current date').'</div>',
      

    );
    
    $form['course']['end_date'] = array(

     '#type' => 'date', 

     '#title' => t('Course End Date'), 

     '#required' => FALSE,
     
     '#default_value' =>  $enddat ,
     
     '#date_format' => 'Y-m-d',
     
     '#suffix' => '<div id="error-end" class="wiziq_errors error-msg ">'.t('End date sholuld be greater than or equal to start date').'</div>',

    );
    
    $form['course']['description'] = array(
    
    '#type' => 'textarea',
    
    '#title' => t('Description'),   
    
    '#default_value' =>  $des ,
    
    '#description' => '(Max 4000 Characters)',
    
    );
    
    $form['courses']['submit'] = array(
    
    '#type' => 'submit',
    
    '#value' => t('Save & Close'),
    
    );
    
    $form['courses']['cancel'] = array(
    
    '#type' => 'submit',
    
    '#submit' => array('course_form_cancel'),
    
    '#value' => t('Cancel'),   
    
    '#limit_validation_errors' => array(),     
    
    );
 
	return $form;
}     
/** validation  */
function course_form_validate($form, &$form_state)
{
    
   //concatinate 0 with start date
    if($form['course']['start_date']['#value']['month'] <= 9)
        $s_mnt = '0'.$form['course']['start_date']['#value']['month'];
	else
		$s_mnt = $form['course']['start_date']['#value']['month'];
		
	if($form['course']['start_date']['#value']['day'] <= 9)
       $s_day = '0'.$form['course']['start_date']['#value']['day'];
	else
	   $s_day = $form['course']['start_date']['#value']['day'];	
	
	//concatinate 0 with end date
	if($form['course']['end_date']['#value']['month'] <= 9)
        $e_mnt = '0'.$form['course']['end_date']['#value']['month'];
	else
		$e_mnt = $form['course']['end_date']['#value']['month'];
		
	if($form['course']['end_date']['#value']['day'] <= 9)
       $e_day = '0'.$form['course']['end_date']['#value']['day'];
	else
	   $e_day = $form['course']['end_date']['#value']['day'];	
	   
    $course_start_date = $form['course']['start_date']['#value']['year'].$s_mnt.$s_day ;	
    $course_end_date = $form['course']['end_date']['#value']['year'].$e_mnt.$e_day ;	
    $current_date = date('Ymd');
	
	
    if(intval($course_start_date) > intval($course_end_date))
    {
		form_set_error('', t('End date should be greater then start date'));
	}
	
	if($course_start_date < $current_date)
	{
		form_set_error('', t('Start date should be greater then current date'));
	}
	
	
	
} 
 /* End Validation */ 
function course_form_submit($form, &$form_state)
{ 
	$course_name = $form['course']['name']['#value'];	 
	$course_start_date = $form['course']['start_date']['#value']['year'].'-'.$form['course']['start_date']['#value']['month'].'-'.$form['course']['start_date']['#value']['day'];
	$course_end_date = $form['course']['end_date']['#value']['year'].'-'.$form['course']['end_date']['#value']['month'].'-'.$form['course']['end_date']['#value']['day'];
	$course_desc = $form['course']['description']['#value'];
	$course_id = $form['course']['course_id']['#value'];
    global $user;
    $userid = $user->uid;
  if(!empty($course_id))
  {
	  $nid = db_update('wiziq_courses')
	  ->fields(array(   
		'fullname' => $course_name,		 
		'startdate' => $course_start_date,
		'enddate' => $course_end_date,
		'description' => $course_desc,
		'created_by' => $userid,			 
	  ))
	  ->condition('id',$course_id, '=')
	  ->execute();
		drupal_set_message(t('Course Update Successfully'));
  }
  else
  {  
	   $nid = db_insert('wiziq_courses')
	  ->fields(array(   
		'fullname' => $course_name,		 
		'startdate' => $course_start_date,
		'enddate' => $course_end_date,
		'description' => $course_desc,
		'created_by' => $userid,			 
	  ))
	  ->execute();
		drupal_set_message(t('Course Saved Successfully'));
   }
   drupal_goto('wiziq/courses');	 
}
 
function course_form_cancel($form, &$form_state)
{ 	  
	// Redirect to the appropriate URL. 
	drupal_goto('wiziq/courses');	 
}

?>
