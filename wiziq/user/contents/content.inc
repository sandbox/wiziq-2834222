<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';	
global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('wiziQ'),'wiziq'),l(t('content'),'wiziq/contents'),t('Upload Content')));
function content_form($form, &$form_state)
{
// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');	

   if(isset($_REQUEST['parent_id'])){	
		$parent_id = $_REQUEST['parent_id'];
		$data = check_parentId($parent_id);
		if(!empty($data))
		$parent_id_update = $data->id; 
		else
		drupal_goto('wiziq/contents');
	}
    elseif(isset($form_state['input']['parent_id']) && $form_state['input']['parent_id'] != 0)
    $parent_id_update = $form_state['input']['parent_id'];
    else
    drupal_goto('wiziq/contents');

	$nam = '';
	$editid = '';
	$support_type_img = '<img src="'.base_path().'sites/all/modules/wiziq/images/supported.gif"  />';
    $form['parent_id'] = array(
    
    '#value' => $parent_id_update,
    
    '#type' => 'hidden',
    
    );
    
	$form['content']['course_id'] = array(

	'#value' => $editid,

	'#type' => 'hidden',

	);
    
	$form['content']['name'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Content Name'),
	
	'#size' => 33, 

	'#maxlength' => 128, 

	'#default_value' =>  $nam,
    
	);
	
	$form['content']['uploadingfile'] = array(

	'#type' => 'managed_file',

	'#title' => t('Choose a file'),
	
	'#upload_validators' => array(
    'file_validate_extensions' => array('doc docx pdf xls xlsx ppt pptx pps ppsx swf flv wav wma mp3 mp4 mov avi mpeg wmv'),
    // Pass the maximum file size in bytes
    'file_validate_size' => array(100*1024*1024),
		),

    );
    
    $form['content']['allowed format'] = array(
	  '#type' => 'item',
	  '#title' => t('Allowed File Types'),
	  '#markup' => $support_type_img,
	);

  
      $form['content']['submit'] = array(
    
    '#type' => 'submit',
    
    '#value' => t('Upload & Close'),
    );
    
    $form['content']['cancel'] = array(
    
    '#type' => 'submit',
    
    '#submit' => array('content_form_cancel'),
    
    '#value' => t('Cancel'),   
    
    '#limit_validation_errors' => array(),     
    );
   
	return $form;
}
/** validation  */
function content_form_validate($form, &$form_state)
{
    
   
} 
 /* End Validation */ 
function content_form_submit($form, &$form_state)
{ 
	
	global $user;
	 
	
	
    $uploaddata['file_title'] = $form_state['input']['name'];    
    $filedata = $form_state['values']['uploadingfile'] ;
	
	$parent_id_update = $form_state['values']['parent_id'];
	
	$folderpath = check_parentId($parent_id_update);
	$filedata = file_load($filedata);
	$retmsg = wiziq_content_upload($uploaddata ,$filedata,$folderpath->folderpath);
	if($retmsg['livestatus'] == 'ok')
	{	 
	    $nid = db_insert('wiziq_contents')
	    ->fields(array(   
		'status' => 'Inprogress',
		'created_by' => $user->uid,
		'isfolder' => 0,
		'name' => $uploaddata['file_title'],
		'parent_id' =>$parent_id_update,
		'content_id' =>$retmsg['content_id'],
		'uploadingfile' =>$filedata->filename, 
		'folderpath' =>'', 
	  ))	   
	  ->execute();	  
      drupal_set_message(t('Content Uploaded'));  
      drupal_goto('wiziq/contents', array('query' => array('parent_id' => $parent_id_update)));
    }
    else
    {
		$errorcode = $retmsg['errorcode'];
		$errormsg = $retmsg['errormsg'] ;
		drupal_set_message(t('Error: '.$errormsg.'<br>Errorcode: '.$errorcode),'error');
	}
 }
function content_form_cancel($form, &$form_state){
	$parent_id_update = $form_state['input']['parent_id'];
	// Redirect to the appropriate URL. 
	drupal_goto('wiziq/contents', array('query' => array('parent_id' => $parent_id_update)));
}

?>
