<?php
global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('Administration'),'admin'),t('WizIQ')));
	$output = '<ul class="admin-list">';

		$output .= '<li class="clearfix">';
		$output .= '<span class="label">'.l(t('Courses'),'admin/wiziq/courses').'</span>';
		$output .= '<div class="description">'.t('Manage Courses').'</div>';

 		$output .= '<li class="clearfix">';
		$output .= '<span class="label">'.l(t('Contents'),'admin/wiziq/contents').'</span>';
		$output .= '<div class="description">'.t('Manage Content').'</div>';
		$output .= '</li>'; 
	 
	$output .= '</ul>';
?>
