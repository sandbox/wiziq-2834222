jQuery(document).ready(function(){
	
	
	 
	 //jQuery('#edit-update-btn').click();	// refresh class in page load
	jQuery('.chkselall').click(function(){
		 
		 if(jQuery(this).is(':checked'))
		 {
			jQuery('.chksingle').attr('checked',true);  
		 }
		 else 
		 {
			jQuery('.chksingle').attr('checked',false);  
		 }
    });
     
    	jQuery('.delrec').click(function(){
			return confirm('Are you sure , you want to delete this class ?');
		});
		
		jQuery('.clsimg').click(function(){
			refresh_submit();
		});
		
		jQuery('.refresh').click(function(){
			refresh_submit();
		});
	/*
    *  tooltip 
    */
    jQuery(".clsdes").hover(function() {	    
	    var id=this.id;		 		 
	    if(jQuery('#spn_'+id).is(":visible"))
	    {
			jQuery('#spn_'+id).hide();
		}
		else
		{
			jQuery('#spn_'+id).show();
		}	 	 
	});
    /*
     * JQuery for the Front & Back End courses 
     */
     
     jQuery('#edit-submit').click(function(){
		jQuery('.wiziq_errors').addClass('error-msg');
		jQuery('.form-select,.form-text').removeClass('error-field');
		var course_name = jQuery('#edit-name').val();
		var course_short_name = jQuery('#edit-short-name').val();
		var s_month = (jQuery('#edit-start-date-month')).val().trim();
		var s_day = (jQuery('#edit-start-date-day')).val().trim();
		var s_year = (jQuery('#edit-start-date-year')).val().trim();
		var e_month = (jQuery('#edit-end-date-month')).val().trim();
		var e_day = (jQuery('#edit-end-date-day')).val().trim();
		var e_year = (jQuery('#edit-end-date-year')).val().trim();
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
		var startdate = s_year+((s_month<10)?'0':'')+s_month+((s_day<10)?'0':'')+s_day;
		var enddate = e_year+((e_month<10)?'0':'')+e_month+((e_day<10)?'0':'')+e_day;
		var currentdate = yyyy.toString()+((mm<10)?'0':'')+mm.toString()+((dd<10)?'0':'')+dd.toString();
		var ret= true;
		if(startdate < currentdate){
			jQuery('#edit-start-date-month,#edit-start-date-day,#edit-start-date-year').addClass('error-field');
			jQuery('#error-start').removeClass('error-msg');
			ret = false;
		}
		else if(startdate > enddate){
			jQuery('#edit-end-date-month,#edit-end-date-day,#edit-end-date-year').addClass('error-field');
			jQuery('#error-end').removeClass('error-msg');
			ret = false;
		}
		else if(course_name == '' ){
		    jQuery('#edit-name').addClass('error-field');
		    jQuery('#error-name').removeClass('error-msg');
		    ret = false;
	    }	
	    else if(course_short_name == '' ){
		    jQuery('#edit-short-name').addClass('error-field');
		    jQuery('#error-short-name').removeClass('error-msg');
		    ret = false;
	    }			
		
		return ret;
  });    
  
  
  jQuery('#edit-save-folder').click(function(){
	  var create_folder = jQuery('#edit-text-folder').val().trim();
	  var ret = true;
	  if(create_folder == '')
		{
		jQuery('#edit-text-folder').addClass('error-field');
		ret = false; 
		}
		return ret;
	  });
  
  

	/*
     * JQuery for the Front & Back End classes 
     */
    // Title validation
    jQuery('#edit-title').blur(function(){
		var ret=true;
		var tit = jQuery(this).val().trim();
		jQuery('.wiziq_errors').addClass('error-msg');
		jQuery('.form-text').removeClass('error-field');		 
		if(tit == '')
		{
			jQuery('#edit-title').addClass('error-field');
			jQuery('#error_class_name').removeClass('error-msg');
			ret = false;
		}
		return ret;		
	}); 
	 // attendee limit validation
    jQuery('#edit-attendee-limit').blur(function(){
		var ret=true;
		var attendee_limit = jQuery(this).val().trim();
		jQuery('.wiziq_errors').addClass('error-msg');
		jQuery('.form-text').removeClass('error-field');		 
		jQuery('#error-attendee-limit').hide() ;
		var patt3 = /[0-9]+/g;	
		 
	    if(attendee_limit == '' || attendee_limit == 0 || attendee_limit.match(patt3) != attendee_limit )
		{
			jQuery('#edit-attendee-limit').addClass('error-field');
			jQuery('#edit-attendee-limit').removeClass('error-msg');
			jQuery('#error-attendee-limit').show() ;
			setTimeout(function(){
			jQuery('#edit-attendee-limit').focus() ;
			},100);
			ret = false;
		}
		else if(attendee_limit <1 || attendee_limit > 1000 )
		{
			jQuery('#edit-attendee-limit').addClass('error-field');
			jQuery('#edit-attendee-limit').removeClass('error-msg');
			jQuery('#error-attendee-limit').show() ;
			setTimeout(function(){
			jQuery('#edit-attendee-limit').focus() ;			
			},100);
			ret = false;
		}
		return ret;		
	}); 	  
   // duration validation
    jQuery('#edit-duration').blur(function(){
		var ret=true;
		var duration = jQuery(this).val().trim();
		jQuery('.wiziq_errors').addClass('error-msg');
		jQuery('.form-text').removeClass('error-field');	
		var patt3 = /[0-9]+/g;	 
		if(duration < 30 || duration > 300 || duration.match(patt3) != duration || duration == '')
		{
			jQuery('#edit-duration').addClass('error-field');
			setTimeout(function(){
			jQuery('#edit-duration').focus() ;
			},100);
			jQuery('#error-duration').removeClass('error-msg');
			ret = false;
		}
		return ret;		
	}); 
	// end validation
	
	jQuery('#edit-class-submit').click(function(){
		
		jQuery('.wiziq_errors').addClass('error-msg');
		jQuery('.form-text').removeClass('error-field');
		jQuery('.form-select').removeClass('error-field');
		var title = (jQuery('#edit-title')).val().trim();
		var duration = (jQuery('#edit-duration')).val().trim();
		var after_class = (jQuery('#edit-after-class')).val().trim();
		var attendee_limit = (jQuery('#edit-attendee-limit')).val().trim();
		var ret=true;
		var patt4 = /[#*_@%]+/g;
		var patt3 = /[0-9]+/g;
		if(title == '')
		{
			jQuery('#edit-title').addClass('error-field');
			jQuery('#error_class_name').removeClass('error-msg');
			ret = false;
		}
		else if(duration < 30 || duration > 300 || duration.match(patt3) != duration || duration == '')
		{
			jQuery('#edit-duration').addClass('error-field');
			jQuery('#error-duration').removeClass('error-msg');
			ret = false;
		}
		else if(attendee_limit == '' || attendee_limit.match(patt3) != attendee_limit )
		{
			jQuery('#edit-attendee-limit').addClass('error-field');
			jQuery('#error-attendee-limit').removeClass('error-msg');
			ret = false;
		}
		else if(jQuery("#edit-recurring-check-1").is(":checked"))
		{
			var value = jQuery('#edit-class-schedule').val();
			if(value == 0)
			{
				jQuery('#edit-class-schedule').addClass('error-field');
				jQuery('#error-schedule').removeClass('error-msg');
				ret = false;
			}
			//~ if(after_class == '' || after_class.match(patt3) != after_class)
			//~ {
				//~ jQuery('#edit-after-class').addClass('error-field');
				//~ jQuery('#error-after-class').removeClass('error-msg');
				//~ ret = false;
			//~ }
		}
		
		return ret;
	});


/* Create folder */
jQuery('.folder_element').click(function(){
	jQuery('.disp_element').show();	
	jQuery('.folder_element').hide();	
	return false;
});
 
/* End Create Folder */





 /* ===========================================================================
  * jQuery for classes part 
  */
   // for single or recurring checkbox   
    check_recurring();    
  //  for on date or after class radio check
    check_end_class();     
  // check for the class schedule
  check_class_schedule();
  // check repeat by month
  check_repeat_by();    
  // checkbox checked
  right_now_check();
 });         
// for single or recurring radio check
function check_recurring()
{
	if(jQuery("#edit-recurring-check-1").is(":checked"))
    {
	    jQuery('.show-check-recurring').removeClass('hide-class-recurring');
	    jQuery('#edit-end-container').css('display','block');	  
	    jQuery('.form-item-right-now').hide();
    }
    else
    {
		jQuery('.show-check-recurring').addClass('hide-class-recurring');
		jQuery('#edit-end-container').css('display','none');
		jQuery('.form-item-right-now').show();
		jQuery('#edit-class-schedule').val(0);
		check_class_schedule();
	}
}
// for on date or after class radio check
function check_end_class()
{
	if(jQuery("#edit-class-end-0").is(":checked"))
    {
	    jQuery('#after-class').removeClass('hide-class-end');
	    jQuery('#on-date').addClass('hide-class-end');
    }
    else
    {
		jQuery('#on-date').removeClass('hide-class-end');
		jQuery('#after-class').addClass('hide-class-end');
	}	
}
// check for the class schedule
function check_class_schedule()
{
	var value = jQuery('#edit-class-schedule').val();
	if(value == 4)
	{
	 jQuery('#edit-repeat-every-week-container').css('display','block');
	 jQuery('#edit-repeat-by-container').css('display','none');
    }
    else if(value == 5)
    {
		jQuery('#edit-repeat-by-container').css('display','block');
		jQuery('#edit-repeat-every-week-container').css('display','none');
	}
	else
	{
		jQuery('#edit-repeat-every-week-container').css('display','none');
		jQuery('#edit-repeat-by-container').css('display','none');
	}
}

// check repeat by month
function check_repeat_by()
{
	if(jQuery("#edit-weekly-repeat-by-repeat-day").is(":checked"))
    {
		 jQuery('.form-item-repeat-by-day-date').show();
		 jQuery('.form-item-repeat-by-day-week').show();
		 jQuery('.form-item-repeat-by-date').hide();
	}
	else
	{
		jQuery('.form-item-repeat-by-day-date').hide();
		 jQuery('.form-item-repeat-by-day-week').hide();
		 jQuery('.form-item-repeat-by-date').show();
	}	
}
function right_now_check()
{	
	 if(jQuery('#edit-right-now').is(':checked'))
		 {			  
			var dt=new Date();
			var ye_ar=dt.getFullYear();
			var mon_th=month = dt.getMonth()+1;
			var d_ay = dt.getDate();
			var ho_urs = dt.getHours();
			var min_ute = dt.getMinutes();			 
			 
			jQuery('#edit-class-start-date-year option').filter(function(){							 
			    return (jQuery(this).text() == ye_ar);			
			}).attr('selected',true); 		
		 			
			jQuery('#edit-class-start-date-month option').filter(function(){
							  
			    return (jQuery(this).val() == mon_th);
			
			}).attr('selected',true); 
			
			jQuery('#edit-class-start-date-day option').filter(function(){
							 
			    return (jQuery(this).text() == d_ay);
			
			}).attr('selected',true); 
			
			jQuery('#edit-hours option').filter(function(){
							 
			    return (jQuery(this).text() == ho_urs);
			
			}).attr('selected',true); 
			
			jQuery('#edit-minutes option').filter(function(){
							 
			    return (jQuery(this).text() == min_ute);
			
			}).attr('selected',true); 
		 }
}

function refresh_submit()
{
	 jQuery('#edit-update-btn').click();
}
// Alert Befor Delete
function fun_confirm(id)
{
	var cls_cnt=jQuery('#href_'+id).text();	 
	if(cls_cnt!=0)
	{	
	alert('Delete Inner Classes First');	
	return false;
	}
	else
	{
		return confirm('Are You Sure, You Want to Delete This Course ?');
	}
}
// End Delete	
//Admin confirm before delete 
	function admin_del_course()
	{	
	var err=0;
	jQuery('input[type=checkbox]:checked').each(function() {
		if(jQuery('#href_'+this.value).text()>0)
		{
			err = 1;			 
		}	 
	});
	 if (err == 1) {
				alert('Delete Inner Class First');
				return false;
			} else {
				return confirm("Are you sure, You want to delete this Course?");
			}
	}
	// End Confirm
	
//Content(Admin confirm before delete )
	function admin_del_content()
	{	
	var err=0;
	var th=jQuery(this);
	jQuery('input[type=checkbox]:checked').each(function() {
		 
		if(jQuery(this).attr('fol')>0)
		{
			err = 1;			 
		}	 
	});
	 if (err == 1) {
				alert('Delete Inner Folder/Files First');
				return false;
			} else {
				return confirm("Are you sure, You want to delete?");
			}
	}
	// End Confirm

	function timedate_uncheck()
	{
		if(jQuery('#edit-right-now').is(':checked'))
		{
			jQuery('#edit-right-now').attr('checked', false);
		}
	}
	 
