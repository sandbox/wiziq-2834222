jQuery(document).ready(function(){	
	            // submit records
	            jQuery('#enrol_sub').click(function(){
					  var nam,creat_cls,view_rec,down_rec,up_cont,tm={},tmp={},tmp1={},tmp2={},tmp3 = {};
				 jQuery('.cls_enroll').each(function(){
					 
					  var user_id=this.id;
					  user_id=user_id.substring(user_id.indexOf('_')+1,user_id.length);
					   
					  
					nam=jQuery(this.cells[0]).text();
				  
          		    if(jQuery(this.cells[1]).find('.create_class').is(':checked'))
          		    creat_cls=1;
          		    else
          		    creat_cls=0;
          		    
          		    if(jQuery(this.cells[2]).find('.view_recording').is(':checked'))
          		    view_rec=1;
          		    else
          		    view_rec=0;
          		     
          		    if(jQuery(this.cells[3]).find('.download_recording').is(':checked'))
          		    down_rec=1;
          		    else
          		    down_rec=0;
          		     
          		    if(jQuery(this.cells[4]).find('.upload_content').is(':checked'))
          		    up_cont=1;
          		    else
          		    up_cont=0; 
          		    
          		 tm[user_id]=(tm.hasOwnProperty(user_id))?(tm[user_id] + user_id):user_id;              		 
          		 tmp[user_id]=(tmp.hasOwnProperty(user_id))?(tmp[user_id] + creat_cls):creat_cls; 
          		 tmp1[user_id]=(tmp1.hasOwnProperty(user_id))?(tmp1[user_id] + view_rec):view_rec; 
          		 tmp2[user_id]=(tmp2.hasOwnProperty(user_id))?(tmp2[user_id] + down_rec):down_rec; 
          		 tmp3[user_id]=(tmp3.hasOwnProperty(user_id))?(tmp3[user_id] + up_cont):up_cont; 
					 
				 });
				    var output = [];
					for (keys in tm) {
						 
						output.push([keys,tmp[keys],tmp1[keys],tmp2[keys],tmp3[keys]]);	 
					}
	               var json = JSON.stringify(output); 
	               
	               jQuery('#sub_data').val(json);
	               jQuery('#enroll_but').click();
	           
				  
				});
	            // end submit 
                //Add users to errolled list from all users list
                jQuery('#wiziq-add-user').click(function () {
					 
                    if(jQuery("#wiziq-all-user option:selected").length==0){
                        alert("Please select user from right panel");
                        jQuery("#wiziq-all-user").focus();
                        return;
                    }
                    var permission_string='';
                    var chkper='';
                    jQuery.map(jQuery('#wiziq-all-user').find("option:selected"),function(option, i) {
               
                   chkper='';
                        permission_string += '<tr class="cls_enroll" id=enroll_'+jQuery(option).val()+'>';
                        permission_string += '<td><input type="hidden" name="user_id_1[]" value="'+jQuery(option).val()+'" />'+jQuery(option).text() +'</td>';
                        permission_string += '<td><input type="hidden" value="0" name="create_class_1['+jQuery(option).val()+']" /><input value="1" type="checkbox" name="create_class['+jQuery(option).val()+']" class="create_class" '+chkper+'  id="create_class_'+jQuery(option).val()+'" /> </td>'; 
                        permission_string += '<td><input type="hidden" value="0" name="view_recording_1['+jQuery(option).val()+']" /><input value="1" type="checkbox"  name="view_recording['+jQuery(option).val()+']" class="view_recording" id="view_recording_'+jQuery(option).val()+'"/> </td>';
                        permission_string += '<td><input type="hidden" value="0" name="download_recording_1['+jQuery(option).val()+']" /><input value="1" type="checkbox" name="download_recording['+jQuery(option).val()+']" class="download_recording" id="download_recording_'+jQuery(option).val()+'"/> </td>';
                        permission_string += '<td><input type="hidden" value="0" name="upload_content_1['+jQuery(option).val()+']" /><input value="1" type="checkbox"  name="upload_content['+jQuery(option).val()+']" class="upload_content" '+chkper+' id="'+jQuery(option).val()+'"/> </td>';
                        permission_string += '</tr>';
                    });                       
                     
                    jQuery('#wiziq-course-permission tbody').append(permission_string);
                    jQuery('#wiziq-all-user').find('option:selected').remove().appendTo('#wiziq-enroll-user');
                    if(jQuery("#wiziq-all-user option").length==0){
                        jQuery("#wiziq-add-user").css('visibility','hidden');
                        }
                    if(jQuery("#wiziq-enroll-user option").length>0){
                        jQuery("#wiziq-remove-user").css('visibility','visible');
                    }
                });

                //Remove users from errolled list
                jQuery("#wiziq-remove-user").click(function(){
                    if(jQuery("#wiziq-enroll-user option:selected").length==0){
                        alert("Please select user from left panel");
                        jQuery("#wiziq-enroll-user").focus();
                        return;
                    }
                    jQuery.map(jQuery('#wiziq-enroll-user').find("option:selected"), function(option, i) {
                        jQuery('#wiziq-course-permission tr#enroll_'+jQuery(option).val()).remove();
                    });
                    jQuery('#wiziq-enroll-user').find('option:selected').remove().appendTo('#wiziq-all-user');
                    if(jQuery("#wiziq-enroll-user option").length==0)
                        jQuery("#wiziq-remove-user").css('visibility','hidden');
                    if(jQuery("#wiziq-all-user option").length>0)
                        jQuery("#wiziq-add-user").css('visibility','visible');
                });
                
                //On upload content, if create class is not checked it will automatically checked
                
                jQuery('.upload_content').live('click',function(){
					 
                    var current_id = jQuery(this).attr('id');
                    if(jQuery(this).is(':checked')){
                     jQuery("#create_class_"+current_id).attr('checked',true);
                     jQuery("#view_recording_"+current_id).attr('checked',true);
                     jQuery("#download_recording_"+current_id).attr('checked',true);
					 }
					 else if(jQuery(this).not(':checked')){
						jQuery("#create_class_"+current_id).attr('checked',false);
						jQuery("#view_recording_"+current_id).attr('checked',false);
						jQuery("#download_recording_"+current_id).attr('checked',false);
					 }
                   });                
                 //on click of create class,if others are not checked then check them and vice versa
                jQuery('.create_class').live('click',function(){
					 
                    var current_id = jQuery(this).attr('id');
                    
                    var lastChar = current_id.split("_").pop();
                    if(jQuery(this).is(':checked')){
                     jQuery("#view_recording_"+lastChar).attr('checked',true);
                     jQuery("#download_recording_"+lastChar).attr('checked',true);
                     jQuery('#'+lastChar).attr('checked',true);
					 }
					 else if(jQuery(this).not(':checked')){
						jQuery("#view_recording_"+lastChar).attr('checked',false);
						jQuery('#'+lastChar).attr('checked',false);
						jQuery("#download_recording_"+lastChar).attr('checked',false);
					 }
                   });
                  // on click downloading recording,view recording true or vice versa
                  jQuery('.download_recording').live('click',function(){
					 
                    var current_id = jQuery(this).attr('id');
                    
                    var lastChar = current_id.split("_").pop();
                    console.log(lastChar);
                    if(jQuery(this).is(':checked')){
                     jQuery("#view_recording_"+lastChar).attr('checked',true);
					 }
					 else if(jQuery(this).not(':checked')){
						jQuery("#view_recording_"+lastChar).attr('checked',false);
					 }
                   });
                  
                 //~ //On create class, if create class is unchecked upload content will automatically unchecked
                //~ jQuery('.create_class').live('click',function(){
                    //~ var currentId = jQuery(this).attr('id');
                    //~ if(!jQuery("#"+currentId).is(':checked')){
                        //~ var ids= currentId.substring(currentId.lastIndexOf("_")+1);
                        //~ jQuery("#"+ids).attr('checked',false);
                        //~ 
                    //~ }
                //~ });
                
                //onload, if needed hide add or remode button
                if(jQuery("#wiziq-enroll-user option").length==0)
                    jQuery("#wiziq-remove-user").css('visibility','hidden');
                if(jQuery("#wiziq-all-user option").length==0)
                    jQuery("#wiziq-add-user").css('visibility','hidden');
 });         
