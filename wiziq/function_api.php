<?php
 
include drupal_get_path('module', 'wiziq').'/translations/wiziq_en.php';	

   function check_api_credentials($secretAcessKey,$access_key,$webServiceUrl)
   {
		require_once("AuthBase.php");
		$authBase = new AuthBase($secretAcessKey,$access_key);
		$method = "get_account_info";
		$requestParameters["signature"] = $authBase->GenerateSignature($method,$requestParameters);
		$requestParameters["app_version"] = "Drupal 1.0";
		$httpRequest = new Wiziq_HttpRequest();
		try
		{
			$XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl.'?method='.$method,http_build_query($requestParameters, '', '&')); 
		}
		catch(Exception $e)
		{	
	  		echo $e->getMessage();
		}
 		if(!empty($XMLReturn))
 		{
 			try
			{
			  $objDOM = new DOMDocument($XMLReturn);
			  $xml = $objDOM->loadXML($XMLReturn);	  
			  $xmlr = new SimpleXMLElement($XMLReturn);
			}
			catch(Exception $e)
			{
			  $api_error = $e->getMessage();
			  $errormsg = t('There is some error in WizIQ Service');
			  drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
			}
		$status = $objDOM->getElementsByTagName("rsp")->item(0);
    	$attribNode = $status->getAttribute("status");
    	if($attribNode == 'fail'){
    	$error = $objDOM->getElementsByTagName("error")->item(0);
    	$code = $error->getAttribute("code");
    	$errormsg = $error->getAttribute("msg");
    	
    	$display_msg = error_msg_display($code,$errormsg);
		drupal_set_message($display_msg , 'error');
}
    	
		return $attribNode; 
		}
	 }//end if	
	 
	 function shorten_string($string, $wordsreturned) {
			/*  Returns the first $wordsreturned out of $string.  If string
			contains fewer words than $wordsreturned, the entire string
			is returned.
			*/
			$retval = $string;      //  Just in case of a problem
			 
			$array = explode(" ", $string);
			if (count($array)<=$wordsreturned)
			/*  Already short enough, return the whole thing
			*/
			{
				$retval = $string;
			}
			else
			/*  Need to chop of some words
			*/
			{
				array_splice($array, $wordsreturned);
				$retval = implode(" ", $array)." ...";
			}
			return $retval;
	}
	 
	 
    //function to get time zones from wiziq api
    function getTimeZone(){
        //For live api
        $xmlTimeUrl = 'http://class.api.wiziq.com/tz.xml';
        //$xmlTimeUrl = $this->moduleBaseLink.'xmls/timezone.xml';
        $timeZone = array();
        $error = "";
        if (function_exists('curl_init')) {
            try {
                $ch = curl_init($xmlTimeUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $data = curl_exec($ch);
                curl_close($ch);
                if(!empty($data)){
                    $xmlObject = new SimpleXmlElement($data, LIBXML_NOCDATA);
                     foreach ($xmlObject->time_zone as $value) {
                        $timeZone[(string)$value] = substr((string)$value,strpos((string)$value, '/')+1);
                    }
                }else{
                    $error = t('Error in getting time zone');
                    drupal_set_message('Error: '.$error,'error');
                }
            } catch (Exception $e) {
				$error = $e->getMessage();
				$errormsg = t('There is some error in WizIQ Service');
				
				drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
            }
        }  else {
            $error = t('Curl extention is not installed');
            drupal_set_message('Error: '.$error,'error');
        }
        asort($timeZone);
        return $timeZone;
    }
    
    //function to get languages from wiziq api
    function getLanguages(){
        $xmlLangUrl = 'http://class.api.wiziq.com/vc-language.xml';
        //$xmlLangUrl = $this->moduleBaseLink.'xmls/language.xml';
        $languages = array();
        $error = "";
        if (function_exists('curl_init')) {
            try {
                $ch = curl_init($xmlLangUrl);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                $data = curl_exec($ch);
                curl_close($ch);
                if(!empty($data)){
                    $xmlObject = new SimpleXmlElement($data, LIBXML_NOCDATA);
                    foreach ($xmlObject->virtual_classroom->languages->language as $value) {
                        $languages[(string)$value->language_culture_name] = (string)$value->display_name;
                    }
                }else{
                    $error = t('Error in getting languages');
					drupal_set_message('Error: '.$error,'error');
                }
            } catch (Exception $e) {
                $error = $e->getMessage();
                $errormsg = t('There is some error in WizIQ Service');
				drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
            }
        }  else {
            $error = t('Curl extention is not installed');
            drupal_set_message('Error: '.$error,'error');
        }
        asort($languages);
        return $languages;
    }
    // fetch credential from database
    function fetchcredential($userid)
    {
		$result = db_query('select 	api_url,secret_key,access_key,content_url,recurring_api_url from {wiziq_api_credentials}');
		foreach($result as $res)
		{	
			$data['api_url'] = $res->api_url;
			$data['sec_key'] = $res->secret_key;
			$data['access'] = $res->access_key;
			$data['content_url'] = $res->content_url;
			$data['recurring_api_url'] = $res->recurring_api_url;
		} 
		return $data;
	}
	// end fetch
	// create single & recurring class
	function createliveclass($data)
	{
		$in_var = $data['input'];
		global $user;
		$attribNode = '';
		$retcreden = fetchcredential($user->uid);
		
	    $str_dat = $in_var['class-start-date']['year'].'-'.$in_var['class-start-date']['month'].'-'.$in_var['class-start-date']['day'];
	 
  	    $hours = $in_var['hours'];
  
  	    $minutes = $in_var['minutes'];
  
     	// start time
	    $format = 'Y-m-d H:i:s';	 
        $date = DateTime::createFromFormat($format, $str_dat.' '.$hours.':'.$minutes.':00');
    
   
        $clstime = $date->format('m/d/Y H:i');
    
		require_once("AuthBase.php");
		$authBase = new AuthBase($retcreden['sec_key'],$retcreden['access']);
		$method = isset($in_var['recurring-check']) && $in_var['recurring-check'] != 0 ? "create_recurring" : "create";
		$requestParameters["signature"] = $authBase->GenerateSignature($method,$requestParameters);	 
	 
	 	$requestParameters["presenter_name"] = $user->name;
	 	$requestParameters["presenter_id"] = $user->uid;
		$requestParameters["app_version"] = "Drupal 1.0";
		if (!empty($in_var['right-now'])) {
			
            date_default_timezone_set($in_var['time-zone']);
            $clstime =  date('m/d/Y H:i:s');     //date('m/d/Y H:i:s', strtotime($data['class_time']));
        } else {
            $clstime = $date->format('m/d/Y H:i');
        }
        $requestParameters["start_time"] = $clstime;
		$requestParameters["title"] = $in_var['title']; //Required
		$requestParameters["duration"] = $in_var['duration']; //optional
		$requestParameters["time_zone"] = $in_var['time-zone']; //optional
		$requestParameters["attendee_limit"] = $in_var['attendee-limit']; //optional
		$requestParameters["control_category_id"] = ""; //optional
		$requestParameters["create_recording"] = $in_var['record-class']; //optional
		$requestParameters["return_url"] = ""; //optional
		$requestParameters["status_ping_url"] = ""; //optional
		$requestParameters["language_culture_name"] = $in_var['language'];
		
		if($method=='create_recurring')
		{
			
			$requestParameters["class_repeat_type"] = $in_var['class-schedule']; //Required  , value =   1
            if ($in_var['class-end'] == 0) {
                
                  $requestParameters["class_occurrence"] = $in_var['after-class']; //Required , value =  4
                //exit;
            } elseif ($in_var['class-end'] == 1) {
                  $requestParameters["class_end_date"] = $in_var['on-date']['year'].'-'.$in_var['on-date']['month'].'-'.$in_var['on-date']['day'];
                //exit;
                //date('m/d/Y', strtotime($data['class_end_date'])); //optional       , value = 2014-10-10 
            }

            if ($in_var['class-schedule'] == 4) {

                $requestParameters["specific_week"] = $in_var['repeat-every-week']; //Required  , value =   2
                $remove_space = implode(' ', $in_var['repeat-every-week-on']);
                $requestParameters["days_of_week"] = preg_replace('/[ ]+/', ',', trim($remove_space));
                
            }

            if ($in_var['class-schedule'] == 5) {

                if ($in_var['weekly-repeat-by'] == 'repeat_day') {

                    $requestParameters["monthly_day"] = $in_var['repeat-by-day-date'];
                    $requestParameters["monthly_week_day"] = $in_var['repeat-by-day-week'];
                    $requestParameters["rdo_by_day"] = "true";
                    
                } elseif ($in_var['weekly-repeat-by'] == 'repeat_date') {
					                    
                    $requestParameters["monthly_date"] = $in_var['repeat-by-date'];
                    $requestParameters["rdo_by_date"] = "true";                    
                    
                }
            }
		}		
		 
		$httpRequest=new Wiziq_HttpRequest();
		try
		{
			$XMLReturn=$httpRequest->wiziq_do_post_request($retcreden['api_url'].'?method='.$method,http_build_query($requestParameters, '', '&')); 
		}
		catch(Exception $e)
		{		  	  
	  		 $api_error = $e->getMessage();
		}
 		if(!empty($XMLReturn))
 		{
			$xmlr=new SimpleXMLElement($XMLReturn);		
 			try
			{
			  $objDOM = new DOMDocument();
			  $objDOM->loadXML($XMLReturn);	  
			}
			catch(Exception $e)
			{
			  $api_error = $e->getMessage();
			}						
			$status = $objDOM->getElementsByTagName("rsp")->item(0);
			$attribNode = $status->getAttribute("status");
		 }
	       if($attribNode == 'ok')
           {
		    $response['master_id'] = '';
            $methodTag = $objDOM->getElementsByTagName("method");
            $response['response_method'] = $methodTag->item(0)->nodeValue;
            $ourdata['response_method'] = $response['response_method'];
            if( $ourdata['response_method'] == 'create_recurring' )
              {
                $presenter_urlTag = $objDOM->getElementsByTagName("class_master_id");
                $response['master_id'] = $presenter_urlTag->item(0)->nodeValue;
                $data['master_id'] = $response['master_id'];
                
                $presenter_urlTag = $objDOM->getElementsByTagName("recurring_summary");
                $response['recurring_summary'] = $presenter_urlTag->item(0)->nodeValue;
              }
                $class_idTag = $objDOM->getElementsByTagName("class_id");
                $response['response_class_id'] = $class_idTag->item(0)->nodeValue;
                $data['response_class_id'] = $response['response_class_id'];
                
                $recording_urlTag = $objDOM->getElementsByTagName("recording_url");
                $response['response_recording_url'] = $recording_urlTag->item(0)->nodeValue;
                $data['response_recording_url'] = $response['response_recording_url'];

                $presenter_urlTag = $objDOM->getElementsByTagName("presenter_url");
                $response['response_presenter_url'] = $presenter_urlTag->item(0)->nodeValue;
                $data['response_presenter_url'] = $response['response_presenter_url'];
                
                $data['get_detail'] = isset($response['response_method']) && $response['response_method'] == 'create' ? 1 : 0 ;
                $data['is_recurring'] = isset($response['response_method']) && $response['response_method'] == 'create' ? 'FALSE' : 'TRUE' ;           
                $data['created_by'] = $requestParameters["presenter_id"];
               
                // Making an array for return data
                $ourdata['created_by'] = $data['created_by'];
                $ourdata['class_name'] = $in_var['title'];
                $ourdata['class_time'] = date("Y-m-d H:i:s" , strtotime($requestParameters["start_time"]));
                $ourdata['duration'] = $in_var['duration'];
                $ourdata['courseid'] = '1';
                $ourdata['classtimezone'] = $in_var['time-zone'];
                $ourdata['language'] = $in_var['language'];
                $ourdata['recordclass'] = $in_var['record-class'];
                $ourdata['attendee_limit'] = $in_var['attendee-limit'];
                $ourdata['response_class_id'] = $response['response_class_id'];
                $ourdata['response_recording_url'] = $response['response_recording_url'];
                $ourdata['response_presenter_url'] = $response['response_presenter_url'];
                $ourdata['status'] = 'upcoming';
                $ourdata['master_id'] = $response['master_id'];                
                $ourdata['get_detail'] = $data['get_detail'];
                $ourdata['is_recurring'] = $data['is_recurring'];                    
             } 
         elseif ($attribNode == "fail") {
			$error = $objDOM->getElementsByTagName("error")->item(0);
			$errorcode = $error->getAttribute("code");
			$errormsg = $error->getAttribute("msg");
			$ourdata['livestatus'] = 'fail';
			$ourdata['error'] = $errormsg;
			$ourdata['errorcode'] = $errorcode;  
			$display_msg = error_msg_display($errorcode,$errormsg);
			drupal_set_message($display_msg , 'error');
          } 
         else {
			$errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
          }
          $ourdata['ret_status'] = $attribNode;
        return $ourdata;		
		
	}
	// end create
	// update single class
	function updateliveclass($data)
	{
		$in_var = $data['input'];
		global $user;
		$attribNode = '';
		$retcreden=fetchcredential($user->uid);		     
    
		require_once("AuthBase.php");
		$authBase = new AuthBase($retcreden['sec_key'],$retcreden['access']);
		$method =  "modify";
		$requestParameters["signature"] = $authBase->GenerateSignature($method,$requestParameters);	 
	   
		
		$requestParameters["title"] = $in_var['title'];
		$requestParameters["duration"] = $in_var['duration'];	 
		$str_dat = $in_var['class-start-date']['year'].'-'.$in_var['class-start-date']['month'].'-'.$in_var['class-start-date']['day'];
		$hours = $in_var['hours'];
		$minutes = $in_var['minutes'];
		$format = 'Y-m-d H:i:s';
		$date = DateTime::createFromFormat($format, $str_dat.' '.$hours.':'.$minutes.':00');
		if (!empty($in_var['right-now'])) {
			
            date_default_timezone_set($in_var['time-zone']);
            $clstime =  date('m/d/Y H:i:s');     //date('m/d/Y H:i:s', strtotime($data['class_time']));
        } else {
            $clstime = $date->format('m/d/Y H:i');
        }
		
		$requestParameters["start_time"] = $clstime;
		$requestParameters["time_zone"] = $in_var['time-zone'];
		$requestParameters["attendee_limit"] = $in_var['attendee-limit'];
		$requestParameters["create_recording"] = $in_var['record-class'];
		$requestParameters["language_culture_name"] = $in_var['language'];	 
		$requestParameters["class_id"] = $in_var['response_class_id'];		
		$requestParameters["app_version"] = "Drupal 1.0";
	 			
	 	$httpRequest = new Wiziq_HttpRequest();
		try
		{
			$XMLReturn = $httpRequest->wiziq_do_post_request($retcreden['api_url'].'?method='.$method,http_build_query($requestParameters, '', '&')); 
		}
		catch(Exception $e)
		{		  	  
	  		 $api_error = $e->getMessage();
		}
 		if(!empty($XMLReturn))
 		{
			$xmlr = new SimpleXMLElement($XMLReturn);			 	
 			try
			{
			  $objDOM = new DOMDocument();
			  $objDOM->loadXML($XMLReturn);	  
			}
			catch(Exception $e)
			{
			  echo $e->getMessage();
			}						
			$status = $objDOM->getElementsByTagName("rsp")->item(0);
			$attribNode = $status->getAttribute("status");
		 }
	       if($attribNode=='ok')
           { 
                // Making an array for return data               
                $ourdata['class_name'] = $in_var['title'];
                $ourdata['class_time'] = date("Y-m-d H:i:s" , strtotime($requestParameters["start_time"]));
                $ourdata['duration'] = $in_var['duration'];              
                $ourdata['classtimezone'] = $in_var['time-zone'];
                $ourdata['language'] = $in_var['language'];
                $ourdata['recordclass'] = $in_var['record-class'];
                $ourdata['attendee_limit'] = $in_var['attendee-limit'];
                                 
             } 
         elseif ($attribNode == "fail") {
			 
            $error = $objDOM->getElementsByTagName("error")->item(0);
            $errorcode = $error->getAttribute("code");
            $errormsg = $error->getAttribute("msg");
            $ourdata['livestatus'] = 'fail';
            $ourdata['error'] = $errormsg;
            $ourdata['errorcode'] = $errorcode;                 
            $display_msg = error_msg_display($errorcode,$errormsg);
			drupal_set_message($display_msg , 'error');
           // redirect the user to class page with error
          } 
         else {
			$errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
/*
			if(isset($_SESSION['course_id'])){
				$course_id = $_SESSION['course_id'];
				drupal_goto('wiziq/classes', array('query' => array('course_id' => $course_id)));
			}
			else
				drupal_goto('wiziq/courses');
*/
          }
          $ourdata['ret_status'] = $attribNode;
        return $ourdata;		
		
	}
	// end update
	 function getRecurringDetail($master_id)
    {
		 
		global $user;
		/************** Get the credential details ************/
		$retcreden = fetchcredential($user->uid);
		$access_key = $retcreden['access'];
        $secretAcessKey = $retcreden['sec_key'];
        $webServiceUrl = $retcreden['api_url'];                
         //// Include API BASE File .....
        require_once("AuthBase.php");
        $authBase = new AuthBase($secretAcessKey, $access_key);
        $requestParameters["signature"] = $authBase->GenerateSignature('view_schedule', $requestParameters);
        $requestParameters["class_master_id"] = $master_id;
        $requestParameters["page_number"] = '1';
        $requestParameters["page_size"] = '60';
		$requestParameters["app_version"] = "Drupal 1.0";
        $httpRequest = new Wiziq_HttpRequest();
        try {
			
            $XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=view_schedule', http_build_query($requestParameters, '', '&'));
            libxml_use_internal_errors(true);
            $objdom = new SimpleXMLElement($XMLReturn, LIBXML_NOCDATA);            
            $attribnode = (string) $objdom->attributes();
            
        } catch (Exception $e) {
            $api_error = $e->getMessage();
        }

        if (!empty($XMLReturn)) {
			
            try {
                $objDOM = new DOMDocument();
                $objDOM->loadXML($XMLReturn);
            } catch (Exception $e) {
                $api_error = $e->getMessage();
            }
            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");
        }
        if ($attribNode == 'ok') {
            $attribnode = (string) $objdom->attributes();
            $recurringlist = $objdom->view_schedule->recurring_list;   
			
        } elseif ($attribNode == "fail") {

            $error = $objDOM->getElementsByTagName("error")->item(0);
            $errorcode = $error->getAttribute("code");
            $errormsg = $error->getAttribute("msg");
            $display_msg = error_msg_display($errorcode,$errormsg);
			drupal_set_message($display_msg , 'error');
            
        } else {
				$errormsg = t('There is some error in WizIQ Service');
				drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
        }


        return $recurringlist;
    
	}
	
	/********** Check for course id in database **************/
	function check_courseid($course_id)
	{
		$result = db_query('SELECT a.id,a.created_by,a.fullname,a.description,a.startdate,a.enddate,b.name FROM {wiziq_courses} as a left join {users} as b on a.created_by=b.uid WHERE a.id = :id', array(':id' => $course_id));
		$record = $result->fetchObject(); 
		return $record;
	}
	/************* Update all the values in DB with get_data api response*************/
	function getData($class_id){		 
		//exit;
		global $user;
		/************** Get the credential details ************/
	  	$retcreden = fetchcredential($user->uid); 
		$access_key = $retcreden['access'];
        $secretAcessKey = $retcreden['sec_key'];
        $webServiceUrl = $retcreden['api_url'];  
		//// Include API BASE File ....
        require_once("AuthBase.php");
        $authBase = new AuthBase($secretAcessKey, $access_key);
		$requestParameters["signature"] = $authBase->GenerateSignature('get_data', $requestParameters);
		$requestParameters["class_id"] = $class_id;
		$requestParameters["app_version"] = "Drupal 1.0";
        $requestParameters["columns"] = "presenter_id, presenter_name,presenter_url, start_time,
                            time_zone, create_recording, status, language_culture_name,
                            duration, recording_url,is_recurring,class_master_id,title,class_recording_status,attendance_report_status , attendee_limit";
        
		$httpRequest = new Wiziq_HttpRequest();
		try {
			$XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=get_data', http_build_query($requestParameters, '', '&'));
            //libxml_use_internal_errors(true);
		} catch (Exception $e) {
			$errormsg = $e->getMessage();
			$api_error = $e->getMessage();
        }
		if (!empty($XMLReturn)) {
			
			try {
				$objdom = new SimpleXMLElement($XMLReturn);
				$attribNode = $objdom->attributes();
					
				if ($attribNode == 'ok') {
					$response = $objdom->get_data->record_list->record;
					
					/************** Updateing all the values in database ***********/
					
					db_update('wiziq_wclasses')
					->fields(array(
					  'class_name' => $response->title,
					  'class_time' => date("Y-m-d H:i:s" , strtotime($response->start_time)) ,
					  'duration'   => $response->duration,
					  'classtimezone' => $response->time_zone,
					  'language' => $response->language_culture_name,
					  'recordclass' => $response->create_recording,
					  'attendee_limit' => $response->attendee_limit,
					  'response_recording_url' => $response->recording_url,
					  'response_presenter_url' => $response->presenter_url,
					  'status' => $response->status,
					  'master_id' => $response->class_master_id,
					  'attendence_report' => $response->attendance_report_status,
					
					))
				  ->condition('response_class_id', $class_id, '=')
				  ->execute();				  
				   
				} elseif ($attribNode == "fail") {
					$errormsg = $objdom->error->attributes()->msg;
					$errorcode = $objdom->error->attributes()->code;
					drupal_set_message(t('Error: '.$errormsg.'<br>Errorcode: '.$errorcode),'error');
					$display_msg = error_msg_display($errorcode,$errormsg);
					drupal_set_message($display_msg , 'error');
				 }
			} catch (Exception $e) {
				$errormsg = $e->getMessage();
			}
		}
		if (isset($api_error))  {
			$errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
		}
	}

/*************** get Class details having some resp0onse class ID *******************/
function getClassDetail($class_id)
{
	$query = db_select('wiziq_wclasses','a')
	->fields('a');  
	$query->leftJoin('users', 'b', 'b.uid = a.created_by');        
	$query->fields('b', array ('name'));
	$query->condition('response_class_id',$class_id, '=');
	$res = $query->execute(); 
	$result = $res->fetchObject();
	return $result;
}

/*********************** Function to delete classes from wiziq server *****************/
function delete_class($class_id){
        global $user;
		/************** Get the credential details ************/
		$retcreden = fetchcredential($user->uid);
		$access_key = $retcreden['access'];
        $secretAcessKey = $retcreden['sec_key'];
        $webServiceUrl = $retcreden['api_url'];
		//// Include API BASE File ....
        require_once("AuthBase.php");
        $authBase = new AuthBase($secretAcessKey, $access_key);
		$requestParameters["signature"] = $authBase->GenerateSignature('cancel', $requestParameters);
		$requestParameters["class_id"] = $class_id;
		$requestParameters["app_version"] = "Drupal 1.0";
        $httpRequest = new Wiziq_HttpRequest();
        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=cancel', http_build_query($requestParameters, '', '&'));
        } catch (Exception $e) {
            $e->getMessage();
            $api_error = $e->getMessage();
        }
        
        if (!empty($XMLReturn)) {
            try {
				$objDOM = new DOMDocument();
				$xyz =  $objDOM->loadXML($XMLReturn);
				$xml = new SimpleXMLElement($XMLReturn);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
            $status = $objDOM->getElementsByTagName("rsp")->item(0);
            $attribNode = $status->getAttribute("status");
            if ($attribNode == "ok") {
				$cancelTag = $objDOM->getElementsByTagName("cancel")->item(0);
				$return['status'] = 'ok';
				$return['canceltag'] = $cancelTag;
            }elseif ($attribNode == "fail") {
				$errors = $objDOM->getElementsByTagName("error")->item(0);
				$errorcode = $errors->getAttribute("code");
				$errormsg = $errors->getAttribute("msg");
				$return['status'] = $attribNode;
				$return['errorcode'] = $errorcode;
				$return['errormsg']	= $errormsg;
				$display_msg = error_msg_display($errorcode,$errormsg);
				drupal_set_message($display_msg , 'error');
				}
			else{
				$errormsg = t('There is some error in WizIQ Service');
				drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
				}
			
	
		}
	return $return;
}



/*********************** Function to get attendee report from wiziq server *****************/
function getAttendance_report ( $class_id ) {
        global $user;
		/************** Get the credential details ************/
		$retcreden = fetchcredential($user->uid);
		$access_key = $retcreden['access'];
        $secretAcessKey = $retcreden['sec_key'];
        $webServiceUrl = $retcreden['api_url'];
		//// Include API BASE File ....
		require_once("AuthBase.php");
		$authBase = new authBase($secretAcessKey,$access_key);
        $method = "get_attendance_report";
        $requestParameters["signature"] = $authBase->generateSignature($method, $requestParameters);
        $requestParameters["class_id"] = $class_id;
        $requestParameters["app_version"] = "Drupal 1.0";
        $httpRequest = new Wiziq_HttpRequest();
        try {
            $XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=get_attendance_report', http_build_query($requestParameters, '', '&'));
            $attendancexml = new SimpleXMLElement($XMLReturn);
            $attendancexmlstatus = $attendancexml->attributes();
            if ($attendancexmlstatus == 'ok') {
                $attendancexmlch = $attendancexml->get_attendance_report;
                $attendancexmlchstatus = $attendancexmlch->attributes();
                 if ($attendancexmlchstatus == 'true') {
                    $attendancexmlchdur = $attendancexmlch->class_duration;
                    $attendancexmlchattlist = $attendancexmlch->attendee_list->attendee;
                    return $attendancexmlchattlist;
                }
            } elseif ($attendancexmlstatus == "fail") {
                $errorcode =  $attendancexml->error->attributes()->code;
                $errormsg =  $attendancexml->error->attributes()->msg;
                drupal_set_message(t('Error: '.$errormsg.'<br>Errorcode: '.$errorcode),'error');
                $display_msg = error_msg_display($errorcode,$errormsg);
				drupal_set_message($display_msg , 'error');
				if(isset($_SESSION['course_id'])){
					$course_id = $_SESSION['course_id'];
					drupal_goto('wiziq/classes', array('query' => array('course_id' => $course_id)));
				}
			} 
        }catch (Exception $e) {
            $error =  $e->getMessage();
            $errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');

	}
}

function getdownloadlink($class_id) {
		global $user;
		/************** Get the credential details ************/
		$retcreden=fetchcredential($user->uid);
		$access_key = $retcreden['access'];
        $secretAcessKey = $retcreden['sec_key'];
        $webServiceUrl = $retcreden['api_url'];
		//// Include API BASE File ....
		require_once("AuthBase.php");
		$authBase = new authBase($secretAcessKey,$access_key);
		$requestParameters["signature"] = $authBase->generateSignature('download_recording', $requestParameters);
		$requestParameters["class_id"] = $class_id;
		$requestParameters["app_version"] = "Drupal 1.0";
        $curos = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (strstr($curos, "win")) {
            $format = 'exe';
            $requestParameters["recording_format"] = "exe";
        } else {
            $format = 'zip';
            $requestParameters["recording_format"] = "zip";
        }
       	$httpRequest = new Wiziq_HttpRequest();
		try {
				$XMLReturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=download_recording', http_build_query($requestParameters, '', '&'));
			} 
		catch (Exception $e) {
			$this->errormsg .= $e->getMessage();		
       		 }

		if (!empty($XMLReturn)) {
            try {
				$objdom = new SimpleXMLElement($XMLReturn);
				$attribNode = $objdom->attributes();

            } catch (Exception $e) {
            $e->getMessage();
            $api_error = $e->getMessage();
            }

		if ($attribNode == 'ok') {

                $status_xml_path = $objdom->download_recording->status_xml_path;
                $xml1  = simplexml_load_file($status_xml_path);
                if ($xml1->download_recording->download_status == 'true') {
                    $record = $xml1->download_recording->recording_download_path;

					/************** Updateing all the values in database ***********/
					
					db_update('wiziq_wclasses')
					->fields(array(
					  'download_recording' => $record[0] ,
					))
				  ->condition('response_class_id', $class_id, '=')
				  ->execute();
                }
			
            } elseif ($attribNode == "fail") {
		$errormsg = $objdom->error->attributes()->msg;
		$errorcode = $objdom->error->attributes()->code;
		drupal_set_message(t('Error: '.$errormsg.'<br>Errorcode: '.$errorcode),'error');
		$display_msg = error_msg_display($errorcode,$errormsg);
		drupal_set_message($display_msg , 'error');
			}
                   }//end if
		        if ( isset($api_error) && $api_error )  {
					$errormsg = t('There is some error in WizIQ Service');
					drupal_set_message(t('Error: '.$errormsg.'<br>'.$api_error),'error');
    
				}
}


/*
 * Function to add attendees in the class
 * @since 1.0
*/
function wiziq_addattendee( $classid , $languageculturename) {
			global $user;
			$attendeescreenname = $user->name;
			$attendeeid = $user->uid;
			
			/************** Get the credential details ************/
			$retcreden = fetchcredential($user->uid);
			$access_key = $retcreden['access'];
			$secretAcessKey = $retcreden['sec_key'];
			$webServiceUrl = $retcreden['api_url'];
			//// Include API BASE File ....
			require_once("AuthBase.php");
			$authBase = new authBase($secretAcessKey,$access_key);
			
			$method = "add_attendees";
			$XMLAttendee="<attendee_list>
			<attendee>
				<attendee_id><![CDATA[$attendeeid]]></attendee_id>
				<screen_name><![CDATA[$attendeescreenname]]></screen_name>
				<language_culture_name><![CDATA[$languageculturename]]></language_culture_name>
			</attendee>
			</attendee_list>";
			
			$requestParameters["signature"] = $authBase->generateSignature($method, $requestParameters);
			$requestParameters["class_id"] = $classid;//required
			$requestParameters["attendee_list"]=$XMLAttendee;
			$requestParameters["app_version"] = "Drupal 1.0";
			$httpRequest = new Wiziq_HttpRequest();
			try
			{
				$XMLReturn=$httpRequest->wiziq_do_post_request($webServiceUrl.'?method=add_attendees',http_build_query($requestParameters, '', '&')); 
			}
			catch(Exception $e)
			{	
				$error = $e->getMessage();
				$errormsg = t('There is some error in WizIQ Service');
				drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');
			}
			if(!empty($XMLReturn))
			{
				try
				{
				  $objDOM = new DOMDocument();
				  $objDOM->loadXML($XMLReturn);
				}
				catch(Exception $e)
				{
				  $error = $e->getMessage();
				  $errormsg = t('There is some error in WizIQ Service');
				  drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');
				}
				$status=$objDOM->getElementsByTagName("rsp")->item(0);
				$attribNode = $status->getAttribute("status");
				if($attribNode == "ok")
				{
					$methodTag = $objDOM->getElementsByTagName("method");
					$method = $methodTag->item(0)->nodeValue;
					
					$class_idTag = $objDOM->getElementsByTagName("class_id");
					$class_id = $class_idTag->item(0)->nodeValue;
					
					$add_attendeesTag=$objDOM->getElementsByTagName("add_attendees")->item(0);
					$add_attendeesStatus = $add_attendeesTag->getAttribute("status");
					
					$attendeeTag = $objDOM->getElementsByTagName("attendee");
					$length = $attendeeTag->length;
					for($i=0;$i<$length;$i++)
					{
						$attendee_idTag = $objDOM->getElementsByTagName("attendee_id");
						$attendee_id = $attendee_idTag->item($i)->nodeValue;
						
						$attendee_urlTag = $objDOM->getElementsByTagName("attendee_url");
						$attendee_url = $attendee_urlTag->item($i)->nodeValue;
						return $attendee_url;
					}				
				}
				elseif($attribNode == "fail")
				{
					$error = $objDOM->getElementsByTagName("error")->item(0);
					$errorcode = $error->getAttribute("code");	
					$errormsg = $error->getAttribute("msg");
					$display_msg = error_msg_display($errorcode,$errormsg);
					drupal_set_message($display_msg , 'error');
			    } 
			}
		}// end function to add attendees to a class
    
 /*
 * Date time function to get live status
 * @since 1.0
 */ 
	function wiziq_get_datetime ( $classtime, $duration, $timezone ) {
		date_default_timezone_set( $timezone );
		$currenttime = date('Y-m-d H:i:s');
		$durationsec = $classtime.'+'.$duration.' minutes'; 
		$new_time = date('Y-m-d H:i:s', strtotime($durationsec));
		if((strtotime($currenttime) >= strtotime($classtime)) && (strtotime($currenttime) <= strtotime($new_time))){
			return true;
		}
		else  {
			return false;
		}
	}//end function to get live status 
/*  save folder */
function wiziq_create_folder($folderpath, $foldername) {
			
			global $user ;
			require_once("AuthBase.php"); 
            
            $retcreden=fetchcredential($user->uid);
			$wiziqaccesskey = $retcreden['access'];
			$wiziqsecretacesskey = $retcreden['sec_key'];
		 	$wiziqcontentwebservice = $retcreden['content_url'];		
	  			
    $requestparameters = array();
    $wiziqauthbase = new authBase($wiziqsecretacesskey, $wiziqaccesskey);
    $method = "create_folder";
    $requestparameters["signature"] = $wiziqauthbase->generateSignature($method,$requestparameters);
    $requestparameters["presenter_id"] = $user->uid;
    $requestparameters["app_version"] = "Drupal 1.0";
    if (!empty($folderpath)) {
        $folderpath = $folderpath."/".$foldername;
        $requestparameters["folder_path"] = $folderpath;
    } else {
        $requestparameters["folder_path"] = $foldername;
    }
    $requestparameters["presenter_name"] = $user->name;
    $wiziqhttprequest = new Wiziq_HttpRequest();
    try {
		 $xmlreturn = $wiziqhttprequest->wiziq_do_post_request($wiziqcontentwebservice."?method=$method" , http_build_query($requestparameters, '', '&'));
          
        $createfolderxml = new SimpleXMLElement($xmlreturn);

        $createfolderxmlstatus = $createfolderxml->attributes();
        if ($createfolderxmlstatus == 'ok') {
            $createfolderxmlch = $createfolderxml->create_folder;
            $att = 'status';
          $createfolderxmlstatus1['res'] = (string)$createfolderxmlch->attributes()->$att;
            
            $createfolderxmlstatus1['path'] = (string)$createfolderxmlch->folder->path;
            if ($createfolderxmlstatus1['res'] == true) {
                return $createfolderxmlstatus1;
            } else {
                $unabletocreate = t('unable to create folder') ;
                drupal_set_message($unabletocreate,'error');
            }
        } else {
                $unabletocreate =  t('unable to create folder') ;
                drupal_set_message($unabletocreate,'error');
        }
    } catch (Exception $e) {
        $error = $e->getMessage();
        $errormsg = t('There is some error in WizIQ Service');
		drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');
    }
    return $createfolderxml;
}
// end saving
/*
 * Function to upload content  for frontend and backend
 * @since 1.0
 */  
function wiziq_content_upload($uploaddata ,$filedata,$folderpath) {
		global $user;
		$retcreden = fetchcredential($user->uid);
		$access_key = $retcreden['access'];
		$secretAcessKey = $retcreden['sec_key'];
		$wiziqcontentwebservice = $retcreden['content_url'];		
                 
		require_once("AuthBase.php");  			
		$requestparameters = array();
		$wiziqauthbase = new authBase($secretAcessKey, $access_key);    
		$method = "upload";
		$requestparameters["signature"] = $wiziqauthbase->generateSignature($method,$requestparameters); 			  			
		if (!empty($uploaddata['file_title'])){
			$requestparameters["title"] = $uploaddata['file_title'];
		} else {
			$filename = array();
			$filename = explode(".", $filedata->filename);
			$requestparameters["title"] = $filename['0'];
		}
		if (!empty($folderpath)) {
			$requestparameters["folder_path"] = $folderpath;
		}
		$requestparameters['presenter_id'] = $user->uid;
		
		$requestparameters["presenter_name"] = $user->name;
		$requestparameters["app_version"] = "Drupal 1.0";
		$content = file_get_contents($filedata->uri);
		 
		//$filefieldname = (array_keys($filedata));
		$delimiter = '-------------' . uniqid();
		$filefields = array(
			'file1' => array(
			'name' =>$filedata->filename,			 
			'type' => pathinfo($filedata->filename, PATHINFO_EXTENSION),
			'content' => $content),
			);					
		$data = '';
		foreach ($requestparameters as $name => $value) {
			$data .= "--" . $delimiter . "\r\n";
			$data .= 'Content-Disposition: form-data; name="' . $name . '";' . "\r\n\r\n";
			// note: double endline
			$data .= $value . "\r\n";
		}
		foreach ($filefields as $name => $file) {
			$data .= "--" . $delimiter . "\r\n";
			// "filename" attribute is not essential; server-side scripts may use it
			$data .= 'Content-Disposition: form-data; name="uploadingfile";' .
			' filename="' . $file['name'] . '"' . "\r\n";
			// this is, again, informative only; good practice to include though
			$data .= 'Content-Type: ' . $file['type'] . "\r\n";
			// this endline must be here to indicate end of headers
			$data .= "\r\n";
			// the file itself (note: there's no encoding of any kind)
			$data .= $file['content'];
		}
		$data .= "\r\n"."--" . $delimiter . "--\r\n";
		$str = $data;
		// set up cURL
		$ch=curl_init($wiziqcontentwebservice."?method=upload");
		curl_setopt_array($ch, array(
			CURLOPT_HEADER => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_HTTPHEADER => array( // we need to send these two headers
				'Content-Type: multipart/form-data; boundary='.$delimiter,
				'Content-Length: '.strlen($str)
			),
			CURLOPT_POSTFIELDS => $data,
		));
		$ress =   curl_exec($ch);
		curl_close($ch);
		try {
			$contentupload = new SimpleXMLElement($ress);
			
			$contentupload_rsp = $contentupload->attributes();
			if ($contentupload_rsp == "ok") {
				$contentupload_status = $contentupload->upload->attributes();
				if ($contentupload_status == "true") {
					$content_details = $contentupload->upload->content_details; 
					$response['livestatus'] = 'ok';
					$response['content_id'] = (string)$content_details->content_id;
					$response['file_name'] = $requestparameters["title"];
				} 
			} elseif ($contentupload_rsp == "fail") {
					$errorcode = $contentupload->error->attributes()->code;
					$errormsg = $contentupload->error->attributes()->msg;
					$response['livestatus'] = 'fail';
					$response['errorcode'] = $errorcode;
					$response['errormsg'] = $errormsg;
					$display_msg = error_msg_display($errorcode,$errormsg);
					drupal_set_message($display_msg , 'error');
			 }
		}catch (Exception $e) {
			$error = $e->getMessage();
			$errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');
		}
		return $response;
	}
	
/************ Drupal Default Breadcrumb for content ***********/
function wiziq_breadcrumb($id)
{	
	$breadcrumb = array();
	do{
	$result=db_query("SELECT parent_id,name,id from {wiziq_contents} where id = (:ids)", array(':ids' => $id));	 		 
	$data = $result->fetchObject();
	if(!empty($data)){
		$id = $data->parent_id; 
		$name = $data->name;
		array_push($breadcrumb,$data);
	}
	else
		$id = 0;
	}
	while($id > 0);
	$breadcrumb = array_reverse($breadcrumb);
	return $breadcrumb;	
}
/**
     * Deletes folder for wiziq
     *
     * @param string $folderdata path of the folder.
     * 
     * @return boolean $deletefolderxmlch_status status is true if folder is deleted.
     */
function wiziq_delete_folder($folderpath,$file_fol) {

		global $user;
		$retcreden = fetchcredential($user->uid);
		$access_key = $retcreden['access'];
		$secretAcessKey = $retcreden['sec_key'];
		$webServiceUrl = $retcreden['content_url'];		 
		require_once("AuthBase.php"); 
		
		$requestparameters = array();
		$authBase = new authBase($secretAcessKey, $access_key);
		$httpRequest = new Wiziq_HttpRequest();
		$response=array();
        
        if($file_fol=='folder')
        {
			$method = "delete_folder";
			$requestparameters["signature"] = $authBase->generateSignature($method, $requestparameters);
			$requestparameters["presenter_id"] = $user->uid;
			$requestparameters["presenter_name"] = $user->name;
			$requestparameters["app_version"] = "Drupal 1.0";
			if (!empty($folderpath)) {
				$requestparameters["folder_path"] = $folderpath;
			}	 
			$requestparameters["presenter_name"] = $user->name;
	 			
			try {
				$xmlreturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=delete_folder', http_build_query($requestparameters, '', '&'));

			//	libxml_use_internal_errors(true);
				
				$deletefolderxml = new SimpleXMLElement($xmlreturn);

				$deletefolderxml_status = $deletefolderxml->attributes();
				$deletefolderxmlch = $deletefolderxml->delete_folder;
				if ($deletefolderxml_status == 'ok') {
					$response['livestatus'] = 'ok';
					// $att = 'status';
					//$deletexml_status = (string)$deletefolderxmlch->attributes()->$att;
				} elseif ($deletefolderxml_status == "fail") {

					$response['livestatus'] = 'fail';
					$errorcode = (string) $deletefolderxml->error->attributes()->code;                 
					$errormsg = (string) $deletefolderxml->error->attributes()->msg;                 
					$response['error'] ="Error Code ". $errorcode.','.$errormsg;
					$display_msg = error_msg_display($errorcode,$errormsg);
					drupal_set_message($display_msg , 'error');
				}
			} catch (Exception $e) {
				$response['livestatus'] = 'fail';
				$error = $e->getMessage();
				$response['error'] = 'Error :'.$error;
				 
			}
		}
		else
		{
		   $method = "delete";
		   $requestparameters["signature"] = $authBase->generateSignature($method, $requestparameters);
				$requestparameters["content_id"] = $folderpath;
				 
				try {
					//Call to api method and pass request parameters
					$xmlreturn = $httpRequest->wiziq_do_post_request($webServiceUrl . '?method=delete', http_build_query($requestparameters, '', '&'));
					 
				$deletexml = new SimpleXMLElement($xmlreturn);
					$deletexmlstatus = $deletexml->attributes();
					if ($deletexmlstatus == 'ok') {
						$deletexmlch = $deletexml->delete;
						$att = 'status';
						$deletexmlchstatus = $deletexmlch->attributes()->$att;
						if ($deletexmlchstatus == 'true') {
							 $response['livestatus'] = 'ok';
						}
					} elseif ($deletexmlstatus == "fail") { 
						$errorcode = $deletexml->error->attributes()->code;
						$errormsg = $deletexml->error->attributes()->msg;
						$response['error']='Error Code '.$errorcode.','.$errormsg;
						$display_msg = error_msg_display($errorcode,$errormsg);
						drupal_set_message($display_msg , 'error');
					}
				}
				catch ( Exception $e ) {
					$response['livestatus'] = 'fail';
					$error = $e->getMessage();
					$response['error'] = 'Error :'.$error;
				}
	   }
        return $response;
	}
/*
 * Gets the content status if it is inprogress,failed or available.
 *
 * @param string $folderpath path of the folder.
 * @param string $foldername name of the folder.
 * @param integer $courseid id of the course.
*/

function content_refresh($id){
		global $user;
		$folderpath = get_Content_by_id($id);

		$method = "list";
		if ( !empty($folderpath->folderpath)) {
			$requestparameters["folder_path"] = $folderpath->folderpath;
		}
		$requestparameters["page_size"] = '60';
		$requestparameters['presenter_id'] = $user->uid;
		$requestparameters["presenter_name"] = $user->name;
		$requestparameters["app_version"] = "Drupal 1.0";
		 try {
			 //Call to method to refresh the status of files
			$contentlistxml = content_method($requestparameters , $method );
			$contentlistxmlstatus = $contentlistxml->attributes();
			if ($contentlistxmlstatus == 'ok') {
                    $recordlist = $contentlistxml->list->record_list;
                    foreach ($recordlist->children() as $value) {
                        $status =  $value->status;
                        $valuecontentid =  $value->content_id;
                        if ( "available" == $status )  {
							//Update the status if available
							db_update('wiziq_contents')
							->fields(array(
								'status' => $status
							))
						  ->condition('content_id', $valuecontentid, '=')
						  ->execute();
						} elseif ( "failed" == $status ) {
							//delete if status if failed 
							db_delete('wiziq_contents')
							->condition('content_id', $valuecontentid, '=')
							->execute();
						}
					}
				}
		}
		catch (Exception $e) {
			$error = $e->getMessage();
			$errormsg = t('There is some error in WizIQ Service');
			drupal_set_message(t('Error: '.$errormsg.'<br>'.$error),'error');
		}
	}
function content_method($requestParameters , $method ){ 
	
			global $user;
			$retcreden = fetchcredential($user->uid);
			$access_key = $retcreden['access'];
			$secretAcessKey = $retcreden['sec_key'];
			$wiziqcontentwebservice = $retcreden['content_url'];
			require_once("AuthBase.php");
			$requestparameters = array();
			$authBase = new authBase($secretAcessKey, $access_key);
			$requestparameters["signature"] = $authBase->generateSignature($method, $requestparameters);
			if( !empty ( $requestParameters ) )
			{
				foreach($requestParameters as $key=>$value)
				{
					$requestparameters[$key] = $value;
				}
			}
			$httpRequest = new Wiziq_HttpRequest();
			$xmlreturn = $httpRequest->wiziq_do_post_request($wiziqcontentwebservice."?method=$method" , http_build_query($requestparameters, '', '&'));
			$createfolderxml = new SimpleXMLElement($xmlreturn);
			return $createfolderxml;
}

function get_Content_by_id($id)
{
	$result = db_query('select folderpath from {wiziq_contents} where id=:id',array(':id' => $id));
	return $result->fetchObject();
}

function check_parentId($id)
{
	$result = db_query("SELECT * from {wiziq_contents} where id = (:ids)", array(':ids' => $id));	 		 
	$data = $result->fetchObject();
	return $data;
}
function get_EnrollUser($user_id,$course_id)
{
	$result = db_query("SELECT * from {wiziq_enroluser} where user_id = (:ids) AND course_id = (:cid)", array(':ids' => $user_id,':cid' => $course_id ));	 		 
	$data = $result->fetchObject();
	return $data;

}
function get_classbyid($class_id)
{
	$result = db_query('select * from {wiziq_wclasses} where id=:id' , array(':id' => $class_id));
	$data = $result->fetchObject();
	return $data;
}
function get_classbyclassid($class_id)
{
	$result = db_query('select * from {wiziq_wclasses} where response_class_id=:id' , array(':id' => $class_id));
	$data = $result->fetchObject();
	return $data;
}
 
 

function error_msg_display($code,$errormsg)
{ 
	$error = t($code);
	$default = language_default();
	if ($default->language != 'en' ){
		if($error == $code)
			$msg = t('WIZIQ_COMMON_ERROR_MSG');
		else
			$msg = t($code);
		}
	else
	{
		//$msg = 'WIZIQ_COMMON_ERROR_MSG'.$code;
		if($code != '')
		$msg = eval('return WIZIQ_COM_'.$code.';');
		else
		$msg = $errormsg;
		
	}
	return $msg;
}
 
 
?>
