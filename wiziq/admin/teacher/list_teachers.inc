<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
/********** Include js ************/
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
/********** Include js ************/
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
 
function list_teachers_form($form, &$form_state)
{	  
	$form['create_teacher'] = array(
		'#type' => 'submit',
		'#value' => t('Add Teacher'),
		'#submit' => array('create_teacher'),	 
		);
   	
  global $user; 
$userid = $user->uid; 
$header = array(
        array('data' =>array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall','class'=>array('chkselall')))),
        array('data' => t('Teacher Name')),
        array('data' => t('Teacher Email')),
        array('data' => t('Teacher Password')),
        array('data' => t('Can Schedule Class')),
        array('data' => t('Activate')),
        array('data' => t('Manage Teacher')),
 );	 
 

// select fields for prefields
//$result = db_query('select a.id,a.fullname,a.description,a.created_by,b.name from {wiziq_courses} as a left join {users} as b on a.created_by=b.uid');
$query = db_select('wiziq_teacher','a')
        ->fields('a', array('id','password', 'can_schedule_class', 'is_active','teacher_id','puser_id'));  
  $query->leftJoin('users', 'b', 'b.uid = a.puser_id');            
  $query->fields('b', array ('name','mail'));
     
 $table_sort = $query->extend('TableSort') // Add table sort extender.

                     ->orderByHeader($header); // Add order by headers.

 $pager = $table_sort->extend('PagerDefault')

                     ->limit(5); // 5 rows per page.
        
$result = $pager->execute();   

 
   
 $rows = array();
  
foreach($result as $res)
{
	if($res->can_schedule_class == 1)
	$schcls = 'Yes';
	else
	$schcls = 'No';
	
	if($res->is_active==1)
	$activ = 'Yes';
	else
	$activ = 'No';
	 
	$form['checkboxes'] = array('#type' => 'checkbox','#attributes' => array('id'=>$res->id,'name'=>'chkval[]','value' =>$res->id,'class'=>array('chksingle')));
	$rows[]= array( 
	   // drupal_render(array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall'))),
	    drupal_render($form['checkboxes']),
          t($res->name),
          t($res->mail), 
          base64_decode($res->password),          
          $schcls,    
          $activ,
          l('Edit','admin/wiziq/teacher_ins',array('query' => array('edit_id'=>$res->id,'teacher_id' => $res->teacher_id,'user_id'=>$res->puser_id))), 
       );    
}
// end select fields
      $form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      ),
      array(
        '#theme' => 'pager',
      ));     
  return $form;  
}
 
function create_teacher($form, &$form_state)
{
	drupal_goto('admin/wiziq/teacher_ins');
}
 
 
