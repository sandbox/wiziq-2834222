<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';	
/********** Include js ************/
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');

function teachers_form()
{ 
	if(isset($_REQUEST['edit_id']) && !empty($_REQUEST['edit_id']))
	{
		$editid = $_REQUEST['edit_id'];
	    $data = db_query("select * from {wiziq_teacher} where id =:nids", array(':nids' => $editid))->fetchAll();	   
	    $user_id = $data[0]->puser_id;
	    $teacher_id = $data[0]->teacher_id;	                 
	    $pass_word = base64_decode($data[0]->password);
        $time_zone = $data[0]->timezone;	                 
	    $abt_teacher = $data[0]->about_the_teacher;	                 
	     
	    if($data[0]->can_schedule_class == '1')
	    $sch_class = 1;
	    else	                 
	    $sch_class = 0;	     
	     	               
	    if($data[0]->is_active == '1')
	    $is_act = 1;
	    else	                 
	    $is_act = 0;		                     
	}
	else
	{
		$editid = -1;
		$teacher_id = -1;
		$user_id = -1;
		$pass_word = '';
		$time_zone = '';
		$sch_class = 1;
		$abt_teacher = '';
		$is_act = 1;
    }
     
     $form['id'] = array(

	'#value' => $editid,

	'#type' => 'hidden',

	);
	
	$form['teacher_id'] = array(

	'#value' => $teacher_id,

	'#type' => 'hidden',

	);
  if($editid == -1 || empty($editid))
  {	    
	   $user_arr = entity_load('user');
	   $user_all = array();
	   foreach($user_arr as $usr_arr)
	   {
		  if(!empty($usr_arr->name))
		  $user_all[$usr_arr->uid] = $usr_arr->name; 
	   }   
		 
	   $form['name'] = array(
		'#type' => 'select',
		
		'#title' => t('Select User'),
		
		'#options' => $user_all,            

		'#attributes' => array('onchange' => 'disable_radio(this.value);')
	  );
  }
  else
  {
	 $user_name = user_load($user_id); 
	 $form['user_id'] = array(

		'#value' => $user_id,

		'#type' => 'hidden',

	 );
	
	   $form['name'] = array(
		'#type' => 'textfield',
		
		'#title' => t('Teacher Name'),
		
		'#attributes' => array('readonly'=>true),
		
		'#value' => $user_name->name,            

	  );
	 
  }
	 $form['password'] = array(
	
	'#type' => 'password',
	
	'#title' => t('Password'),
	
	'#required' => TRUE, 
		 
	'#attributes' => array('value' => $pass_word),
	
    '#size' => 30, 
    
    '#maxlength' => 128,    
      
	);
	
 $outzon = getTimeZone(); 
  // output from XML
  
  $form['time-zone'] = array(
   
    '#type' => 'select',
    
    '#title' => 'Time Zone',
    
    '#default_value'=>$time_zone,
    
    '#options' => $outzon,
    
  );
  
   $form['schedule-yes']['schedule-no'] = array(
    
     '#type' => 'radios',
    
     '#title' => t('Can Schedule Class'),
     
     '#default_value' => $sch_class,
    
     '#options' =>  array('1' => t('Yes'), '0' => t('No')),         
     
  );
  
   $form['active-yes']['active-no'] = array(
    
     '#type' => 'radios',
    
     '#title' => t('Active'),
     
     '#default_value' => $is_act,
    
     '#options' =>  array('1' => t('Yes'), '0' => t('No')),   
     
  );
  
    $form['about_the_teacher'] = array(
	
	'#type' => 'textarea',
	
	'#title' => t('About the teacher'),
	
	'#default_value'=>$abt_teacher,
		 
	);	 
	
 $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Submit'),                   
        '#submit' => array('teacher_submit_but_form'),               
     );
    
    $form['cancel'] = array(
    
    '#type' => 'submit',
    
    '#submit' => array('teacher_form_cancel'),
    
    '#value' => t('Cancel'),   
    
    '#limit_validation_errors' => array(),  
       
    );
   
   global $user;
   $form['admin_check'] = array(
    
    '#type' => 'hidden',
    
    '#value' => $user->uid,   
    );
    $form['onload_admin_check'] = array(
    
    '#type' => 'hidden',
    
    '#value' => $editid,   
    );
    
	return $form;
} 
/** validation  */
function teachers_form_validate($form, &$form_state)
{
    
   
} 
 /* End Validation */ 
function teacher_submit_but_form($form, &$form_state)
{
	    global $user;	          
        $requestparameters["password"]            = $form_state['values']['password'];
        $requestparameters["timezone"]            = $form_state['values']['time-zone'];
        $requestparameters["can_schedule_class"]  = $form_state['values']['schedule-no'];
        $requestparameters["is_active"]           = $form_state['values']['active-no'];
        $requestparameters["about_the_teacher"]   = $form_state['values']['about_the_teacher'];
        $upd_id   = $form_state['values']['id'];
   if($upd_id == -1)
   {
	   $user_fields = user_load($form_state['values']['name']);   
	    $requestparameters["name"] = $user_fields->name;
        $requestparameters["email"] = $user_fields->mail;   
	   // insert in teacher table
       $retdata=wiziq_teacher_method($requestparameters,'add_teacher');   
    if($retdata['ret_status'] == 'ok')
	{
      $insqry = "insert into wiziq_teacher 
				(puser_id,password,timezone, 
				about_the_teacher,can_schedule_class,is_active, 
				teacher_id, teacher_email)
				values ('$user_fields->uid','".base64_encode($requestparameters["password"])."','".$requestparameters["timezone"]."',
				'".$requestparameters["about_the_teacher"]."','".$requestparameters["can_schedule_class"]."',
				'".$requestparameters["is_active"]."',
				'".$retdata['teacher_id']."','".$requestparameters["email"]."'
				)
				";
		 $sub_users = db_query($insqry);		
	   drupal_goto('admin/wiziq/teachers');
	   drupal_set_message(t('Teacher Saved Successfully'));			
	 }  
	 else
	 {
		 drupal_set_message(t($retdata['error']),'error');
	 }  
	 // end insert table
   }
   else
   {
	    $user_fields = user_load($form_state['values']['user_id']);   
	    $requestparameters["name"] = $user_fields->name;
        $requestparameters["email"] = $user_fields->mail;   
	    $requestparameters["teacher_id"] = $form_state['values']['teacher_id'];
	    // Update Method	 
	   
	    $retdata = wiziq_teacher_method($requestparameters, 'edit_teacher');   
	    
    if($retdata['ret_status'] == 'ok')
	{
      $insqry = "update wiziq_teacher 
				set password='".base64_encode($requestparameters["password"])."',timezone='".$requestparameters["timezone"]."', 
				about_the_teacher='".$requestparameters["about_the_teacher"]."',can_schedule_class='".$requestparameters["can_schedule_class"]."',is_active='".$requestparameters["is_active"]."', 
				teacher_email='".$requestparameters["email"]."' where id='".$upd_id."'";
		$sub_users = db_query($insqry);
		$result = db_query('select * from {wiziq_teacher} where id=:id' , array(':id' => $upd_id));
		$data = $result->fetchObject();
		if($requestparameters["can_schedule_class"] == 0 || $requestparameters["is_active"] == 0)
			$update_enroll = db_query("update wiziq_enroluser set create_class = 0 , upload_content = 0 where user_id = :id",array(':id' => $data->puser_id));
		elseif($requestparameters["can_schedule_class"] == 1 || $requestparameters["is_active"] == 1)
			$update_enroll = db_query("update wiziq_enroluser set create_class = 1 , upload_content = 1 where user_id = :id",array(':id' => $data->puser_id));
		drupal_goto('admin/wiziq/teachers');		
		drupal_set_message(t('Teacher Updated Successfully'));			
	}  
	else
	{
		 drupal_set_message(t($retdata['error']),'error');		 
	}
	   // End Update
   } 
}
function teacher_form_cancel($form, &$form_state){	 	 
	// Redirect to the appropriate URL. 
	drupal_goto('admin/wiziq/teachers');
}

?>
