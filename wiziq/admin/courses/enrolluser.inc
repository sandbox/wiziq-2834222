<?php
   drupal_add_js(drupal_get_path('module', 'wiziq').'/js/enrolluser.js');
 
 include drupal_get_path('module', 'wiziq').'/function_api.php';
 	// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
function enrolluser_form()
{	
	// Redirect to Listing of courses if not administrator		
	   global $user;	   
	   if(!in_array('administrator', $user->roles))
	   {  
		   drupal_set_message(t('Please Login'),'error');
		   drupal_goto('wiziq/courses');
	   }		
     // End Redirect 	 
	$userid = $user->uid; 	
	/*********** Check for course id **********/
	$course_id = check_courseid($_REQUEST['course_id']);
	if(!empty($course_id)){
		global $base_url;
		drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'admin/wiziq'),l(t('Courses'),'admin/wiziq/courses'),l(t($course_id->fullname),'admin/wiziq/courses'),t('Enroll Users')));
		$course_id = $course_id->id;		
	}
	else
		drupal_goto('admin/wiziq/courses');
	
	
	
	
	/*********** Fetch Enroll user***************/

     $enrolled_users = db_query("SELECT t1.* , t2.name FROM wiziq_enroluser AS t1, users AS t2
                            WHERE t1.user_id = t2.uid AND t1.course_id =" . $course_id);   
     $enrolled_users = $enrolled_users->fetchAll();
          
     $opt = array();  
     $enrolled_ids = array();           
     $en_users = '';          
     if (count($enrolled_users) > 0) {
                                    foreach ($enrolled_users as $enrolled) {
																														 										
                                        $en_users.= '<option value="' . $enrolled->user_id . '" >' . $enrolled->name . '</option>';
                                        $enrolled_ids[] = $enrolled->user_id;   
                                                                         
                                    }
     }
 /* End Fetching */ 		
      $en_user = array();
	  $user_all = entity_load('user');
	  $all_user = '';
	  foreach ($user_all as $us) {		 
							 							
                            if (!in_array($us->uid, $enrolled_ids) && !empty($us->uid) && $us->name != $user->name)
                           $all_user.='<option value="'.$us->uid.'" >'.$us->name.'</option>';                             
	  }   	                      
                             
 $form['enroll']['course_id'] = array(
    
    '#value' => $_REQUEST['course_id'],
    
    '#type' => 'hidden',
    
    ); 
    
    $form['enroll']['sub_data'] = array(
    
    '#attributes' => array('id'=>'sub_data'),
    
    '#type' => 'hidden',
    
    );  
    
    $form['enroll']['existing-users'] = array(
    
    '#value' =>implode(",", $enrolled_ids),
    
    '#type' => 'hidden',
    
    );                      
 
    $rows = array();  	 	
	$rows[]= array(
        t('Enroll User'),
         '<select name="enrolleduser" id="wiziq-enroll-user" multiple size="5">'.$en_users.'</select>',
         '<input type="button" name="wiziq-add-user" id="wiziq-add-user" value="<<'.t('Add').'" /><br><input type="button" id="wiziq-remove-user" name="wiziq-remove-user" value="'.t('Remove').' >>" />' ,
         '<select name="alluser" multiple size="5" id="wiziq-all-user">'.$all_user.'</select>'
       );     
            		
    $form['table'] = array( 
       '#theme' => 'table',        
       '#rows' =>  $rows ,
       '#tree' => TRUE,
       '#empty' => t('Table has no row!'),       
      );   
      
    $header = array(
        array('data' => t('User Name')),
        array('data' => t('Create Class')),               	 
        array('data' => t('View Recording')),
	    array('data' => t('Download Recording')),
        array('data' => t('Upload Content')),              
    );	 
     
 $rows2 = array();

  if(count($enrolled_users) > 0) {                        
 
                        foreach ($enrolled_users as $enrolled) {
						 
							 
							$tbody = '';                             
                            $user_nam = '<input type="hidden" name="user_id[]" value="' . $enrolled->user_id . '" />' . $enrolled->name ;
                           
                            if($enrolled->create_class == 1)
                            $tbody = 'checked';                             
                            $creat_cls = '<input type="hidden" value="0" name="create_class[' . $enrolled->user_id . ']" /><input value="1" type="checkbox" name="create_class[' . $enrolled->user_id . ']" class="create_class" id="create_class_' . $enrolled->user_id . '"'. $tbody.' />';
                             
                            if($enrolled->view_recording == 1)
                            $tbody = 'checked';  
                            else
                            $tbody = '';                              
                                 
                            $view_rec= '<input type="hidden" value="0" name="view_recording[' . $enrolled->user_id . ']" /><input value="1" type="checkbox"  name="view_recording[' . $enrolled->user_id . ']" class="view_recording" id="view_recording_' . $enrolled->user_id . '" '.$tbody.' />';
                            
                            if($enrolled->download_recording == 1)
                            $tbody = 'checked';  
                            else
                            $tbody = '';  
                             
                            $down_rec= '<input type="hidden" value="0" name="download_recording[' . $enrolled->user_id . ']" /><input value="1" type="checkbox" name="download_recording[' . $enrolled->user_id . ']" class="download_recording" id="download_recording_' . $enrolled->user_id . '" '.$tbody.' />';
                            
                            if($enrolled->upload_content == 1)
                            $tbody = 'checked';  
                            else
                            $tbody = '';
                             
                            $up_cont= '<input type="hidden" value="0" name="upload_content[' . $enrolled->user_id . ']" /><input value="1" type="checkbox"  name="upload_content[' . $enrolled->user_id . ']" class="upload_content" id="' . $enrolled->user_id . '"'.$tbody.' />';
                            
							$rows2[]=array('data' =>array(
												  array('data' => $user_nam),
												  array('data' => $creat_cls),               	 
												  array('data' => $view_rec),
												  array('data' => $down_rec),
												  array('data' => $up_cont))              
												  ,'class'=>array('cls_enroll'),'id' =>'enroll_'.$enrolled->user_id);         
                        }                  
                     }
     $form['table2'] = array( 
       '#theme' => 'table',  
       '#header' => $header,      
       '#rows' =>  $rows2,     
       '#tree' => TRUE,         
       '#attributes' => array('id'=>'wiziq-course-permission'),       
      );
      
     $form['enroll_submit'] = array(
        '#type' => 'button',
        '#value' => t('Save'), 
        '#attributes' => array('id'=>'enrol_sub'),         
        '#weight'=>10,       
     );
      $form['enroll_submit_but'] = array(
        '#type' => 'submit',
        '#value' => t('test'), 
        '#attributes' => array('id'=>'enroll_but','style'=>'display:none'),         
        '#submit' => array('enroll_submit_but_form'),         
        '#weight'=>10,       
     );
    $form['enroll_cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'), 
        '#weight'=>10,       
        '#submit' => array('enroll_submit_cancel'),
        '#limit_validation_errors' => array(),    
    );         
      return $form; 
}
function enroll_submit_but_form($form, &$form_state){ 

	     $ajex_data = json_decode($form_state['values']['sub_data']);
	     $course_id = $form_state['complete form']['enroll']['course_id']['#value'];
	     db_delete('wiziq_enroluser')
			->condition('course_id',$course_id)
			->execute();
	     for($i=0;$i<count($ajex_data);$i++)
	     {
			db_insert('wiziq_enroluser')
			  ->fields(array(
				  'user_id' => $ajex_data[$i][0],
				  'course_id' => $course_id,
				  'create_class' => $ajex_data[$i][1],
				  'view_recording' => $ajex_data[$i][2],
				  'download_recording' => $ajex_data[$i][3],
				  'upload_content' => $ajex_data[$i][4],
			  ))
			  ->execute(); 
		 }
		drupal_goto('admin/wiziq/courses');
}
function enroll_submit_cancel()
{
	drupal_goto('admin/wiziq/courses');
}
