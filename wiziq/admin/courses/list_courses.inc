<?php
// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js	    
// add style
   drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');
// end style

global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('Administration'),'admin'),l(t('WizIQ'),'admin/wiziq'),t('Courses')));

include drupal_get_path('module', 'wiziq').'/function_api.php';
	drupal_session_start();
	if(isset($_SESSION['course_id']) || isset($_SESSION['course_id_form'])){
		unset($_SESSION['course_id']);
		unset($_SESSION['course_id_form']);
	}
function list_course_form()
{

global $user; 
$userid = $user->uid; 
$header = array(
        array('data' =>array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall','class'=>array('chkselall')))),
        array('data' => t('Course Name'),'field' => 'fullname'),
        array('data' => t('Description'),'field' => 'description'),
        array('data' => t('Manage Course')),
        array('data' => t('Created by'),'field' => 'created_by'),
        array('data' => t('Number of Classes')),
 );	 
 

// select fields for prefields
//$result = db_query('select a.id,a.fullname,a.description,a.created_by,b.name from {wiziq_courses} as a left join {users} as b on a.created_by=b.uid');
$query=db_select('wiziq_courses','a')
        ->fields('a', array('id', 'fullname', 'description','created_by'));  
  $query->leftJoin('users', 'b', 'b.uid = a.created_by');        
  $query->leftJoin('wiziq_wclasses', 'c', 'c.courseid = a.id');        
  $query->fields('b', array ('name'));
   
  
  $query->addExpression('COUNT(c.courseid)', 'course_count');  
  
  $query->groupBy('a.id');
       
 $table_sort = $query->extend('TableSort') // Add table sort extender.

                     ->orderByHeader($header); // Add order by headers.

 $pager = $table_sort->extend('PagerDefault')

                     ->limit(5); // 5 rows per page.
        
$result= $pager->execute();   

 $form['button1'] = array(
    '#type' => 'submit',
    '#value' => t('Create Course'),
    '#submit' => array('mymodule_my_form_action_one')
  );
  $form['button2'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'return admin_del_course();'),
    '#submit' => array('list_courses_form_delete')
  );
  
  //for image
  $img = '<img src="'.base_path().'sites/all/modules/wiziq/images/edit.png"  height=20px width=20px/>';
  $imgadd = '<img src="'.base_path().'sites/all/modules/wiziq/images/add.png"  height=20px width=20px/>';
  $imgenroll = '<img src="'.base_path().'sites/all/modules/wiziq/images/enroll.gif"  height=20px width=20px/>';
   
  // end image
 $rows = array();
  
foreach($result as $res)
{
	$desc = shorten_string($res->description , '6');
/* Linke For Enroll user page
	$linkbuttons =l($img, 'admin/courses_ins',array('html' => 'true','query' => array('edit' => $res->id))).' '.l($imgadd, 'admin/courses_ins',array('html' => 'true')).' '.l($imgdel,'admin/enrolluser',array('html' => 'true'));	 
* End Enroll User */ 

	//$linkbuttons =l($img, 'wiziq/courses_ins',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Course')))).' '.l($imgadd,'wiziq/classes_ins',array('html' => 'true','attributes' => array('title' => t('Add Class')),'query' => array('course_id' => $res->id))).' '.l($imgdel,$_GET['q'],array('html' => 'true','query' => array('delete' => $res->id),'attributes' => array('title' => t('Delete Class'))));	 
	$linkbuttons = l($img, 'admin/wiziq/courses/courses_ins',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Course')))).' '.l($imgadd, 'admin/wiziq/courses/classes/classes_ins',array('html' => 'true','attributes' => array('title' => t('Add Class')),'query' => array('course_id' => $res->id))).' '.l($imgenroll,'admin/wiziq/courses/enrolluser',array('html' => 'true','attributes' => array('title' => t('Enroll Users')),'query' => array('course_id' => $res->id)));	 
	$form['checkboxes'] = array('#type' => 'checkbox','#attributes' => array('id'=>$res->id,'name'=>'chkval[]','value' =>$res->id,'class'=>array('chksingle')));
	$rows[]= array( 
	   // drupal_render(array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall'))),
	    drupal_render($form['checkboxes']),
         l(t($res->fullname),'admin/wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))),
        '<div class="divdes"><span id="'.$res->id.'" class="clsdes">'.l(t($desc),'admin/wiziq/courses/course_det',array('query' => array('course_id' => $res->id))).'</span><span id="spn_'.$res->id.'" class="pop_dis">'.$res->description.'</span></div>',
         $linkbuttons,          
         t($res->name),    
         l($res->course_count,'admin/wiziq/courses/classes',array('query' => array('course_id' => $res->id),'attributes' => array('id' =>'href_'.$res->id))), 
       );    
}
// end select fields
      $form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      ),
      array(
        '#theme' => 'pager',
      ));     
  return $form;  
} 
function mymodule_my_form_action_one($form, &$form_state) {			 
  drupal_goto('admin/wiziq/courses/courses_ins');
  // Redirect to the appropriate URL.   
}
function list_courses_form_delete($form, &$form_state) {
 $id = array();
 if(isset($_POST['chkval']))
 $chkarr = $_POST['chkval'];	
 else
 $chkarr = '';	
 
 if(!empty($chkarr))
 { 		 
 foreach($chkarr as $val)
 {
	 $id[] = $val ;
 } 
	 $nodel = db_query("delete from {wiziq_courses} where id  IN (:nids)", array(':nids' => $id));
	 $recdel = db_query("SELECT ROW_COUNT()")->fetchField();	 
	 drupal_set_message(t($recdel.' Records Deleted Successfully '));
 }
 else
 {	  
	 drupal_set_message(t('No Record Selected' ));
 }  
  
}
 
