<?php

// select fields for prefields
$query = db_select('wiziq_api_credentials', 'n');  
$query->fields('n');//SELECT the fields from node    
$result = $query->execute();   
$record = $result->fetchAssoc() ;
// end select fields
 
 $form['wiziq_api_url'] = array(
  '#type' => 'textfield',
  '#title' => t('class API URL'),
  '#default_value' => $record['api_url'],
  '#required' => TRUE,
  '#attributes' => array('readonly' => 'readonly','style'=>'background: none repeat scroll 0 0 #EAEAEA;')
 );
  $form['wiziq_recurring_api_url'] = array(
  '#type' => 'textfield',
  '#title' => t('Recurring API URL'),
  '#default_value' => $record['recurring_api_url'],
  '#required' => TRUE,
  '#attributes' => array('readonly' => 'readonly','style'=>'background: none repeat scroll 0 0 #EAEAEA;')
 );
 
  $form['wiziq_content_url'] = array(
  '#type' => 'textfield',
  '#title' => t('Content API URL'),
  '#default_value' => $record['content_url'],
  '#required' => TRUE,
  '#attributes' => array('readonly' => 'readonly','style'=>'background: none repeat scroll 0 0 #EAEAEA;')
 );
 
  $form['wiziq_access_key'] = array(
  '#type' => 'textfield',
  '#title' => t('Access Key'),
  '#default_value' => $record['access_key'],
  '#required' => TRUE,
  '#suffix' => '<p style="color:red; margin:-13px 0 10px 0px !important;">Do not have WizIQ key. Get these <a target="_blank" href="https://www.wiziq.com/api/"> here </a></p>',   
   
 );
  
 
  $form['wiziq_secret_key'] = array(
  '#type' => 'textfield',
  '#title' => t('Secret Key'),
  '#default_value' => $record['secret_key'],
  '#required' => TRUE,
  '#suffix' => '<p style="color:red; margin:-13px 0 10px 0px !important;">Do not have WizIQ key. Get these <a target="_blank" href="https://www.wiziq.com/api/"> here </a></p>',    
   
 );
 
  $form['wiziq_content_language'] = array(
  '#type' => 'textfield',
  '#title' => t('Virtual Classroom Language XML'),
  '#default_value' => $record['content_language'],
  '#required' => TRUE,   
  '#attributes' => array('readonly' => 'readonly','style'=>'background: none repeat scroll 0 0 #EAEAEA;')
 );
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
	 
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('cancel_credentials'),
  );
function cancel_credentials()
{
	drupal_goto('admin/wiziq');
}
