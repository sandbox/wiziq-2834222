<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
global $base_url;
 drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'admin/wiziq'),l(t('Courses'),'admin/wiziq/courses'),l(t('Class'),'admin/wiziq/courses/classes',array('query'=>array('course_id'=>$_SESSION['course_id']))),t('Detail')));
function class_det_form()
{
 
global $user; 
$userid = $user->uid; 
$rows = array();  
// end select fields
if(isset($_REQUEST['class_id'])){
	$class_id = $_REQUEST['class_id'];
	$id_check = getClassDetail($class_id);
	if(!empty($id_check))
		getData($id_check->response_class_id);
}
else
$class_id = -1;
//unset($_SESSION['messages']['error']);
$res = getClassDetail($class_id);  
$rows = array();
if(!empty($res)){
	$outlan = getLanguages();
	foreach ( $outlan as $key=>$value)
	{
		if($res->language == $key)
		{
			$language = $value;
		}
	}
		// for current live status
	if($res->status == 'upcoming'){
		$sta = wiziq_get_datetime($res->class_time, $res->duration, $res->classtimezone );
		if($sta == true)
		$status = t("Live Class");
		else 
		$status = $res->status;
	}
	else
		$status = $res->status;
		// end current live status
	
	$rows[]= array(
         t('Class title:'),t($res->class_name),
	 );
	 $res->created_by == $userid ? $teacher = t('You') : $teacher = $res->name;
	 $rows[]= array(
         t('Teacher:'),$teacher,
	 );
	 $rows[]= array(
         t('Class Status:'),t($status),
	 );
	 $rows[]= array(
         t('Timing of Class:'),t($res->class_time),
	 );
	 $rows[]= array(
         t('Time Zone:'),t($res->classtimezone),
	 );
	 $rows[]= array(
         t('Duration (in Minutes):'),t($res->duration),
     );
     $rows[]= array(
         t('Language in Classroom:'),t($language),
	 );
	 $rows[]= array(
         t('Recording opted:'),$res->recordclass == 'true' ? t('yes') : t('No'),
     );
}
/*********** handle Launch Class Link *********** 
	$launch = $res->status == 'upcoming' ? l('Launch Class',$res->response_presenter_url,array('html' => 'true','attributes' => array('style' => 'padding:0 92px 0 0' ))): '';
	/*********** handle Download recording Link *********/
/*
	$download = $res->download_recording != '' ? l('Download Class',$res->download_recording): '';

	$rows[]=array(1=>array('data'=>$launch.''.$download,'colspan' => 2));	
*/
	
	$form['back_class'] = array(
    '#type' => 'submit',
    '#value' => t('Back to Classes'),
    '#submit' => array('back_to_classes'),
    '#attributes' => array('onclick' => 'this.form.target="_self";return true;')
  );
    
    	  
	if($res->status == 'upcoming'){
	  if($res->created_by == $userid){
	 $form['launch_class'] = array(
		'#type' => 'submit',
		'#value' => t('Launch Class'),
		'#parents' => array($res->response_presenter_url), 
		'#submit' => array('launch_class'),
		'#attributes' => array('onclick' => 'this.form.target="_blank";return true;'),
	  );
  }else{
	  $form['join_class'] = array(
		'#type' => 'submit',
		'#value' => t('Join Class'),
		'#parents' => array($res->response_class_id , $res->language), 
		'#submit' => array('join_class'),
		'#attributes' => array('onclick' => 'this.form.target="_blank";return true;'),
	  );
  }
}
  if($res->download_recording != '') {
  $form['download_recording'] = array(
		'#type' => 'submit',
		'#value' => t('Download Class'),
		'#submit' => array('download_class'),
		'#parents' => array($res->download_recording), 
	  );
  }
      
     $form['table'] = array(
       '#theme' => 'table',       
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),        
      );
      return $form; 		
	
     
}
function back_to_classes()
{
	$course_id = $_SESSION['course_id'];
		drupal_goto('admin/wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
} 
function download_class($form, &$form_state)
{
	$download_recording = $form['download_recording']['#parents'][0];
	$form_state['redirect'] = $download_recording;
} 

 function launch_class($form, &$form_state)
{
	$start_url = $form['launch_class']['#parents'][0];
	$form_state['redirect'] = $start_url;
}
 
function join_class($form, &$form_state)
{
	$class_id = $form['join_class']['#parents'][0];
	$language = $form['join_class']['#parents'][1];
	$join_url = wiziq_addattendee($class_id, $language);
	$form_state['redirect'] = $join_url;
}
 
 
