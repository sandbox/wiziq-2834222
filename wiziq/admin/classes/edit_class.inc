<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
	// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');

global $base_url;
 drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'admin/wiziq'),l(t('Courses'),'admin/wiziq/courses'),l(t('Class'),'admin/wiziq/courses/classes',array('query'=>array('course_id'=>$_SESSION['course_id'])))));
function class_edit_form()
{

$editid = '';
$nam = '';
$duration = '';
$strdat = '';
$tim_zone = '';
$attendee_limit = '';
$download_recording = 0;
$language = '';
if(isset($_GET['edit']))
{
	$editid = $_GET['edit'];
	$result = db_query('select class_name,duration,class_time,classtimezone,attendee_limit,recordclass,language,response_class_id from {wiziq_wclasses} where id=:id' , array(':id' => $editid));
	if(!empty($result)){
		foreach($result as $res)
		{					  
			$nam = $res->class_name;
			$duration = $res->duration;	
		// startdate		
			$strdat1 = $res->class_time;   
		  $year = date('Y', strtotime($strdat1))  ;    
		  $month = date('m', strtotime($strdat1))  ;     
		  $day = date('d', strtotime($strdat1))  ;         	
		  $hour = date('H', strtotime($strdat1))  ;         	
		  $minut = date('i', strtotime($strdat1))  ;         	
		 $strdat = array(
		  'day' => (int) $day,
		  'month' => (int) $month,
		  'year' => $year,
		);
		 // end startdate
		 
			
			$tim_zone = $res->classtimezone;		 
			$attendee_limit = $res->attendee_limit;		 
			 $download_recording = $res->recordclass;		 
			 if($download_recording == '')
			 $download_recording = 0;
			 
			$language = $res->language;		 
			$response_class_id = $res->response_class_id;		 
		}
	 }
}
         
	$form['class']['class_id'] = array(
    
    '#value' => $editid,
    
    '#type' => 'hidden',
    
    );
    
    $form['class']['response_class_id'] = array(
    
    '#value' => $response_class_id,
    
    '#type' => 'hidden',
    
    );
    
    $form['title'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Class Title'),
	
	'#required' => TRUE, 
	
    '#size' => 30, 
    
    '#default_value' =>  $nam ,
    
    '#maxlength' => 128,     
    
    '#suffix' => '<div id="error-name" class="wiziq_errors error-msg">'.t('Course name should not be empty').'</div>',
    
	);
	
	 $form['duration'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Class Duration (in minutes)'),
	
	'#required' => TRUE, 
	
    '#size' => 30, 
    
    '#default_value' =>  $duration ,
    
    '#maxlength' => 128, 
    
    '#description' => t('minimun 30 and maximum 300 minutes'),
    
    '#suffix' => '<div id="error-name" class="wiziq_errors error-msg ">'.t('Course name should not be empty').'</div>',
    
	);
	
	// field set start for wiziq data & time
	
    $form['time-date'] = array(
    
    '#type' => 'fieldset',
    
    '#title' => t('Date & Time'),
    
    '#weight' => 5,
    
    '#collapsible' => TRUE,
    
    '#collapsed' => FALSE,
    
   );
    
	    
  $form['time-date']['right-now'] =  array( 
   
   '#type' => 'checkbox',
   
   '#title' => t('Schedule for right now'),
   
   '#description' => t('check for the current date and time'),
   
   '#attributes' => array('onclick' => 'right_now_check()'),
    
    ); 
    
    
    
      
   $form['time-date']['class-start-date'] = array(
    
     '#type' => 'date',

     '#title' => t('Class Time'), 
     
     '#default_value' =>  $strdat ,

     '#required' => TRUE,
          
     '#date_format' => 'Y-m-d',
     
     '#suffix' => '<div id="error-start" class="wiziq_errors error-msg ">'.t('Start date sholuld be greater than or equal to current date').'</div>',
   
    );
    
    $form['time-date']['hours'] = array(
    '#type' => 'select',
    
    '#default_value' =>  $hour ,
    
    '#options' => array(
      '00' => '00','01' => '01','02' => '02','03' => '03','04' => '04','05' => '05',
      '06' => '06','07' => '07','08' => '08','09' => '09',10 => 10,
      11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,
      16 => 16,17 => 17,18 => 18,19 => 19,20 => 20,
      21 => 21,22 => 22,23 => 23,
    ),
    
    '#description' => t('Hours'), 
  );
   
   $form['time-date']['minutes'] = array(
   
    '#type' => 'select',
    
    '#default_value' =>  $minut ,
    
    '#options' => array(
      '00' => '00','01' => '01','02' => '02','03' => '03','04' => '04','05' => '05',
      '06' => '06','07' => '07','08' => '08','09' => '09',10 => 10,
      11 => 11,12 => 12,13 => 13,14 => 14,15 => 15,16 => 16,17 => 17,18 => 18,19 => 19,20 => 20,
      21 => 21,22 => 22,23 => 23,24 => 24,25 => 25,26 => 26,27 => 27,28 => 28,29 => 29,30 => 30,
      31 => 31,32 => 32,33 => 33,34 => 34,35 => 35,36 => 36,37 => 37,38 => 38,39 => 39,40 => 40,
      41 => 41,42 => 42,43 => 43,44 => 44,45 => 45,46 => 46,47 => 47,48 => 48,49 => 49,50 => 50,
      51 => 51,52 => 52,53 => 53,54 => 54,55 => 55,56 => 56,57 => 57,58 => 58,59 => 59,60 => 60,
    ),
    
    '#description' => t('Minutes'),
  );
  $outzon = getTimeZone();
  // output from XML
  $form['time-date']['time-zone'] = array(
   
    '#type' => 'select',
    
    '#default_value' =>  $tim_zone ,
    
    '#title' => t('Time Zone'),
    
    '#options' => $outzon,
    
  );
  
  
  // fieldset start for wiziq settings
  $form['wiziq-settings'] = array(
    
    '#type' => 'fieldset',
    
    '#title' => t('Add more information about yourself and your class'),
    
    '#weight' => 9,
    
    '#collapsible' => TRUE,
    
    '#collapsed' => TRUE,
    
   );
      
    $form['wiziq-settings']['attendee-limit'] = array(
	
	'#type' => 'textfield',
	
	'#title' => t('Attendee Limit in a Class'), 
	
	'#default_value' =>  $attendee_limit ,
	
    '#size' => 30, 
    
    '#maxlength' => 128, 
             
	);
  
	$form['wiziq-settings']['record-class'] = array(
	
     '#type' => 'radios',
    
     '#title' => t('Record this class'),
    
     '#default_value' => $download_recording,
    
     '#options' => array(
     
     'true' => t('Yes'),
     
     'false' => t('No'),
     ),
   );
    $outlan = getLanguages();
    // output from XML
   $form['wiziq-settings']['language'] = array(
    '#type' => 'select',
    
    '#default_value' => $language,
    
    '#title' => t('Select a language'),
    
    '#options' => $outlan ,
    
  ); 
  
   $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'), 
        '#weight'=>10,       
  );
    $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'), 
        '#weight'=>10,       
        '#submit' => array('classes_cancel'),
        '#limit_validation_errors' => array(),    
  );
    
	return $form;
}
function class_edit_form_submit($form, &$form_state)
{  
/*
	$title=$form_state['input']['title'];
	$duration=$form_state['input']['duration'];	 
	$str_dat=$form_state['input']['start-date']['year'].'-'.$form_state['input']['start-date']['month'].'-'.$form_state['input']['start-date']['day'];
	$hours=$form_state['input']['hours'];
	$minutes=$form_state['input']['minutes'];
	$format = 'Y-m-d H:i:s';
	$date = DateTime::createFromFormat($format, $str_dat.' '.$hours.':'.$minutes.':00');
    $clstime=$date->format('Y-m-d H:i:s');
    $time_zone=$form_state['input']['time-zone'];
	$attendee_limit=$form_state['input']['attendee-limit'];
	$record_class=$form_state['input']['record-class'];
	$language=$form_state['input']['language'];	 
		
	$response_class_id=$form_state['input']['response_class_id'];		
    global $user;
    $userid= $user->uid;
*/    
    $class_id = $form_state['input']['class_id'];	
    $retdata = updateliveclass($form_state);
  
   if($retdata['ret_status'] == 'ok')
   {
   
	  $nid = db_update('wiziq_wclasses')
	  ->fields(array(   
		'class_name' =>$retdata['class_name'],
		'duration' => $retdata['duration'],
		'class_time' => $retdata['class_time'],
		'classtimezone' => $retdata['classtimezone'],
		'attendee_limit' => $retdata['attendee_limit'],
		'recordclass' => $retdata['recordclass'],
		'language' => $retdata['language'],			 
	  ))
	  ->condition('id',$class_id, '=')
	  ->execute();
		drupal_set_message(t('Class Update Successfully'));
		if(isset($_SESSION['course_id'])){
		$course_id = $_SESSION['course_id'];
		drupal_goto('admin/wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
	}
	else
		drupal_goto('admin/wiziq/courses');  
		
  }
  else{
		if($retdata['errorcode'] == '1017')
		{
			$nid = db_update('wiziq_wclasses')
			->fields(array(
			'status' => 'expired',
			))
		  ->condition('id',$class_id, '=')
		  ->execute();
		}
		$form_state['rebuild'] = TRUE;
		drupal_set_message(t('Error: '.$retdata['error'].'<br>Errorcode: '.$retdata['errorcode']),'error');  
	}
}
function classes_cancel()
{
	if(isset($_SESSION['course_id'])){
		$course_id = $_SESSION['course_id'];
		drupal_goto('admin/wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
	}
	else
		drupal_goto('admin/wiziq/courses');  	
}


?>
