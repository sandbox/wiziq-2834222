<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
// add js
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
// end js
// add style
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');

global $base_url;
drupal_set_breadcrumb(array(l(t('Home'),$base_url),l(t('WizIQ'),'admin/wiziq'),l(t('Courses'),'admin/wiziq/courses')));

function list_classes_form()
{	
	 /***************** Create session for course id *****************/
	 drupal_session_start();
	 if(isset($_REQUEST['course_id']))
	 $course_id = check_courseid($_REQUEST['course_id']);
	 if(!empty($course_id))
			$_SESSION['course_id'] = $course_id->id;
	
	/************* Delete Classes ***************/
	if(isset($_REQUEST['delete']) && !empty($_REQUEST['delete']))
	{
		$cid = $_REQUEST['delete'];
		$result = getClassDetail($cid);
		if(isset($result) && !empty($result)){
		if($result->status == 'upcoming'){
			$return = delete_class($cid);
		if(isset($return) && $return['status'] == 'fail')
			drupal_set_message(t($return['errormsg'].'<br>Errorcode : '.$return['errorcode']),'error');
		else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				drupal_set_message(t('Class Delete Successfully'));
			}
		}
			else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				drupal_set_message(t('Class Delete Successfully'));
			
		  }
		}
	}
	
	
	/*********** Fetch records having get_detail 0***************/
	$query = db_select('wiziq_wclasses','a')
    ->fields('a')
    ->condition('get_detail',0,'=');
    $result = $query->execute();
    foreach($result as $res)
    {
	  $rec_details = getRecurringDetail($res->master_id);	  
	   if(!empty($rec_details)){
       foreach($rec_details->class_details as $rec_detail) 
		{
		    if($res->response_class_id != $rec_detail->class_id)
		    {				   
			  db_insert('wiziq_wclasses')
              ->fields(array(
			  'created_by' => $res->created_by,
			  'class_name' => $rec_detail->class_title,
			  'class_time' => date("Y-m-d H:i:s" , strtotime($rec_detail->start_time)) ,
			  'duration'   => $rec_detail->duration,
			  'courseid'   => $res->courseid,
			  'classtimezone' => $res->classtimezone,
			  'language' => $res->language,
			  'recordclass' => $res->recordclass,
			  'attendee_limit' => $res->attendee_limit,
			  'response_class_id' => $rec_detail->class_id,
			  'response_recording_url' => $rec_detail->recording_url,
			  'response_presenter_url' => $rec_detail->presenter_list->presenter->presenter_url,
			  'status' => $res->status,
			  'master_id' => $res->master_id,
			  'attendence_report' => $rec_detail->attendance_report_status,
			  'get_detail' => 1,
			  'download_recording' => '',
			  'is_recurring' => 'TRUE',			  
			  ))
			  ->execute();
			}
		 	    
		}	
			  db_update('wiziq_wclasses')
				  ->fields(array(
					'get_detail' => 1,
					'class_time' => date("Y-m-d H:i:s" , strtotime($rec_detail->start_time)) ,
					'attendence_report' => $rec_detail->attendance_report_status,
					))
				  ->condition('response_class_id', $res->response_class_id, '=')
				  ->execute();
			}	
	}

 global $user; 
$userid = $user->uid; 
  
 	function array_insert(&$array, $insert, $position) {
		settype($array, "array");
		settype($insert, "array");
		settype($position, "int");	 
		//if pos is start, just merge them
		if($position == 0) {
			$array = array_merge($insert, $array);
		} else { 
	 
		  //if pos is end just merge them
			if($position >= (count($array)-1)) {
				$array = array_merge($array, $insert);
			} else {
				//split into head and tail, then merge head+inserted bit+tail
				$head = array_slice($array, 0, $position);
				$tail = array_slice($array, $position);
				$array = array_merge($head, $insert, $tail);
			}
		}
	}

$refresh_img = '<img id="clsimg"  src="'.base_path().'sites/all/modules/wiziq/images/refresh.png" height=20px width=20px />';
$header = array(
		array('data' =>array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall','class'=>array('chkselall')))),
        array('data' => t('Class Title'),'field'=>'class_name'),
        array('data' => t('Class Start Time') ),               	 
        array('data' => t('Presenter') ),
        array('data' => t('Class Status').$refresh_img,'class' => 'clsimg'),
        array('data' => t('Attendance Report')),
        array('data' => t('View Recording')),
        array('data' => t('Downlaod Recording')),
        
 );	  
 if($userid != 0)
 {
	 array_insert($header, array('data' => t('Manage Class')),4);	      
 } 
  
$rows = array(); 
// button
if(isset($_REQUEST['course_id'])){
$form['create-class'] = array(
    '#type' => 'submit',
    '#value' => t('Create Class'),
    '#submit' => array('classes_form_action')
  );
  $form['update-btn'] = array(
    '#type' => 'submit',
    '#value' => t('Get Class'),
    '#attributes' => array('style'=> 'display:none'),
    '#submit' => array('classes_form_Get')
  );
  
  if(isset($_REQUEST['master_id'])){
	$form['back-class'] = array(
    '#type' => 'submit',
    '#value' => 'Back to Classes',
    '#submit' => array('back_to_classes')
   );
  }
}
$form['back_course'] = array(
    '#type' => 'submit',
    '#value' => t('Back to Courses'),
    '#submit' => array('back_to_courses')
  );
// end button
// end select fields
if(isset($_REQUEST['course_id']))
$course_id = $_REQUEST['course_id'];
else
$course_id = -1;

$query=db_select('wiziq_wclasses','a')
        ->fields('a');  
	  $query->leftJoin('users', 'b', 'b.uid = a.created_by');        
	  $query->fields('b', array ('name'));
	  $query->condition('courseid',$course_id, '=');
	if(isset($_REQUEST['master_id']))
	  $query->condition('master_id',$_REQUEST['master_id'], '=');
   
        
 $table_sort = $query->extend('TableSort') // Add table sort extender.

                     ->orderByHeader($header); // Add order by headers.

 $pager = $table_sort->extend('PagerDefault')

                     ->limit(5); // 5 rows per page.
        
$result = $pager->execute();   
 $rows = array();
 $img = '<img src="'.base_path().'sites/all/modules/wiziq/images/edit.png" height=20px width=20px />';
 $imgdel = '<img src="'.base_path().'sites/all/modules/wiziq/images/delete.png" height=20px width=20px ></img>';
 $img_reccur = '<img src="'.base_path().'sites/all/modules/wiziq/images/list.png" height=20px width=20px ></img>';
foreach($result as $res)
{ 
	if(isset($classIds))
		$classIds.=",".$res->response_class_id;
	else
		$classIds = $res->response_class_id;
		
		
		/*********** handle link buttons *********/
		$edit_link = $res->status == 'upcoming'? l($img, 'admin/wiziq/courses/classes/edit',array('html' => 'true','query' => array('edit' => $res->id),'attributes' => array('title' => t('Edit Class')))) : '';
		$linkbuttons = $edit_link.' '.l($imgdel,$_GET['q'],array('html' => 'true','query' => array('course_id' => $_SESSION['course_id'],'delete' => $res->response_class_id),'attributes' => array('title' => t('Delete Class'),'class'=>array("delrec"))));
		
		/*********** handle link button for recurring class *********/
		$linkbuttons .= $res->is_recurring == 'TRUE' && !isset($_REQUEST['master_id']) ? ' '.l($img_reccur,$_GET['q'],array('html' => 'true','query' => array('course_id' => $_SESSION['course_id'],'master_id' => $res->master_id),'attributes' => array('title' => t('View Recurring Class Detail'),'class'=>array("recclass")))) : '' ;
		/*********** handle Report Link *********/
		$report = $res->attendence_report == 'available' ? l(t('View'),'admin/wiziq/courses/classes/view_attendee_report',array('query' => array('class_id' => $res->response_class_id))) : '_' ;
		/*********** handle view recording Link *********/
		$view_recording = ($res->status == 'completed' && $res->recordclass == 'true') ? l(t('View'),$res->response_recording_url,array('attributes'=>array('target'=>'blank'))) : '_';
		/*********** handle Download recording Link *********/
		$download = $res->download_recording != '' ? l(t('Download'),$res->download_recording,array('attributes'=>array('target'=>'blank'))): '_';
		
		$form['checkboxes'] = array('#type' => 'checkbox','#attributes' => array('id'=>$res->response_class_id,'name'=>'chkvalclass[]','value'=>$res->response_class_id,'class'=>array('chksingle')));
		
				// for current live status
		if($res->status == 'upcoming'){
			$sta=wiziq_get_datetime ($res->class_time, $res->duration, $res->classtimezone );
			if($sta == true)
			$status = t("Live Class");
			else 
			$status = $res->status;
		}
	else
		$status = $res->status;
		// end current live status
		
/*
		$date = new DateTime($res->class_time.' +00');
		
		$newdate = $date->setTimezone(new DateTimeZone($res->classtimezone));
		echo $date->format('Y-m-d H:i:s').'<br>';
*/
		
/*
		$date = new DateTime('2012-07-16 01:00:00 +00');
$date->setTimezone(new DateTimeZone('Europe/Moscow')); // +04
*/

//echo $date->format('Y-m-d H:i:s').'<br>'; // 2012-07-15 05:00:00 
		
		$Class_time = t($res->class_time)."(".$res->classtimezone.")";
		$rows[]= array(
			drupal_render($form['checkboxes']),
			 l(t($res->class_name),'admin/wiziq/courses/classes/class_det',array('query' => array('class_id' => $res->response_class_id))),
			 $Class_time,
			 t($res->name),
			 t($status),
			 $linkbuttons,          
			 $report,    
			 $view_recording,
			 $download,
		   );       
}
   
   if(isset($classIds) && !empty($classIds))
refresh_class($classIds); 
   
   $form['class_ids'] = array(
    '#type' => 'hidden',
    '#attributes'=>array('id'=>'clsid'),
    '#value' => isset($classIds)? $classIds:''
  );
    
    $form['button2'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#attributes' => array('onclick' => 'return admin_del_course();'),
    '#submit' => array('list_classes_form_delete')
  );
	
       $form['table'] = array(array(
       '#theme' => 'table',
       '#header' => $header,
       '#rows' => $rows,
       '#empty' => t('Table has no row!'),
       '#attributes' => array('fullname' => 'sort-table')
      ),
      array(
        '#theme' => 'pager',
      ));		
       
return $form;  
}

function classes_form_Get($form, &$form_state) {
 $clsids=$form_state['input']['class_ids'];
 refresh_class($clsids);
}
function refresh_class($clsids)
{
	if(!empty($clsids)){ 			 
 
	 $data = db_query("SELECT response_class_id,status,recordclass,attendence_report FROM {wiziq_wclasses} WHERE response_class_id IN($clsids)");

	/*********** For getData API Request ************/
	 foreach($data as $result)
	{
		if($result->status == 'upcoming' || ($result->status == 'completed' && $result->attendence_report != 'available' ))
			getData($result->response_class_id);
	}
	/*********** For getdownloadlink API Request ************/ 
	 foreach($data as $result)
	{
		if( $result->status == 'completed' && $result->recordclass != 'true' )
			getdownloadlink($result->response_class_id);
	}
	}
}


function classes_form_action($form, &$form_state)
{
	$course_id = $_SESSION['course_id'];
	drupal_goto('admin/wiziq/courses/classes/classes_ins', array('query' => array('course_id' => $course_id)));
	// Redirect to the appropriate URL. 
}

function back_to_courses()
{
	drupal_goto('admin/wiziq/courses');
}
function back_to_classes()
{
	if(isset($_SESSION['course_id'])){
		$course_id = $_SESSION['course_id'];
		drupal_goto('admin/wiziq/courses/classes', array('query' => array('course_id' => $course_id)));
	}
}

function list_classes_form_delete($form, &$form_state)
{
	$id = array();
	 if(isset($_POST['chkvalclass']))
	 $chkarr = $_POST['chkvalclass'];	
	 else
	 $chkarr = '';	

	 if(!empty($chkarr))
	 { 		 
	 foreach($chkarr as $val)
	 {
		$cid = $val;
		$result = getClassDetail($cid);
		if(isset($result) && !empty($result)){
		if($result->status == 'upcoming'){
			$return = delete_class($cid);
		if(isset($return) && $return['status'] == 'fail')
			drupal_set_message(t($return['errormsg'].'<br>Errorcode : '.$return['errorcode']),'error');
		else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				$recdel=db_query("SELECT ROW_COUNT()")->fetchField();	 
				drupal_set_message(t($recdel.' Records Deleted Successfully '));
			}
		}
			else{
				$num_deleted = db_delete('wiziq_wclasses')
				->condition('response_class_id', $cid)
				->execute();
				$recdel=db_query("SELECT ROW_COUNT()")->fetchField();	 
				drupal_set_message(t($recdel.' Record Deleted Successfully '));
			
		  }
		}
	 } 
	 }
	 else
	 {	  
		 drupal_set_message(t('No Record Selected' ));
	 }  
}
