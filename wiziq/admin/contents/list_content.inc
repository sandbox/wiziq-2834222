<?php
include drupal_get_path('module', 'wiziq').'/function_api.php';
/********** Include js ************/
drupal_add_js(drupal_get_path('module', 'wiziq').'/js/list_courses.js');
/********** Include js ************/
drupal_add_css(drupal_get_path('module', 'wiziq').'/css/wiziq-style.css');

/********** Breadcrumb for content path ***********/
	if(isset($_REQUEST['parent_id']))	
		$parent_id = $_REQUEST['parent_id'];	 		 
	else
		$parent_id = 1;			
	global $base_url;
	$path = array(l(t('Home'),$base_url),l(t('WizIQ'),'admin/wiziq'),l(t('Content'),'admin/wiziq/contents'));
	if($parent_id != 0){
	$breadcrumb = wiziq_breadcrumb($parent_id);
	$count = count($breadcrumb);
	$i = 1;
		foreach ($breadcrumb as $data)
		{
			if($i != $count)
				$sub_path = l("$data->name",'admin/wiziq/contents',array('query' => array('parent_id' => "$data->id"))) ;
			else
				$sub_path = $data->name;
			array_push($path,$sub_path);
			$i++;
		}
}
drupal_set_breadcrumb($path);


function list_content_form($form, &$form_state)
{
	if(isset($_REQUEST['parent_id']))	
    $parent_id = $_REQUEST['parent_id'];	 		 
    elseif(isset($form_state['input']['parent_id']) && $form_state['input']['parent_id']!=0)
    $parent_id = $form_state['input']['parent_id'];
    else
    $parent_id = 1;	 		 
	
	
	$refresh_img = '<img class="refresh"  src="'.base_path().'sites/all/modules/wiziq/images/refresh.png" height=20px width=20px />';
	$header = array(
		array('data' =>array('#type' => 'checkbox','#default_value' =>'select all','#attributes' => array('id'=>'chkselall','name'=>'chkselall','class'=>array('chkselall')))),
		array('data' => t('Content Name'),'field' => 'name'),
		array('data' => t('Type')),
		array('data' => t('Inside')),
		array('data' => t('Status').$refresh_img),
	);
 
 
  $query = db_select('wiziq_contents','a')
	->fields('a');	        
	$query->leftJoin('wiziq_contents','e','e.parent_id = a.id');  
	$query->addExpression('SUM(CASE WHEN e.isfolder = 1 THEN 1 ELSE 0 END)','countfolder');   
	$query->addExpression('SUM(CASE WHEN e.isfolder = 0 THEN 1 ELSE 0 END)', 'countfile');         
	$query->condition('a.parent_id',$parent_id, '='); 
	$query->groupBy('a.id');
 
 

	$table_sort = $query->extend('TableSort') // Add table sort extender.

	->orderByHeader($header); // Add order by headers.

	$pager = $table_sort->extend('PagerDefault')

	->limit(5); // 5 rows per page.

	$result = $pager->execute();   
	
	$rows = array();
	$ids = array();
	foreach($result as $res)
	{
		/********** Start Fields for table **********/
		/********* Checkboxes for every field *********/
		$form['checkboxes'] = array('#type' => 'checkbox','#attributes' => array('id'=>$res->id,'name'=>'chkval[]','value' =>$res->id,'class'=>array('chksingle'),'fol' => $res->countfolder + $res->countfile));
		/********* Name Link for name column in table *********/
		$name_link = $res->isfolder == 1 ? l("$res->name",'admin/wiziq/contents',array('query' => array('parent_id' => "$res->id"))) : $res->name ;
		/********* inside column in table **********/
		$inside = $res->isfolder == 1 ? $res->countfolder.t(' Folder(s) ').', ' . $res->countfile.t(' File(s)') : '';
		/********* images for file type column *********/
		$type = $res->uploadingfile;
		
		if($type != 'folder'){
			$type = pathinfo($res->uploadingfile, PATHINFO_EXTENSION);

		}
		$type_img = '<img src="'.base_path().'sites/all/modules/wiziq/images/'.$type.'.png" height=20px width=20px />';
		/********** End Fields for table **********/
		if($res->parent_id != 0)
		{
		$rows[]= array(
		 drupal_render($form['checkboxes']),
		 $name_link,
		 $type_img,
		 $inside,
		 t($res->status),
		);        
	}
		$ids[$res->id] = $res->folderpath.','.$res->countfile;
	}
	$ids = json_encode($ids);
	$form['parent_id'] = array(

		'#value' => $parent_id,

		'#type' => 'hidden',

		);
	$form['ids'] = array(

		'#value' => array($ids),

		'#type' => 'hidden',

		);
	$form['upload_content'] = array(
		'#type' => 'submit',
		'#value' => t('Upload Content'),
		'#submit' => array('upload_content'),
		'#prefix' => '<div class="container-inline">', 
		);
		$form['delete_content'] = array(
		'#type' => 'submit',
		'#value' => t('Delete'),
		'#attributes' => array('onclick' => 'return admin_del_content();'),
		'#submit' => array('list_content_form_delete')
		);
		$form['create_folder'] = array(
		'#type' => 'button',
		'#value' => t('Create Folder'),
		'#attributes' => array('class'=> array('folder_element')),
		);
		
		$form['folder']['text_folder'] = array(
		'#type' => 'textfield',
		'#size' => 20, 
		'#maxlength' => 128,
		'#attributes' => array('class'=> array('disp_element')),
		);
		
		$form['save_folder'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		'#attributes' => array('class'=> array('disp_element')),
		'#submit' => array('list_content_form_folder')
		);
		
		$form['cancel_folder'] = array(
		'#type' => 'button',
		'#value' => t('Cancel'),		 
		'#attributes' => array('class'=> array('disp_element')),
		'#suffix' => '</div>'
		);
		 
     
		
	$form['table'] = array(array(
		'#theme' => 'table',
		'#header' => $header,
		'#rows' => $rows,
		'#empty' => t('Table has no row!'),
		'#attributes' => array('fullname' => 'sort-table')
		),
		array(
		'#theme' => 'pager',
		));     
		
		
	$form['update-btn'] = array(
		'#type' => 'submit',
		'#value' => t('Get Update'),
		'#attributes' => array('style'=> 'display:none'),
		'#submit' => array('contents_status_update')
		);

  return $form;
	
	
}
/********* to get the updated status **********/
function contents_status_update($form, &$form_state)
{
		content_refresh($_REQUEST['parent_id']);
}

function upload_content($form, &$form_state)
{

	drupal_goto('admin/wiziq/contents/content_ins', array('query' => array('parent_id' => $form_state['input']['parent_id'])));
}
// save folder
function list_content_form_folder($form, &$form_state)
{
	global $user;
    $folder_name = $form_state['input']['text_folder'];	
 
	if(isset($form_state['input']['parent_id']) && $form_state['input']['parent_id'] != 0)
	{
		 $result = db_query("SELECT id,folderpath,parent_id from {wiziq_contents} where id  IN (:ids)", array(':ids' => $form_state['input']['parent_id']));	 		 
		 foreach($result as $res)
	    {
			 $folder_path = $res->folderpath;
			  $parent_id = $res->id;
			  $is_folder = 1;
			  $content_id = 0;
			  $uploading_file = 'folder';
		}
		
	} 
	else
	{
		$folder_path = '';
		$parent_id = 1;
		$is_folder = 1;
		$content_id = 0;
		$uploading_file = 'folder';
    }
	
	$retmsg = wiziq_create_folder($folder_path, $folder_name);

	if($retmsg['res'] == true)
	{
		 $nid = db_insert('wiziq_contents')
	  ->fields(array(   
		'created_by' => $user->uid,
		'isfolder' => $is_folder,
		'name' => $folder_name,
		'status' => '',
		'uploadingfile' => $uploading_file,
		'content_id' => $content_id,
		'parent_id' => $parent_id,		 
		'folderpath' => $retmsg['path'],			 
	  ))	   
	  ->execute();
		drupal_set_message(t('Folder Created'));
	}	
}
// end saving
function list_content_form_delete($form, &$form_state)
{
	 $id = array();
	 if(isset($_POST['chkval']))
	 $chkarr = $_POST['chkval'];
	 else
	 $chkarr = '';
	 if(!empty($chkarr))
	 {
		 $notdel=$i=0;
		 foreach($chkarr as $val)
		{
			  $record=db_query("select isfolder,folderpath,content_id from {wiziq_contents} where id=:nids", array(':nids' => $val))->fetchObject();
			  // $record = $result->fetchObject();
			 
			   if($record->isfolder == 1)
			   {
				   $ret = wiziq_delete_folder($record->folderpath,'folder');       // delete if folder
				   $nodel = db_query("delete from {wiziq_contents} where id=:nids", array(':nids' => $val));
				   $i++;
		       }
			   else
			   {
			      $ret = wiziq_delete_folder($record->content_id,'file');          // delete if file	  			 			  
				  if($ret['livestatus'] == 'ok')
				  {
					 $nodel = db_query("delete from {wiziq_contents} where id=:nids", array(':nids' => $val));
					 $i++;
				  }
				  else
				  {
					  $notdel++;
					  $errdis=$ret['error'];
				  }
		      }
		}
		 
		 if($i != 0 && $notdel != 0)
		 drupal_set_message(t($i.' Records Deleted Successfully,Some Records Pending Try After Some Time '));
		 elseif($i != 0 && $notdel == 0)
		 drupal_set_message(t($i.' Records Deleted Successfully'));
		 else
		 drupal_set_message(t($errdis),'error');
	 }
	 else
	 {
		 drupal_set_message(t('No Record Selected' ));
	 }
}
