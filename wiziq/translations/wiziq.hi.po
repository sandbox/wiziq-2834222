# $Id$
#
# Hindi translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  function_api.php: n/a
#  menu_admin.php: n/a
#  wiziq.module: n/a
#  admin/classes/attendee_report.inc: n/a
#  admin/classes/class_det.inc: n/a
#  admin/classes/classes.inc: n/a
#  admin/classes/edit_class.inc: n/a
#  admin/classes/list_classes.inc: n/a
#  admin/courses/course_det.inc: n/a
#  admin/courses/courses.inc: n/a
#  user/attendee_report.inc: n/a
#  user/class_det.inc: n/a
#  user/classes.inc: n/a
#  user/course_det.inc: n/a
#  user/edit_class.inc: n/a
#  user/list_classes.inc: n/a
#  admin/contents/content.inc: n/a
#  admin/contents/list_content.inc: n/a
#  user/contents/list_content.inc: n/a
#  wiziq.info: n/a
#  admin/wiziq_credential.php: n/a
#  admin/courses/enrolluser.inc: n/a
#  user/courses.inc: n/a
#  user/contents/content.inc: n/a
#  admin/courses/list_courses.inc: n/a
#  user/list_courses.inc: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2014-06-03 12:05+0000\n"
"PO-Revision-Date: 2014-06-03 12:05+0000\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: Hindi <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: function_api.php:28;87;123;315;415;492;595;666;715;795;840;853;954;1065;1238
msgid "There is some error in WizIQ Service"
msgstr "hi-There is some error in WizIQ Service"

#: function_api.php:82
msgid "Error in getting time zone"
msgstr "hi-Error in getting time zone"

#: function_api.php:92;127
msgid "Curl extention is not installed"
msgstr "hi-Curl extention is not installed"

#: function_api.php:118
msgid "Error in getting languages"
msgstr "hi-Error in getting languages"

#: function_api.php:945;949
msgid "unable to create folder"
msgstr "hi-unable to create folder"

#: menu_admin.php:5 wiziq.module:40;115;254 admin/classes/attendee_report.inc:4 admin/classes/class_det.inc:4 admin/classes/classes.inc:9 admin/classes/edit_class.inc:10 admin/classes/list_classes.inc:10 admin/courses/course_det.inc:4 admin/courses/courses.inc:4 user/attendee_report.inc:30 user/class_det.inc:17 user/classes.inc:21 user/course_det.inc:4 user/edit_class.inc:30 user/list_classes.inc:20
msgid "Courses"
msgstr "hi-Courses"

#: menu_admin.php:6
msgid "Manage Courses"
msgstr "hi-Manage Courses"

#: menu_admin.php:9 wiziq.module:393;433
msgid "Contents"
msgstr "hi-Contents"

#: menu_admin.php:10
msgid "Manage Content"
msgstr "hi-Manage Content"

#: wiziq.module:354
msgid "View Attendee Report"
msgstr "hi-View Attendee Report"

#: wiziq.module:475
msgid "This is wiziq help part.For Further assistance contect <a href=\"http://www.wiziq.com\">here</a>."
msgstr "hi-"

#: wiziq.module:487;492
msgid "Welcome in admin WizIQ"
msgstr "hi-Welcome in admin WizIQ"

#: wiziq.module:516
msgid "Credential Saved Successfully"
msgstr "hi-Credential Saved Successfully"

#: wiziq.module:520
msgid "Please Enter valid credential"
msgstr "hi-Please Enter valid credential"

#: wiziq.module:12;26 admin/classes/attendee_report.inc:4 admin/classes/class_det.inc:4 admin/classes/classes.inc:9 admin/classes/edit_class.inc:10 admin/classes/list_classes.inc:10 admin/contents/content.inc:4 admin/contents/list_content.inc:14 admin/courses/course_det.inc:4 user/attendee_report.inc:30 user/class_det.inc:17 user/classes.inc:21 user/course_det.inc:4 user/edit_class.inc:30 user/list_classes.inc:20 user/contents/list_content.inc:14
msgid "WizIQ"
msgstr "hi-WizIQ"

#: wiziq.module:58;75
msgid "Courses Detail"
msgstr "hi-Courses Detail"

#: wiziq.module:94
msgid "List Courses"
msgstr "hi-List Courses"

#: wiziq.module:133;156;215;233
msgid "Classes"
msgstr "hi-Classes"

#: wiziq.module:174
msgid "List Classes"
msgstr "hi-List Classes"

#: wiziq.module:194
msgid "Classes Form"
msgstr "hi-Classes Form"

#: wiziq.module:276
msgid "WizIQ Credentials"
msgstr "hi-WizIQ Credentials"

#: wiziq.module:293
msgid "User Permissions"
msgstr "hi-User Permissions"

#: wiziq.module:317;334;371
msgid "Edit Courses"
msgstr "hi-Edit Courses"

#: wiziq.module:413;450
msgid "Contents Insert"
msgstr "hi-Contents Insert"

#: wiziq.info:0;0
msgid "Wiziq"
msgstr "hi-Wiziq"

#: wiziq.info:0
msgid "This is an wizIQ Module"
msgstr "hi-This is an wizIQ Module"

#: admin/wiziq_credential.php:12
msgid "class API URL"
msgstr "hi-class API URL"

#: admin/wiziq_credential.php:19
msgid "Recurring API URL"
msgstr "hi-Recurring API URL"

#: admin/wiziq_credential.php:27
msgid "Content API URL"
msgstr "hi-Content API URL"

#: admin/wiziq_credential.php:35
msgid "Access Key"
msgstr "hi-Access Key"

#: admin/wiziq_credential.php:43
msgid "Secret Key"
msgstr "hi-Secret Key"

#: admin/wiziq_credential.php:51
msgid "VC Language Interface"
msgstr "hi-VC Language Interface"

#: admin/wiziq_credential.php:59
msgid "Submit"
msgstr "hi-Submit"

#: admin/wiziq_credential.php:64 admin/classes/classes.inc:387 admin/classes/edit_class.inc:273 admin/contents/content.inc:94 admin/contents/list_content.inc:153 admin/courses/courses.inc:155 admin/courses/enrolluser.inc:176 user/classes.inc:412 user/courses.inc:168 user/edit_class.inc:300 user/contents/content.inc:93 user/contents/list_content.inc:177
msgid "Cancel"
msgstr "hi-Cancel"

#: admin/classes/attendee_report.inc:4 admin/classes/class_det.inc:4 admin/classes/classes.inc:9 admin/classes/edit_class.inc:10 admin/classes/list_classes.inc:10 admin/contents/content.inc:4 admin/contents/list_content.inc:14 admin/courses/course_det.inc:4 admin/courses/courses.inc:4 user/attendee_report.inc:30 user/class_det.inc:17 user/classes.inc:21 user/course_det.inc:4 user/edit_class.inc:30 user/list_classes.inc:20 user/contents/content.inc:4 user/contents/list_content.inc:14
msgid "Home"
msgstr "hi-Home"

#: admin/classes/attendee_report.inc:4 admin/classes/class_det.inc:4 admin/classes/edit_class.inc:10 user/attendee_report.inc:30 user/class_det.inc:17 user/edit_class.inc:30
msgid "Class"
msgstr "hi-Class"

#: admin/classes/attendee_report.inc:4 user/attendee_report.inc:30
msgid "Attendee Report"
msgstr "hi-Attendee Report"

#: admin/classes/attendee_report.inc:14 user/attendee_report.inc:39
msgid "Attendee Name"
msgstr "hi-Attendee Name"

#: admin/classes/attendee_report.inc:15 user/attendee_report.inc:40
msgid "Entry Time"
msgstr "hi-Entry Time"

#: admin/classes/attendee_report.inc:16 user/attendee_report.inc:41
msgid "Exit Time"
msgstr "hi-Exit Time"

#: admin/classes/attendee_report.inc:17 user/attendee_report.inc:42
msgid "Attended Time"
msgstr "hi-Attended Time"

#: admin/classes/attendee_report.inc:30 admin/classes/class_det.inc:74 user/attendee_report.inc:55 user/class_det.inc:93
msgid "Back to Classes"
msgstr "hi-Back to Classes"

#: admin/classes/attendee_report.inc:37 admin/classes/class_det.inc:111 admin/classes/list_classes.inc:267 admin/contents/list_content.inc:164 admin/courses/course_det.inc:55 admin/courses/enrolluser.inc:99 admin/courses/list_courses.inc:97 user/attendee_report.inc:62 user/class_det.inc:129 user/course_det.inc:64 user/list_classes.inc:307 user/list_courses.inc:152 user/contents/list_content.inc:188
msgid "Table has no row!"
msgstr "hi-Table has no row!"

#: admin/classes/class_det.inc:4 user/class_det.inc:17
msgid "Detail"
msgstr "hi-Detail"

#: admin/classes/class_det.inc:29 admin/classes/list_classes.inc:225 user/class_det.inc:58 user/list_classes.inc:276
msgid "Live Class"
msgstr "hi-Live Class"

#: admin/classes/class_det.inc:38 user/class_det.inc:66
msgid "Class title:"
msgstr "hi-Class title:"

#: admin/classes/class_det.inc:40 admin/courses/course_det.inc:21 user/class_det.inc:68 user/course_det.inc:30
msgid "You"
msgstr "hi-You"

#: admin/classes/class_det.inc:42 user/class_det.inc:70
msgid "Teacher:"
msgstr "hi-Teacher:"

#: admin/classes/class_det.inc:45 user/class_det.inc:73
msgid "Class Status:"
msgstr "hi-Class Status:"

#: admin/classes/class_det.inc:48 user/class_det.inc:76
msgid "Timing of Class:"
msgstr "hi-Timing of Class:"

#: admin/classes/class_det.inc:51 user/class_det.inc:79
msgid "Time Zone:"
msgstr "hi-Time Zone:"

#: admin/classes/class_det.inc:54 user/class_det.inc:82
msgid "Duration (in Minutes):"
msgstr "hi-Duration (in Minutes):"

#: admin/classes/class_det.inc:57 user/class_det.inc:85
msgid "Language in Classroom:"
msgstr "hi-Language in Classroom:"

#: admin/classes/class_det.inc:60 user/class_det.inc:88
msgid "Recording opted:"
msgstr "hi-Recording opted:"

#: admin/classes/class_det.inc:60 user/class_det.inc:88
msgid "yes"
msgstr "hi-yes"

#: admin/classes/class_det.inc:60 admin/classes/classes.inc:367 admin/classes/edit_class.inc:250 user/class_det.inc:88 user/classes.inc:391 user/edit_class.inc:277
msgid "No"
msgstr "hi-No"

#: admin/classes/class_det.inc:84 user/class_det.inc:103
msgid "Launch Class"
msgstr "hi-Launch Class"

#: admin/classes/class_det.inc:92 user/class_det.inc:111
msgid "Join Class"
msgstr "hi-Join Class"

#: admin/classes/class_det.inc:102
msgid "Download Class"
msgstr "hi-Download Class"

#: admin/classes/classes.inc:9 admin/classes/list_classes.inc:146 admin/courses/enrolluser.inc:104 user/classes.inc:21 user/list_classes.inc:183
msgid "Create Class"
msgstr "hi-Create Class"

#: admin/classes/classes.inc:43 admin/classes/edit_class.inc:78 admin/classes/list_classes.inc:127 user/classes.inc:65 user/edit_class.inc:103 user/list_classes.inc:162
msgid "Class Title"
msgstr "hi-Class Title"

#: admin/classes/classes.inc:52 user/classes.inc:73 user/edit_class.inc:113
msgid "Please enter a title."
msgstr "hi-Please enter a title."

#: admin/classes/classes.inc:60 admin/classes/edit_class.inc:96 user/classes.inc:81 user/edit_class.inc:121
msgid "Class Duration (in minutes)"
msgstr "hi-Class Duration (in minutes)"

#: admin/classes/classes.inc:70 admin/classes/edit_class.inc:106 user/classes.inc:91 user/edit_class.inc:131
msgid "minimun 30 and maximum 300 minutes"
msgstr "hi-minimun 30 and maximum 300 minutes"

#: admin/classes/classes.inc:72 user/classes.inc:93 user/edit_class.inc:133
msgid "Please enter duration between 30 to 300 minutes."
msgstr "hi-Please enter duration between 30 to 300 minutes."

#: admin/classes/classes.inc:82 admin/classes/edit_class.inc:118 user/classes.inc:103 user/edit_class.inc:143
msgid "Date & Time"
msgstr "hi-Date & Time"

#: admin/classes/classes.inc:98 user/classes.inc:117
msgid "Want to create a Single Class"
msgstr "hi-Want to create a Single Class"

#: admin/classes/classes.inc:98 user/classes.inc:117
msgid "Want to create a Recurring Class"
msgstr "hi-Want to create a Recurring Class"

#: admin/classes/classes.inc:109 user/classes.inc:130
msgid "Class Schedule"
msgstr "hi-Class Schedule"

#: admin/classes/classes.inc:115 user/classes.inc:136
msgid "Please select Class schedule."
msgstr "hi-Please select Class schedule."

#: admin/classes/classes.inc:126 admin/classes/edit_class.inc:133 user/classes.inc:147 user/edit_class.inc:158
msgid "Schedule for right now"
msgstr "hi-Schedule for right now"

#: admin/classes/classes.inc:128 admin/classes/edit_class.inc:135 user/classes.inc:149 user/edit_class.inc:160
msgid "check for the current date and time"
msgstr "hi-check for the current date and time"

#: admin/classes/classes.inc:138 admin/classes/edit_class.inc:148 user/classes.inc:162 user/edit_class.inc:173
msgid "Class Time"
msgstr "hi-Class Time"

#: admin/classes/classes.inc:144 admin/classes/edit_class.inc:156 admin/courses/courses.inc:109 user/courses.inc:121 user/edit_class.inc:181
msgid "Start date sholuld be greater than or equal to current date"
msgstr "hi-Start date sholuld be greater than or equal to current date"

#: admin/classes/classes.inc:160 admin/classes/edit_class.inc:173 user/classes.inc:182 user/edit_class.inc:199
msgid "Hours"
msgstr "hi-Hours"

#: admin/classes/classes.inc:177 admin/classes/edit_class.inc:192 user/classes.inc:199 user/edit_class.inc:219
msgid "Minutes"
msgstr "hi-Minutes"

#: admin/classes/classes.inc:185 admin/classes/edit_class.inc:202 user/classes.inc:207 user/edit_class.inc:229
msgid "Time Zone"
msgstr "hi-Time Zone"

#: admin/classes/classes.inc:200 user/classes.inc:222
msgid "Repeat every week"
msgstr "hi-Repeat every week"

#: admin/classes/classes.inc:210 user/classes.inc:232
msgid "on"
msgstr "hi-on"

#: admin/classes/classes.inc:230 user/classes.inc:254
msgid "Repeat by"
msgstr "hi-Repeat by"

#: admin/classes/classes.inc:234 user/classes.inc:258
msgid "Day"
msgstr "hi-Day"

#: admin/classes/classes.inc:234 user/classes.inc:258
msgid "Date"
msgstr "hi-Date"

#: admin/classes/classes.inc:261 user/classes.inc:285
msgid "of every month"
msgstr "hi-of every month"

#: admin/classes/classes.inc:291 user/classes.inc:315
msgid "Ends"
msgstr "hi-Ends"

#: admin/classes/classes.inc:295 user/classes.inc:319
msgid "After Classes"
msgstr "hi-After Classes"

#: admin/classes/classes.inc:295 user/classes.inc:319
msgid "On date"
msgstr "hi-On date"

#: admin/classes/classes.inc:311
msgid "Invalid End Class."
msgstr "hi-Invalid End Class."

#: admin/classes/classes.inc:331 admin/classes/edit_class.inc:214 user/classes.inc:355 user/edit_class.inc:241
msgid "Add more information about yourself and your class"
msgstr "hi-Add more information about yourself and your class"

#: admin/classes/classes.inc:345 admin/classes/edit_class.inc:228 user/classes.inc:369 user/edit_class.inc:255
msgid "Attendee Limit in a Class"
msgstr "hi-Attendee Limit in a Class"

#: admin/classes/classes.inc:351 user/classes.inc:375
msgid "Invalid attendee limit."
msgstr "hi-Invalid attendee limit."

#: admin/classes/classes.inc:359 admin/classes/edit_class.inc:242 user/classes.inc:383 user/edit_class.inc:269
msgid "Record this class"
msgstr "hi-Record this class"

#: admin/classes/classes.inc:365 admin/classes/edit_class.inc:248 user/classes.inc:389 user/edit_class.inc:275
msgid "Yes"
msgstr "hi-Yes"

#: admin/classes/classes.inc:375 admin/classes/edit_class.inc:260 user/classes.inc:399
msgid "Select a language"
msgstr "hi-Select a language"

#: admin/classes/classes.inc:382 user/classes.inc:407
msgid "Schedule & Continue"
msgstr "hi-Schedule & Continue"

#: admin/classes/classes.inc:426 user/classes.inc:482
msgid "Classes Saved Successfully"
msgstr "hi-Classes Saved Successfully"

#: admin/classes/classes.inc:438
msgid "Error Code: "
msgstr "hi-Error Code: "

#: admin/classes/edit_class.inc:88;108 admin/courses/courses.inc:75 user/courses.inc:87
msgid "Course name should not be empty"
msgstr "hi-Course name should not be empty"

#: admin/classes/edit_class.inc:268 admin/contents/list_content.inc:146 admin/courses/enrolluser.inc:163 user/edit_class.inc:295 user/contents/list_content.inc:170
msgid "Save"
msgstr "hi-Save"

#: admin/classes/edit_class.inc:319 user/edit_class.inc:346
msgid "Class Update Successfully"
msgstr "hi-Class Update Successfully"

#: admin/classes/list_classes.inc:36;43 user/list_classes.inc:73;80
msgid "Class Delete Successfully"
msgstr "hi-Class Delete Successfully"

#: admin/classes/list_classes.inc:128 user/list_classes.inc:163
msgid "Class Start Time"
msgstr "hi-Class Start Time"

#: admin/classes/list_classes.inc:129 user/list_classes.inc:164
msgid "Presenter"
msgstr "hi-Presenter"

#: admin/classes/list_classes.inc:130 user/list_classes.inc:165
msgid "Class Status"
msgstr "hi-Class Status"

#: admin/classes/list_classes.inc:131 user/list_classes.inc:166
msgid "Attendance Report"
msgstr "hi-Attendance Report"

#: admin/classes/list_classes.inc:132 admin/courses/enrolluser.inc:105 user/list_classes.inc:167
msgid "View Recording"
msgstr "hi-View Recording"

#: admin/classes/list_classes.inc:133 user/list_classes.inc:168
msgid "Downlaod Recording"
msgstr "hi-Downlaod Recording"

#: admin/classes/list_classes.inc:138 user/list_classes.inc:173
msgid "Manage Class"
msgstr "hi-Manage Class"

#: admin/classes/list_classes.inc:151 user/list_classes.inc:189
msgid "Get Class"
msgstr "hi-Get Class"

#: admin/classes/list_classes.inc:166 admin/courses/course_det.inc:38 user/course_det.inc:46 user/list_classes.inc:205
msgid "Back to Courses"
msgstr "hi-Back to Courses"

#: admin/classes/list_classes.inc:207 user/list_classes.inc:257
msgid "Edit Class"
msgstr "hi-Edit Class"

#: admin/classes/list_classes.inc:208 user/list_classes.inc:258
msgid "Delete Class"
msgstr "hi-Delete Class"

#: admin/classes/list_classes.inc:211 user/list_classes.inc:261
msgid "View Recurring Class Detail"
msgstr "hi-View Recurring Class Detail"

#: admin/classes/list_classes.inc:213;215 user/list_classes.inc:265;268
msgid "View"
msgstr "hi-View"

#: admin/classes/list_classes.inc:217 user/list_classes.inc:270
msgid "Download"
msgstr "hi-Download"

#: admin/classes/list_classes.inc:258 admin/contents/list_content.inc:127 admin/courses/list_courses.inc:59 user/contents/list_content.inc:151
msgid "Delete"
msgstr "hi-Delete"

#: admin/classes/list_classes.inc:362 admin/contents/list_content.inc:289 admin/courses/list_courses.inc:128 user/contents/list_content.inc:313
msgid "No Record Selected"
msgstr "hi-No Record Selected"

#: admin/contents/content.inc:4 user/contents/content.inc:4
msgid "content"
msgstr "hi-content"

#: admin/contents/content.inc:4 admin/contents/list_content.inc:121 admin/courses/enrolluser.inc:107 user/contents/content.inc:4 user/contents/list_content.inc:145
msgid "Upload Content"
msgstr "hi-Upload Content"

#: admin/contents/content.inc:49 admin/contents/list_content.inc:45 user/contents/content.inc:49 user/contents/list_content.inc:69
msgid "Content Name"
msgstr "hi-Content Name"

#: admin/contents/content.inc:63 user/contents/content.inc:63
msgid "Choose a file"
msgstr "hi-Choose a file"

#: admin/contents/content.inc:76 user/contents/content.inc:75
msgid "Allowed File Types"
msgstr "hi-Allowed File Types"

#: admin/contents/content.inc:85 user/contents/content.inc:84
msgid "Upload & Close"
msgstr "hi-Upload & Close"

#: admin/contents/content.inc:135 user/contents/content.inc:134
msgid "Content Uploaded"
msgstr "hi-Content Uploaded"

#: admin/contents/list_content.inc:14 user/contents/list_content.inc:14
msgid "Content"
msgstr "hi-Content"

#: admin/contents/list_content.inc:46 user/contents/list_content.inc:70
msgid "Type"
msgstr "hi-Type"

#: admin/contents/list_content.inc:47 user/contents/list_content.inc:71
msgid "Inside"
msgstr "hi-Inside"

#: admin/contents/list_content.inc:48 user/contents/list_content.inc:72
msgid "Status"
msgstr "hi-Status"

#: admin/contents/list_content.inc:82 user/contents/list_content.inc:106
msgid " Folder(s) "
msgstr "hi-Folder(s) "

#: admin/contents/list_content.inc:82 user/contents/list_content.inc:106
msgid " File(s)"
msgstr "hi- File(s)"

#: admin/contents/list_content.inc:133 user/contents/list_content.inc:157
msgid "Create Folder"
msgstr "hi-Create Folder"

#: admin/contents/list_content.inc:174 user/contents/list_content.inc:198
msgid "Get Update"
msgstr "hi-Get Update"

#: admin/contents/list_content.inc:238 user/contents/list_content.inc:262
msgid "Folder Created"
msgstr "hi-Folder Created"

#: admin/courses/course_det.inc:4 user/course_det.inc:4
msgid "Course Detail"
msgstr "hi-Course Detail"

#: admin/courses/course_det.inc:16 user/course_det.inc:25
msgid "Full Name:"
msgstr "hi-Full Name:"

#: admin/courses/course_det.inc:19 user/course_det.inc:28
msgid "Short Name:"
msgstr "hi-Short Name:"

#: admin/courses/course_det.inc:23 user/course_det.inc:32
msgid "Created By:"
msgstr "hi-Created By:"

#: admin/courses/course_det.inc:26 user/course_det.inc:35
msgid "Start Date:"
msgstr "hi-Start Date:"

#: admin/courses/course_det.inc:29 user/course_det.inc:38
msgid "End Date:"
msgstr "hi-End Date:"

#: admin/courses/course_det.inc:32 user/course_det.inc:41
msgid "Description:"
msgstr "hi-Description:"

#: admin/courses/course_det.inc:44 user/course_det.inc:52
msgid "Go to Classes"
msgstr "hi-Go to Classes"

#: admin/courses/courses.inc:4 user/contents/content.inc:4
msgid "wiziQ"
msgstr "hi-wiziQ"

#: admin/courses/courses.inc:4 admin/courses/list_courses.inc:54 user/courses.inc:4 user/list_courses.inc:92
msgid "Create Course"
msgstr "hi-Create Course"

#: admin/courses/courses.inc:65 admin/courses/list_courses.inc:21 user/courses.inc:77 user/list_courses.inc:55
msgid "Course Name"
msgstr "hi-Course Name"

#: admin/courses/courses.inc:83 user/courses.inc:95
msgid "Alias"
msgstr "hi-Alias"

#: admin/courses/courses.inc:93 user/courses.inc:105
msgid "Course short name should not be empty"
msgstr "hi-Course short name should not be empty"

#: admin/courses/courses.inc:101 user/courses.inc:113
msgid "Course Start Date"
msgstr "hi-Course Start Date"

#: admin/courses/courses.inc:117 user/courses.inc:130
msgid "Course End Date"
msgstr "hi-Course End Date"

#: admin/courses/courses.inc:125 user/courses.inc:138
msgid "End date sholuld be greater than or equal to start date"
msgstr "hi-End date sholuld be greater than or equal to start date"

#: admin/courses/courses.inc:133 admin/courses/list_courses.inc:22 user/courses.inc:146 user/list_courses.inc:56
msgid "Description"
msgstr "hi-Description"

#: admin/courses/courses.inc:137
msgid "Max 4000 Characters"
msgstr "hi-Max 4000 Characters"

#: admin/courses/courses.inc:145 user/courses.inc:158
msgid "Save & Close"
msgstr "hi-Save & Close"

#: admin/courses/courses.inc:196 user/courses.inc:209
msgid "End date should be greater then start date"
msgstr "hi-End date should be greater then start date"

#: admin/courses/courses.inc:201 user/courses.inc:214
msgid "Start date should be greater then current date"
msgstr "hi-Start date should be greater then current date"

#: admin/courses/courses.inc:230 user/courses.inc:244
msgid "Course Update Successfully"
msgstr "hi-Course Update Successfully"

#: admin/courses/courses.inc:245 user/courses.inc:258
msgid "Course Saved Successfully"
msgstr "hi-Course Saved Successfully"

#: admin/courses/enrolluser.inc:19
msgid "Please Login"
msgstr "hi-Please Login"

#: admin/courses/enrolluser.inc:86
msgid "Enroll User"
msgstr "hi-Enroll User"

#: admin/courses/enrolluser.inc:88
msgid "Add"
msgstr "hi-Add"

#: admin/courses/enrolluser.inc:88
msgid "Remove"
msgstr "hi-Remove"

#: admin/courses/enrolluser.inc:103
msgid "User Name"
msgstr "hi-User Name"

#: admin/courses/enrolluser.inc:106 user/class_det.inc:121
msgid "Download Recording"
msgstr "hi-Download Recording"

#: admin/courses/enrolluser.inc:169
msgid "test"
msgstr "hi-test"

#: admin/courses/list_courses.inc:23 user/list_courses.inc:64
msgid "Manage Course"
msgstr "hi-Manage Course"

#: admin/courses/list_courses.inc:24 user/list_courses.inc:57
msgid "Created by"
msgstr "hi-Created by"

#: admin/courses/list_courses.inc:25 user/list_courses.inc:60
msgid "Number of Classes"
msgstr "hi-Number of Classes"

#: admin/courses/list_courses.inc:80 user/list_courses.inc:107;121
msgid "Edit Course"
msgstr "hi-Edit Course"

#: admin/courses/list_courses.inc:80 user/list_courses.inc:107;121
msgid "Add Class"
msgstr "hi-Add Class"

#: admin/courses/list_courses.inc:80
msgid "Enroll Users"
msgstr "hi-Enroll Users"

#: user/attendee_report.inc:25 user/classes.inc:46 user/edit_class.inc:26
msgid "You are not Authorized"
msgstr "hi-You are not Authorized"

#: user/class_det.inc:29 user/list_classes.inc:47
msgid "You are not enroll to view this classs"
msgstr "hi-You are not enroll to view this classs"

#: user/courses.inc:26
msgid "You are not authorized for this section"
msgstr "hi-You are not enroll to view this classs"

#: user/edit_class.inc:287
msgid "Language of instruction:"
msgstr "hi-Language of instruction:"

#: user/list_courses.inc:28
msgid "Course Delete Successfully"
msgstr "hi-Course Delete Successfully"

#: user/list_courses.inc:30
msgid "Some Problem occuring"
msgstr "hi-Some Problem occuring"

#: user/list_courses.inc:58
msgid "Start Date"
msgstr "hi-Start Date"

#: user/list_courses.inc:59
msgid "End Date"
msgstr "hi-End Date"

#: user/list_courses.inc:107;121
msgid "Delete Course"
msgstr "hi-Delete Course"

#: user/contents/list_content.inc:44
msgid "You are not enroll to view this Content"
msgstr "hi-You are not enroll to view this Content"

#: user/contents/list_content.inc:50
msgid "Please visit course first"
msgstr "hi-Please visit course first"

#: custom error msg
msgid "403"
msgstr "hi-Forbidden: The request is refused by the API."

msgid "200"
msgstr "hi-The request has been successfully completed."

msgid "304"
msgstr "hi-Not Modified: Operation ok. There was no new data to return."

msgid "400"
msgstr "hi-Bad Request: The request is invalid. Error messages are returned for explanation purposes."

msgid "401"
msgstr "hi-Not Authorized: The credentials provided are invalid or are needed for the operation."

msgid "404"
msgstr "hi-Not Found: The resource requested doesn’t exist. Ex: The provided car_id in the url doesn’t exist."

msgid "500"
msgstr "hi-Internal Server Error: An unrecoverable error has occurred at server side."

msgid "502"
msgstr "hi-Bad Gateway: The API service is down or being upgraded."

msgid "503"
msgstr "hi-Service Unavailable: The service is overloaded with a lot requests and can’t manage the incoming request."

msgid "1000"
msgstr "hi-There is some error while processing your request."

msgid "1001"
msgstr "hi-presenter_id parameter is missing."

msgid "1002"
msgstr "hi-presenter_name parameter is missing."

msgid "1003"
msgstr "hi-Specified time_zone parameter is not available."

msgid "1004"
msgstr "hi-start_time parameter cannot precede current datetime."

msgid "1005"
msgstr "hi-Invalid start_time parameter."

msgid "1006"
msgstr "hi-time_zone parameter is missing."

msgid "1007"
msgstr "hi-Attendees can be  added only in an upcoming class."

msgid "1008"
msgstr "hi-This class has been cancelled."

msgid "1009"
msgstr "hi-class_id is invalid."

msgid "1010"
msgstr "hi-duration parameter cannot exceed  #MaxClassMinutes# minutes."

msgid "1011"
msgstr "hi-attendee_limit parameter cannot exceed #MaxUserPerClass# attendees."

msgid "1012"
msgstr "hi-Only #MaxConcurrentClass# simultaneous classes are allowed."

msgid "1013"
msgstr "hi-No record found."

msgid "1014"
msgstr "hi-title parameter is missing."

msgid "1015"
msgstr "hi-Cannot modify an in-progress class."

msgid "1016"
msgstr "hi-Cannot modify a completed class."

msgid "1017"
msgstr "hi-Cannot modify an expired class."

msgid "1018"
msgstr "hi-Cannot modify a deleted class."

msgid "1019"
msgstr "hi-start_time parameter is missing."

msgid "1020"
msgstr "hi-class_id parameter is missing."

msgid "1021"
msgstr "hi-attendee_list parameter is missing."

msgid "1022"
msgstr "hi-Datetime is not in valid format."

msgid "1023"
msgstr "hi-screen_name is missing."

msgid "1024"
msgstr "hi-attendee_id is missing."

msgid "1025"
msgstr "hi-page_size parameter cannot exceed 100."

msgid "1026"
msgstr "hi-Class has not held yet."

msgid "1027"
msgstr "hi-Expired class."

msgid "1028"
msgstr "hi-Cancelled class."

msgid "1029"
msgstr "hi-Attendance report will be available soon."

msgid "1030"
msgstr "hi-presenter_email parameter is missing."

msgid "1031"
msgstr "hi-presenter_email is not valid."

msgid "1032"
msgstr "hi-You have already scheduled a class for the current time."

msgid "1033"
msgstr "hi-Class is already cancelled."

msgid "1034"
msgstr "hi-Cannot cancel inprogress class."

msgid "1035"
msgstr "hi-Cannot cancel completed class."

msgid "1036"
msgstr "hi-Cannot cancel expired class."

msgid "Techers"
msgstr "hi-Techers"

msgid "Select User"
msgstr "hi-Select User"

msgid "Teacher Name"
msgstr "hi-Teacher Name"

msgid "Can Schedule Class"
msgstr "hi-Can Schedule Class"

msgid "About the teacher"
msgstr "hi-About the teacher"

msgid "Teacher Saved Successfully"
msgstr "hi-Teacher Saved Successfully"

msgid "Teacher Updated Successfully"
msgstr "hi-Teacher Updated Successfully"

msgid "Add Teacher"
msgstr "hi-Add Teacher"

msgid "Teacher Email"
msgstr "hi-Teacher Email"

msgid "Teacher Password"
msgstr "hi-Teacher Password"

msgid "Activate"
msgstr "hi-Activate"

msgid "Manage Teacher"
msgstr "hi-Manage Teacher"

